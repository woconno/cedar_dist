extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    // Tell cargo to tell rustc to link the system bzip2
    // shared library.
    //println!("cargo:rustc-link-lib=bz2");

    // Tell cargo to invalidate the built crate whenever the wrapper changes
    println!("cargo:rerun-if-changed=wrapper.h");
    println!("cargo:rustc-link-search=../../../build/unix-user/");
    let func_regexes = vec![
        "sal_dma_.*",
        "soc_mem_write.*",
        "soc_mem_read.*",
        "soc_mem_field32_.*",
        "soc_mem_field_.*",
        "soc_format_.*",
        "soc_reg64_.*",
        "soc_reg_set",
        "soc_reg_get",
        "soc_errmsg",
        "cmdlist_add",
        "custom_cmd",

        //start higher level interfaces
        "bcmi_ft_init",

        //start the clock
        "bcm_time_interface_t_init",
        "bcm_time_interface_add"
    ];

    let var_regexes = vec![
        //MEMORIES
        //FTFP_TCAM, FTFP2_TCAM
        "BSK_FTFP_.*",
        "VALIDf",
        "KEY_MODEf",
        "KEY_TYPEf",
        "KEY_SINGLE_WIDEf",
        "KEYf",
        "KEY_KEYf",
        "MASK_MODEf",
        "MASK_TYPEf",
        "MASK_KEYf",
        "MASKf",
        "MASK_SINGLE_WIDEf",

        //FTFP_POLICY, FTFP2_POLICY
        //"BSK_FTFP_POLICY"
        "BIDIRECTIONAL_FLOWf",
        "SESSION_KEY_LTS_PROFILEf",
        "SESSION_DATA_LTS_PROFILEf",
        "ALU_DATA_LTS_PROFILEf",
        "DO_NOT_FTf",
        "GROUP_IDf",
        "SESSION_KEY_TYPEf",
        "SESSION_KEY_MODEf",
        "LEARN_DISABLEf",
        "UFLOW_FIELDS_VALIDf",

        //FTFP_LOGICAL_TBL_SEL_TCAM
        "BSK_FTFP_LTS_LOGICAL_TBL_SEL_TCAMm",
        //VALIDf,
        "SOURCE_CLASSf",
        "VFI_VALIDf",
        "MIRRORf",
        "DROPf",
        "PARSER1_L4_VALIDf",
        "PARSER2_L4_VALIDf",
        "L3_TYPEf",
        "FORWARDING_TYPEf",
        //cut off here



        "EXTRACT_TYPE_A_.*",
        "EXTRACT_TYPE_C_.*",
        
        
        "PARSER.*",
        "L3_TYPE_.*",
        "HVE_RESULTS_.*",
        "GROUP_LOOKUP_.*",
        "BSC_TL_DG_TO_DT_ALU_BASE_CONTROL",
        "CHECK_.*",
        "FLEX_CHECK_LOGIC_.*",
        "UPDATE_.*",
        "EXPORT_.*",
        "LOAD_TRIGGERf",
        "CLEAR_ON_.*",
        "ALU_BASE_.*",
        "MEM_BLOCK_.*",
        "DATAf",
        "BSC_DG_GROUP_AL32_.*",
        "ALU_OP_.*",
        "BSC_DT_.*",
        "SHIFT_AMOUNT_.*",
        "BSD_POLICY_.*",
        "BSD_FLEX_ALU32_.*",
        "BSC_DG_.*",
        "BSC_KG_.*",
        "GROUP_ALU32_.*",
        "SESSION_DATA_TYPEf",
        "BSC_EX_.*",
        "TIME_INTERVAL_ENABLEf",
        "RA_ENABLEf",
        "PACKET_BUILD_ENABLEf",
        "MAX_RECORDS_PER_PACKETf",
        "MAX_POINTERf",
        "ENABLEf",
        "HDR_.*",
        "SOC_BLOCK_ANY",
        "SOC_BLOCK_ALL",
        "KEY_.*",
        "MASK_.*",
        "BSK_SESSION_.*",
        "MASKf",
        "GROUP_IDf",
        "SESSION_KEY_LTS_PROFILEf",
        "SESSION_DATA_LTS_PROFILEf",
        "UFLOW_FIELDS_VALIDf",
        "BSC_TL_DG_TO_DT_.*",
        "BSC_TL_DG_TO_DT_ALU32_.*",
        "FLEX_CHECK_LOGIC_.*",
        "GROUP_VALIDf",
        "PERIODIC_EXPORT_ENf",
        "SET_IDf",
        "AGE_OUT_PROFILE_IDXf",
        "COLLECTOR_IDf",
        "FLOW_EXCEED_PROFILE_IDXf",
        "SHIFT_AMOUNT_BYTE_.*",
        "ALU_DATA_LTS_PROFILEf",
        "BSC_AG_PERIODIC_EXPORTr",
        "EXPORT_PERIODf",
        "TIME_INTERVALf",
        "FT_.*_ENABLEf",
        "FT_.*_BANK_VALID_BITMAPf",
        "TIMESTAMP_.*_TRIGf",
        "FLEX_CHECK_.*_KMAP_CONTROLf",
        "BYTE_COUNT_CHECK_.*",
        "BYTE_COUNT_UPDATE_ALU_SELECTf",
        "FLEX_EXPORT_KMAP_CONTROLf",
        "BSK_ALU_DATA_LTS_MUX_CTRL_PLUS_MASKm",
        "FIRST_DELAY_OPERANDf",
        "SECOND_DELAY_OPERANDf",
        "DELAY_MODEf",
        "DELAY_.*",
        "BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt",
        "ALU32_SPECIFIC_CONTROL_BUSf",
        "TIMESTAMP_PROFILE_IDXf",
        "TS_SELECT_3f",
        "TS_SELECT_2f",
        "TS_SELECT_1f",
        "TS_SELECT_0f",
        "DATA_3f",
        "DATA_2f",
        "DATA_1f",
        "DATA_0f",
        "BSD_FLEX_TIMESTAMP_.*",
        "GROUP_KEY_TYPEf",
        "ALU32_.*",
        "TIMESTAMP_ENGINE_.*",
        "BANK_.*",
        "FT_KEY_SINGLEm",
        "BSC_DT_FLEX_SESSION_DATA_SINGLEm",
        "BASE_VALIDf",
        "OPAQUE_DATA_.*",
        "VALID_.*",
        "OPAQUE_DATAf",
        "BCM_TIME_ENABLE",
        "AGE_OUT_ENABLEf",
        "AGE_OUT_INTERVALf",
        "DO_NOT_AGE_WHEN_FIFO_FULLf",
        "EXPORT_AGE_OUTf",
        "EXPORT_FLUSH_ENf",
        "BSC_TL_KG_TO_KT_AGE_CONTROL_BUSfmt"
    ];

    let type_regexes = vec![
        "bsc_.*",
        "cmd_result_t",
        "bsk_.*",
        "bsd_.*",
        "ft_key_single_entry_t",
        "bcm_time_interface_t"
    ];

    // The bindgen::Builder is the main entry point
    // to bindgen, and lets you build up options for
    // the resulting bindings.
    let mut builder = bindgen::Builder::default();

    for fr in func_regexes {
        builder = builder.allowlist_function(fr);
    }

    for vr in var_regexes {
        builder = builder.allowlist_var(vr);
    }

    for tr in type_regexes {
        builder = builder.allowlist_type(tr);
    }

    let bindings = builder
        //.clang_arg("--sysroot=../../../")
        .clang_arg("-I../../../include")
        .clang_arg("-DBCM_ESW_SUPPORT=1")

        // The input header we would like to generate
        // bindings for.
        .header("wrapper.h")
        // Tell cargo to invalidate the built crate whenever any of the
        // included header files changed.
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        // Finish the builder and generate the bindings.
        .generate()
        // Unwrap the Result and panic on failure.
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
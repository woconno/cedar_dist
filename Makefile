export BUILDDIR=$(shell pwd)/target/debug

lib = libcustomer
SHELL := /bin/zsh
build:
	{\
	cargo build &&\
	cp -v ./target/debug/libbsca2.a ../../../build/unix-user/libcustomer.a ;\
	echo COPIED LIBRARY ;\
	}

move: build
	rm -f $(LIBDIR)/libcustomer.a
	cp -v ./target/debug/libbsca2.a ../../../build/unix-user/libcustomer.a
	echo "COPIED LIBRARY"

all: build
use serde::{Deserialize};

#[derive(Deserialize, Debug)]
pub struct SWCfg {
    pub filter_mux: Vec<SWExtractCfg>,
    pub queries: Vec<SWQueryCfg>,
    pub assign: SWAssignCfg
}

#[derive(Deserialize, Debug)]
pub struct SWAssignCfg {
    pub banks: [u32; 4],
    pub alus: [u32; 12],
    pub ts_engines: [u32; 4],
    pub bank_valid_bm_ift: u32,
    pub bank_valid_bm_eft: u32
}

#[derive(Deserialize, Debug)]
pub struct SWQueryCfg {
    pub gid: u32,
    pub filter: SWFilterCfg,
    pub reduce: SWReduceCfg,
    pub alus: Vec<SWAluCfg>,
    pub pde_pdd_prof: SWPDEPDDProfCfg,

    #[serde(default)]
    pub eft_alus: Option<Vec<SWAluCfg>>,
    #[serde(default)]
    pub eft_pde_pdd_prof: Option<SWPDEPDDProfCfg>
}

#[derive(Deserialize, Debug)]
pub struct SWFilterCfg {
    pub filter_key:  [u8; 24],
    pub filter_mask: [u8; 24],
    pub row: u32
}

#[derive(Deserialize, Debug)]
pub struct SWReduceCfg {
    pub reduce_mux: Vec<SWExtractCfg>,
    pub reduce_mask: [u8; 16],
    pub row: u32
}

#[derive(Deserialize, Debug)]
pub struct SWExtractCfg {
    pub sel: u32,
    pub mode: u32,
    pub section: u32
}

#[derive(Deserialize, Debug)]
pub enum CheckOpCfg {
    GREATER,
    GEQ,
    EQ,
    NEQ,
    LESS,
    LEQ,
    PASS,
    NONE,
    MASK
}

#[derive(Deserialize, Debug)]
pub enum UpdateOpCfg {
    NOOP,
    INCREMENT,
    ADD_BYTES,
    ADD_VALUE,
    UPDATE_MIN,
    UPDATE_MAX,
    UPDATE_VAL,
    UPDATE_EWMA
}

#[derive(Deserialize, Debug)]
pub enum DelayOperandCfg {
    PKT_TS0, //timestamp in the packet
    PKT_TS1, //timestamp of when the packet hit the ingress port
    SESSION_TS0, //value stored in timestamp 0
    SESSION_TS1, //value stored in timestamp 1
    SESSION_TS2, //value stored in timestamp 2
    SESSION_TS3, //value stored in timestamp 3
    CMIC_TS, //current time from CMIC, use this one for current time by default
    LTS_MODE_TOD, //current time
    LIVE_IPROC1, //current time
    LIVE_IPROC2, //current time
    BSC_TS_UTC_CONVERSION_TOD, //current time
}

#[derive(Deserialize, Debug)]
pub enum DelayModeCfg {
    PTP_64,
    NTP_64,
    GENERIC_48BIT, //USE THIS ONE IF UNSURE
    LTS_MODE
}

#[derive(Deserialize, Debug)]
pub enum AttrGranCfg {
    G32Bit,
    G16Bit
}

#[derive(Deserialize, Debug)]
pub enum DelayAttrSelectCfg {
    USE_DELAY_VAL, //read/write the delay result instead of the selected attr
    USE_SELECTED_ATTR_VAL //read/write the attr defined in the ALU config
}

#[derive(Deserialize, Debug)]
pub struct SWAluCfg {
    pub alu_idx: u32,
    pub profile_idx: u32,
    pub check_0_attr_select: u32,
    pub check_0_op: CheckOpCfg,
    pub check_1_attr_select: u32,
    pub check_1_op: CheckOpCfg,
    pub timestamp_2_trig: bool,
    pub timestamp_3_trig: bool,
    pub update_attr_select: u32,
    pub update_op: UpdateOpCfg,
    pub update_op_param: u32,
    pub export_op: u32,
    pub export_check_mode: u32,
    pub clear_on_export: bool,
    pub clear_on_periodic_export: bool,
    pub load_trigger: u32,
    pub timestamp_0_trig: bool,
    pub timestamp_1_trig: bool,
    //specific controls
    pub spec_check_0_thresh: u32,
    pub spec_check_1_thresh: u32,
    pub spec_export_thresh: u32,
    pub spec_first_delay_op: DelayOperandCfg,
    pub spec_second_delay_op: DelayOperandCfg,
    pub spec_delay_mode: DelayModeCfg,
    pub spec_delay_gran: u32,
    pub spec_delay_offset: u32,
    pub spec_check_attr_0_ts_select: DelayAttrSelectCfg,
    pub spec_check_attr_1_ts_select: DelayAttrSelectCfg,
    pub spec_update_attr_1_ts_select: DelayAttrSelectCfg,
    pub spec_check_attr_0_gran: AttrGranCfg,
    pub spec_check_attr_1_gran: AttrGranCfg,
    pub replace_check_0_byte_count: bool,
    pub replace_check_1_byte_count: bool,
    pub replace_update_byte_count: bool
}

#[derive(Deserialize, Debug)]
pub struct SWPDEPDDProfCfg {
    pub pdd_profile: SWPDDProfileCfg,
    pub pde_profile: Vec<SWPDEEntryCfg>,
    pub export_pde: Vec<SWPDEEntryCfg>,
    // export_pde: Vec<ExportPDEShiftCfg>,
    pub profile_id: u32
}

#[derive(Deserialize, Debug)]
pub struct SWPDDProfileCfg {
    pub alus: [bool; 12],
    pub ts_engines: [bool; 4]
}

#[derive(Deserialize, Debug)]
pub enum PDEEntityTypeCfg {
    TS0,
    TS1,
    TS2,
    TS3,
    ALU0,
    ALU1,
    ALU2,
    ALU3,
    ALU4,
    ALU5,
    ALU6,
    ALU7,
    ALU8,
    ALU9,
    ALU10,
    ALU11,
}

#[derive(Deserialize, Debug)]
pub struct SWPDEEntryCfg {
    pub source: PDEEntityTypeCfg,
    pub dsts: Vec<u32>
}
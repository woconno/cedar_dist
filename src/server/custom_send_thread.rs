use std::{net::{Ipv4Addr, UdpSocket, SocketAddr}, sync::{mpsc::Receiver}, time::{Duration, Instant}};
use serde::Serialize;
use crate::server::{SWCfg, ll_config::{SWExtractCfg, SWQueryCfg, SWPDEEntryCfg, PDEEntityTypeCfg}};

const GID_TO_TEMPLATE_ID_OFFSET: u32 = 1000;

const KEY_END_BYTE: usize = 59; //FIRST BYTE AFTER THE KEY
const DATA_START_BYTE: usize = 60; //FIRST BYTE OF THE DATA

pub enum TemplateThreadMessage{
    NewCfg(Vec<TemplateInfo>),
    _Stop
}

#[derive(Debug, Clone, Copy)]
pub enum ElementType {
    SrcIPv4,
    DstIPv4,
    SrcL4Port,
    DstL4Port,
    IPProto,
    TCPFlags,
    ALU0,
    ALU1,
    ALU2,
    ALU3,
    ALU4,
    ALU5,
    ALU6,
    ALU7,
    ALU8,
    ALU9,
    ALU10,
    ALU11,
    TS0,
    TS1,
    TS2,
    TS3,
    UNKNOWN,
    PADDING
}

fn element_type_to_id(el: &ElementType) -> u8 {
    match el {
        &ElementType::SrcIPv4 => 1,
        &ElementType::DstIPv4 => 2,
        &ElementType::SrcL4Port => 3,
        &ElementType::DstL4Port => 4,
        &ElementType::IPProto => 5,
        &ElementType::TCPFlags => 6,
        &ElementType::ALU0 => 100,
        &ElementType::ALU1 => 101,
        &ElementType::ALU2 => 102,
        &ElementType::ALU3 => 103,
        &ElementType::ALU4 => 104,
        &ElementType::ALU5 => 105,
        &ElementType::ALU6 => 106,
        &ElementType::ALU7 => 107,
        &ElementType::ALU8 => 108,
        &ElementType::ALU9 => 109,
        &ElementType::ALU10 => 110,
        &ElementType::ALU11 => 111,
        &ElementType::TS0 => 200,
        &ElementType::TS1 => 201,
        &ElementType::TS2 => 202,
        &ElementType::TS3 => 203,
        &ElementType::UNKNOWN => 0,
        &ElementType::PADDING => 255
    }
}

fn entity_type_to_element_tag(t: &PDEEntityTypeCfg) -> ElementType {
    match t {
        &PDEEntityTypeCfg::TS0   => ElementType::TS0,
        &PDEEntityTypeCfg::TS1   => ElementType::TS1,
        &PDEEntityTypeCfg::TS2   => ElementType::TS2,
        &PDEEntityTypeCfg::TS3   => ElementType::TS3,
        &PDEEntityTypeCfg::ALU0  => ElementType::ALU0,
        &PDEEntityTypeCfg::ALU1  => ElementType::ALU1,
        &PDEEntityTypeCfg::ALU2  => ElementType::ALU2,
        &PDEEntityTypeCfg::ALU3  => ElementType::ALU3,
        &PDEEntityTypeCfg::ALU4  => ElementType::ALU4,
        &PDEEntityTypeCfg::ALU5  => ElementType::ALU5,
        &PDEEntityTypeCfg::ALU6  => ElementType::ALU6,
        &PDEEntityTypeCfg::ALU7  => ElementType::ALU7,
        &PDEEntityTypeCfg::ALU8  => ElementType::ALU8,
        &PDEEntityTypeCfg::ALU9  => ElementType::ALU9,
        &PDEEntityTypeCfg::ALU10 => ElementType::ALU10,
        &PDEEntityTypeCfg::ALU11 => ElementType::ALU11
    }
}

struct KeyElement {
    width: u32,
    element: ElementType
}

fn mux_to_element(mux_ctrl: &Vec<SWExtractCfg>) -> Vec<KeyElement> {
    let mut kp = Vec::new();
    for ctrl in mux_ctrl.iter() {
        match (ctrl.sel, ctrl.mode, ctrl.section) {
            (17, 1, 0) => { kp.push(KeyElement { width: 1, element: ElementType::IPProto }) }
            (14, 0, 0) => { kp.push(KeyElement { width: 4, element: ElementType::SrcIPv4 }) }
            (15, 0, 0) => (),
            (22, 0, 0) => { kp.push(KeyElement { width: 4, element: ElementType::DstIPv4 }) },
            (23, 0, 0) => (),
            (34, 0, 0) => { kp.push(KeyElement { width: 2, element: ElementType::DstL4Port }) },
            (35, 0, 0) => { kp.push(KeyElement { width: 2, element: ElementType::SrcL4Port }) },
            (0x25, 1,0)=> { kp.push(KeyElement { width: 1, element: ElementType::TCPFlags }) },
            (_, 0, _) => { kp.push(KeyElement { width: 2, element: ElementType::UNKNOWN }) },
            (_, 1, _) => { kp.push(KeyElement { width: 1, element: ElementType::UNKNOWN }) },
            (_, _, _) => { kp.push(KeyElement { width: 1, element: ElementType::UNKNOWN }) },
        }
    }

    kp
}

fn pde_bytes_to_offset_width(bytes_shifts: &[u32]) -> (u16, u16) { //(offset, width)
    //suppose ALU0: [63, 62, 61, 60] comes in
    //byte_shift 63 is goes to the first byte of the data bank since the data bank is 64 bytes wide and byte_shift 0 goes to the last byte
    //the data bank starts at byte 60 in the IPFIX packet and ends at byte 124, which is the last byte
    //ALU0 occupies bytes 60, 61, 62, 63 in the packet (corresponding to shift_byte_63, 62, 61, 60)
    //so we would report (60, 4)
    //ALU1: [59, 58, 57, 56] -> (64, 4)
    //TS0: [55, 54, 53, 52, 51, 50] -> (68, 6)
    let top_idx = *bytes_shifts.iter().max().expect("Failed to get max of bytes");
    let width = bytes_shifts.len() as u16;

    let offset_in_to_data = 63 - top_idx;
    let offset_in_ipfix_pkt = DATA_START_BYTE as u16 + offset_in_to_data as u16;
    (offset_in_ipfix_pkt, width)
}

#[derive(Debug, Serialize)]
pub struct EntryInfo {
    pub start_byte: u16,
    pub width: u16,
    pub el_type: u8
}

#[derive(Debug, Serialize)]
pub struct TemplateInfo {
    key_info: Vec<EntryInfo>,
    data_info: Vec<EntryInfo>,
    id: u32
}

fn query_to_template(query: &SWQueryCfg) -> TemplateInfo {
    let mut info = TemplateInfo {
        key_info: Vec::new(),
        data_info: Vec::new(),
        id: query.gid + GID_TO_TEMPLATE_ID_OFFSET
    };

    let key_elements = mux_to_element(&query.reduce.reduce_mux); //first element is closest to byte 58, last element is furthest from byte 58
    let mut cur_byte = KEY_END_BYTE as u16;
    for k in key_elements.iter() {
        cur_byte -= k.width as u16;
        let el_id = element_type_to_id(&k.element);
        info.key_info.push(EntryInfo { start_byte: cur_byte, width: k.width as u16, el_type: el_id });
    }

    for shift in query.pde_pdd_prof.export_pde.iter() {
        let t = element_type_to_id(&entity_type_to_element_tag(&shift.source));
        let (offset, width) = pde_bytes_to_offset_width(&shift.dsts);
        info.data_info.push(EntryInfo { start_byte: offset, width: width, el_type: t });
    }

    if query.eft_pde_pdd_prof.is_some() {
        for shift in query.eft_pde_pdd_prof.as_ref().unwrap().export_pde.iter() {
            let t = element_type_to_id(&entity_type_to_element_tag(&shift.source));
            let (offset, width) = pde_bytes_to_offset_width(&shift.dsts);
            info.data_info.push(EntryInfo { start_byte: offset, width: width, el_type: t });
        }
    }

    info
}

pub fn config_to_template_info(cfg: &SWCfg) -> Vec<TemplateInfo> {
    cfg.queries.iter().map(|q|{ query_to_template(q) }).collect()
}

fn gen_template(tmp: &TemplateInfo) -> Vec<u8> {
    serde_json::to_vec(tmp).expect("Failed to serialize template")
}

fn send_template(tmp_msg: &Vec<u8>, socket: &UdpSocket, dst_addr: Ipv4Addr, dst_port: u16) {
    let addr = SocketAddr::new(std::net::IpAddr::V4(dst_addr), dst_port);
    let _bytes_sent = socket.send_to(tmp_msg.as_slice(), addr).expect("FAILED TO SEND TEMPLATE");
}


pub fn template_send_thread(sock_addr: Ipv4Addr, sock_port: u16, dst_addr: Ipv4Addr, dst_port: u16, interval_ms: u32, rx: Receiver<TemplateThreadMessage>){
    let mut cur_templates = Vec::<Vec<u8>>::new();
    let addr = SocketAddr::new(std::net::IpAddr::V4(sock_addr), sock_port);
    let socket = UdpSocket::bind(addr).expect("TEMPLATE THREAD FAILED TO BIND TO SOCKET");
    println!("CONFIG:");
    println!("  template_udp_src: {}:{}", sock_addr, sock_port);
    println!("  template_udp_dst: {}:{}", dst_addr, dst_port);
    loop {
        match rx.recv_timeout(Duration::from_millis(interval_ms as u64)) {
            Ok(ttm) => match ttm {
                TemplateThreadMessage::NewCfg(cfg) => {
                    let start = Instant::now();
                    cur_templates = Vec::new();
                    for ti in cfg.iter() {
                        cur_templates.push(gen_template(ti));
                    }
                    let end = start.elapsed();
                    println!("generating all templates took {:?}", end);
                },
                TemplateThreadMessage::_Stop => { break; }
            },
            Err(_) => {
                //let start = Instant::now();
                for tmp in cur_templates.iter() {
                    send_template(tmp, &socket, dst_addr, dst_port);
                }
                //let end = start.elapsed();
                //println!("Sending {} template took {:?}", cur_templates.len(), end);
            }
        }
    }
}
use crate::hw_config::tables::{CheckOp, UpdateOp, DelayOperand, DelayMode, DelayAttrSelect, ALUConf};
use crate::hw_config::{tables::*, memory_interface::SOCError, table_abstraction::bsc_dg_group_alu32_profile_table::AttrGran};
use crate::server::ll_config::*;

//ALL OF THESE CONVERT BETWEEN THE ENUMS USED IN THE CONFIG AND THE ENUMS USED INTERNALLY
fn delay_op_cfg_to_normal(op: &DelayOperandCfg) -> DelayOperand {
    match op {
        &DelayOperandCfg::PKT_TS0 =>     DelayOperand::PKT_TS0,
        &DelayOperandCfg::PKT_TS1 =>     DelayOperand::PKT_TS1,
        &DelayOperandCfg::SESSION_TS0 => DelayOperand::SESSION_TS0,
        &DelayOperandCfg::SESSION_TS1 => DelayOperand::SESSION_TS1,
        &DelayOperandCfg::SESSION_TS2 => DelayOperand::SESSION_TS2,
        &DelayOperandCfg::SESSION_TS3 => DelayOperand::SESSION_TS3,
        &DelayOperandCfg::CMIC_TS =>     DelayOperand::CMIC_TS,
        _ => DelayOperand::CMIC_TS
    }
}

fn check_op_cfg_to_normal(op: &CheckOpCfg) -> CheckOp {
    match op {
        &CheckOpCfg::GREATER => CheckOp::GREATER,
        &CheckOpCfg::GEQ => CheckOp::GEQ,
        &CheckOpCfg::EQ => CheckOp::EQ,
        &CheckOpCfg::NEQ => CheckOp::NEQ,
        &CheckOpCfg::LESS => CheckOp::LESS,
        &CheckOpCfg::LEQ => CheckOp::LEQ,
        &CheckOpCfg::PASS => CheckOp::PASS,
        &CheckOpCfg::NONE => CheckOp::NONE,
        &CheckOpCfg::MASK => CheckOp::MASK
    }
}

fn update_op_cfg_to_normal(op: &UpdateOpCfg) -> UpdateOp {
    match op {
        &UpdateOpCfg::NOOP => UpdateOp::NOOP,
        &UpdateOpCfg::INCREMENT => UpdateOp::INCREMENT,
        &UpdateOpCfg::ADD_BYTES => UpdateOp::ADD_BYTES,
        &UpdateOpCfg::ADD_VALUE => UpdateOp::ADD_VALUE,
        &UpdateOpCfg::UPDATE_MIN => UpdateOp::UPDATE_MIN,
        &UpdateOpCfg::UPDATE_MAX => UpdateOp::UPDATE_MAX,
        &UpdateOpCfg::UPDATE_VAL => UpdateOp::UPDATE_VAL,
        &UpdateOpCfg::UPDATE_EWMA => UpdateOp::UPDATE_EWMA,
    }
}

fn delay_mode_cfg_to_normal(mode: &DelayModeCfg) -> DelayMode {
    match mode {
        &DelayModeCfg::GENERIC_48BIT => DelayMode::GENERIC_48BIT,
        &DelayModeCfg::LTS_MODE => DelayMode::LTS_MODE,
        &DelayModeCfg::NTP_64 => DelayMode::NTP_64,
        &DelayModeCfg::PTP_64 => DelayMode::PTP_64
    }
}

fn attr_gran_cfg_to_normal(gran: &AttrGranCfg) -> AttrGran {
    match gran {
        &AttrGranCfg::G32Bit => AttrGran::G32Bit,
        &AttrGranCfg::G16Bit => AttrGran::G16Bit
    }
}

fn delay_attr_cfg_to_normal(sel: &DelayAttrSelectCfg) -> DelayAttrSelect {
    match sel { 
        &DelayAttrSelectCfg::USE_SELECTED_ATTR_VAL => DelayAttrSelect::USE_SELECTED_ATTR_VAL, 
        &DelayAttrSelectCfg::USE_DELAY_VAL => DelayAttrSelect::USE_DELAY_VAL
    }
}

fn ts_to_shifts(ts: &TSProfile) -> [u32; 6] {
    match ts {
        &TSProfile::TS0 => [136, 135, 134, 133, 132, 131],
        &TSProfile::TS1 => [130, 129, 128, 127, 126, 125],
        &TSProfile::TS2 => [124, 123, 122, 121, 120, 119],
        &TSProfile::TS3 => [118, 117, 116, 115, 114, 113]
    }
}

fn alu_to_shifts(alu: &ALUProfile) -> [u32; 4] {
    match alu {
        &ALUProfile::ALU0 =>  [106, 105, 104, 103],
        &ALUProfile::ALU1 =>  [102, 101, 100, 99],
        &ALUProfile::ALU2 =>  [98, 97, 96, 95],
        &ALUProfile::ALU3 =>  [94, 93, 92, 91],
        &ALUProfile::ALU4 =>  [90, 89, 88, 87],
        &ALUProfile::ALU5 =>  [86, 85, 84, 83],
        &ALUProfile::ALU6 =>  [82, 81, 80, 79],
        &ALUProfile::ALU7 =>  [78, 77, 76, 75],
        &ALUProfile::ALU8 =>  [74, 73, 72, 71],
        &ALUProfile::ALU9 =>  [70, 69, 68, 67],
        &ALUProfile::ALU10 => [66, 65, 64, 63],
        &ALUProfile::ALU11 => [62, 61, 60, 59]
    }
}

fn pde_entity_to_shifts(entity: &PDEEntityTypeCfg) -> Vec<u32> {
    match entity {
        &PDEEntityTypeCfg::TS0 => Vec::from(ts_to_shifts(&TSProfile::TS0)),
        &PDEEntityTypeCfg::TS1 => Vec::from(ts_to_shifts(&TSProfile::TS1)),
        &PDEEntityTypeCfg::TS2 => Vec::from(ts_to_shifts(&TSProfile::TS2)),
        &PDEEntityTypeCfg::TS3 => Vec::from(ts_to_shifts(&TSProfile::TS3)),
        &PDEEntityTypeCfg::ALU0  => Vec::from(alu_to_shifts(&ALUProfile::ALU0)),
        &PDEEntityTypeCfg::ALU1  => Vec::from(alu_to_shifts(&ALUProfile::ALU1)),
        &PDEEntityTypeCfg::ALU2  => Vec::from(alu_to_shifts(&ALUProfile::ALU2)),
        &PDEEntityTypeCfg::ALU3  => Vec::from(alu_to_shifts(&ALUProfile::ALU3)),
        &PDEEntityTypeCfg::ALU4  => Vec::from(alu_to_shifts(&ALUProfile::ALU4)),
        &PDEEntityTypeCfg::ALU5  => Vec::from(alu_to_shifts(&ALUProfile::ALU5)),
        &PDEEntityTypeCfg::ALU6  => Vec::from(alu_to_shifts(&ALUProfile::ALU6)),
        &PDEEntityTypeCfg::ALU7  => Vec::from(alu_to_shifts(&ALUProfile::ALU7)),
        &PDEEntityTypeCfg::ALU8  => Vec::from(alu_to_shifts(&ALUProfile::ALU8)),
        &PDEEntityTypeCfg::ALU9  => Vec::from(alu_to_shifts(&ALUProfile::ALU9)),
        &PDEEntityTypeCfg::ALU10 => Vec::from(alu_to_shifts(&ALUProfile::ALU10)),
        &PDEEntityTypeCfg::ALU11 => Vec::from(alu_to_shifts(&ALUProfile::ALU11))
    }
}
//END ENUM CONVERTERS
//DONE THIS WAY INSTEAD OF AS TABLE LOOKUPS BECAUSE IT'S A HAIR FASTER

//GIVEN A LOW LEVEL SWITCH CONFIG, APPLIES THE CONFIG TO THE SWITCH
//ALLOCATES THE SWITCH TABLES, TODO: ALLOW PASSING IN AN EXISTING TABLE
pub fn apply_config(req: &SWCfg) -> Result<(), SOCError> {
    let mut sw = SwitchHardware::new(0);
    sw.init()?;

    //ASSIGNMENTS
    let banks = [Banks::BANK_0, Banks::BANK_1, Banks::BANK_2, Banks::BANK_3];
    let flowtrackers = [Flowtracker::IFT, Flowtracker::MFT, Flowtracker::EFT];
    for (idx, ft) in req.assign.banks.iter().enumerate() {
        sw.bank_mapping.assign_bank(&banks[idx], &flowtrackers[*ft as usize])?;
    }

    let ts_es = [TSProfile::TS0, TSProfile::TS1, TSProfile::TS2, TSProfile::TS3];
    for (idx, ft) in req.assign.ts_engines.iter().enumerate() {
        sw.ts_mapping.assign_ts_engine(&ts_es[idx], &flowtrackers[*ft as usize])?;
    }

    let alus = [
        ALUProfile::ALU0,
        ALUProfile::ALU1,
        ALUProfile::ALU2,
        ALUProfile::ALU3,
        ALUProfile::ALU4,
        ALUProfile::ALU5,
        ALUProfile::ALU6,
        ALUProfile::ALU7,
        ALUProfile::ALU8,
        ALUProfile::ALU9,
        ALUProfile::ALU10,
        ALUProfile::ALU11
    ];

    for (idx, ft) in req.assign.alus.iter().enumerate() {
        sw.alu32_mapping.assign_alu(&alus[idx], &flowtrackers[*ft as usize])?;
    }

    sw.bank_valid_bitmap.set_ft_enabled(&Flowtracker::IFT, req.assign.bank_valid_bm_ift)?;
    sw.bank_valid_bitmap.set_ft_enabled(&Flowtracker::EFT, req.assign.bank_valid_bm_eft)?;

    sw.ft_enable.set_ft_enabled(&Flowtracker::IFT, true)?;
    sw.ft_enable.set_ft_enabled(&Flowtracker::EFT, true)?;
    //END ASSIGNMENTS

    //BEGIN DEFAULT PROFILES
    sw.periodic_export_r.write_config(1000)?;

    let age_out_idx = 1;
    sw.age_out_profile.write_config(age_out_idx, true, 1, false);
    //sw.age_out_profile.write_config(age_out_idx, false, 0, false);
    
    let flow_exceed_idx = 1;
    sw.flow_exceed_profile.write_config(flow_exceed_idx, 25000);

    let collector_profile_idx = 0;
    sw.collector_config.write_config(collector_profile_idx);

    sw.ipfix_hdr_construct.write_config(collector_profile_idx, 0x3e, (
        0x89c240,
        0x830200,
        0x932400,
        0x9b6400,
        0xc2a200,
        0xaac21e,
        0x814200,
        0
    ));

    let static_header_data: [u64; 8] = [
        0x98039bca42c00000,
        0xaabbccdd81000001,
        0x08004500002c0000,
        0x0000ff113a08c0a8,
        0x0004c0a800641f91,
        0xf61800186841000a,
        0x0010000000000000,
        0x0000000000000000
    ];

    sw.ipfix_pkt_build.write_config(0, &static_header_data[0].to_ne_bytes());
    sw.ipfix_pkt_build.write_config(1, &static_header_data[1].to_ne_bytes());
    sw.ipfix_pkt_build.write_config(2, &static_header_data[2].to_ne_bytes());
    sw.ipfix_pkt_build.write_config(3, &static_header_data[3].to_ne_bytes());
    sw.ipfix_pkt_build.write_config(4, &static_header_data[4].to_ne_bytes());
    sw.ipfix_pkt_build.write_config(5, &static_header_data[5].to_ne_bytes());
    sw.ipfix_pkt_build.write_config(6, &static_header_data[6].to_ne_bytes());
    sw.ipfix_pkt_build.write_config(7, &static_header_data[7].to_ne_bytes());

    let session_data_idx = 0;
    let alu_mux_mask_idx = 0;
    let timestamp_profile_idx = 0;
    sw.session_data_mux.write_config(session_data_idx, &vec![
        Extract { offset_16b: 37, mode: ExtractMode::E16Bit, offset_1b: 0} //???
    ]);
    sw.session_data_mask.write_config(session_data_idx, &mut [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0xFF, 0, 0, 0]);

    sw.alu_mux_mask.write_config(alu_mux_mask_idx, &vec![1, 4, 0, 2, 3, 5, 6, 7], 0xFF);

    sw.timestamp_profiles.write_profile(timestamp_profile_idx, 3, 0xFFFF492, 3, 0xFFFF492, 0, 0, 0, 0);

    //END DEFAULT PROFILES

    //BEGIN OUTER FILTER SETUP
    let first_layer_idx = 0;
    sw.ftfp_lts_tcam.write_config(first_layer_idx, 0xe, 0);
    sw.ftfp_lts_sram.write_config(first_layer_idx, 0, 0);
    sw.ftfp_mask.write_config(first_layer_idx);

    //configure mux
    let mut extracts = Vec::new();
    for e in req.filter_mux.iter() {
        extracts.push(Extract {
            offset_16b: e.sel,
            mode: match e.mode { 0 => ExtractMode::E16Bit, 1 => ExtractMode::E8Bit, 2 => ExtractMode::E4Bit, _ => { println!("UNRECOGNIZED MODE: {}", e.mode); return Err(SOCError::INTERNAL)}},
            offset_1b: e.section
        });
    }
    sw.ftfp_mux.write_config(first_layer_idx, &extracts);
    //END OUTER FILTER SETUP

    for query in req.queries.iter() {
        let ftfp_idx = query.filter.row;
        let group_id = query.gid;
        let session_key_idx = query.reduce.row;

        //BEGIN INNER FILTER SETUP
        let mut key = query.filter.filter_key.clone();
        let mut filter = query.filter.filter_mask.clone();
        sw.ftfp_tcam.write_config(ftfp_idx, &mut key, &mut filter);
        sw.ftfp_policy.write_config(ftfp_idx, group_id, false, session_key_idx, session_data_idx, alu_mux_mask_idx, 0);
        //END INNER FILTER SETUP

        //BEGIN KEY SETUP
        let mut reduce_mask = query.reduce.reduce_mask.clone();
        let mut key_extracts = Vec::new();
        for e in query.reduce.reduce_mux.iter() {
            key_extracts.push(Extract {
                offset_16b: e.sel,
                mode: match e.mode { 0 => ExtractMode::E16Bit, 1 => ExtractMode::E8Bit, 2 => ExtractMode::E4Bit, _ => { println!("KEY UNRECOGNIZED MODE: {}", e.mode); return Err(SOCError::INTERNAL)}},
                offset_1b: e.section
            });
        }
        sw.session_key_mux.write_config(session_key_idx, &key_extracts);
        sw.session_key_mask.write_config(session_key_idx, &mut reduce_mask);
        //END KEY SETUP

        //BEGIN GROUP SETUP
        let session_data_type = query.pde_pdd_prof.profile_id;
        sw.kg_group_table.write_config(group_id, age_out_idx, flow_exceed_idx, collector_profile_idx, session_data_type, group_id + 1000);
        let mut alu_vec = Vec::new();
        for alu in query.alus.iter() {
            alu_vec.push((alus[alu.alu_idx as usize], alu.profile_idx));
        }

        match query.eft_alus.as_ref() {
            Some(eft_alus) => {
                for eft_alu in eft_alus.iter() {
                    alu_vec.push((alus[eft_alu.alu_idx as usize], eft_alu.profile_idx));
                }
            },
            None => ()
        }

        sw.dg_group_table.write_group(group_id, &alu_vec, timestamp_profile_idx, collector_profile_idx);
        //END GROUP SETUP

        //BEGIN ALU SETUP
        for alu in query.alus.iter() {
            let prof = ALUConf {
                check_0_attr_select: alu.check_0_attr_select,
                check_1_attr_select: alu.check_1_attr_select,
                check_0_op: check_op_cfg_to_normal(&alu.check_0_op),
                check_1_op: check_op_cfg_to_normal(&alu.check_1_op),
                timestamp_0_trig: alu.timestamp_0_trig,
                timestamp_1_trig: alu.timestamp_1_trig,
                timestamp_2_trig: alu.timestamp_2_trig,
                timestamp_3_trig: alu.timestamp_3_trig,
                update_attr_select: alu.update_attr_select,
                update_op: update_op_cfg_to_normal(&alu.update_op),
                update_op_param: alu.update_op_param,
                export_op: alu.export_op,
                export_check_mode: alu.export_check_mode,
                clear_on_export: alu.clear_on_export,
                clear_on_periodic_export: alu.clear_on_periodic_export,
                load_trigger: alu.load_trigger,
                spec_check_0_thresh: alu.spec_check_0_thresh,
                spec_check_1_thresh: alu.spec_check_1_thresh,
                spec_first_delay_op: delay_op_cfg_to_normal(&alu.spec_first_delay_op),
                spec_second_delay_op: delay_op_cfg_to_normal(&alu.spec_second_delay_op),
                spec_delay_mode: delay_mode_cfg_to_normal(&alu.spec_delay_mode),
                spec_delay_gran: alu.spec_delay_gran,
                spec_delay_offset: alu.spec_delay_offset,
                spec_check_attr_0_ts_select: delay_attr_cfg_to_normal(&alu.spec_check_attr_0_ts_select),
                spec_check_attr_1_ts_select: delay_attr_cfg_to_normal(&alu.spec_check_attr_1_ts_select),
                spec_update_attr_1_ts_select: delay_attr_cfg_to_normal(&alu.spec_update_attr_1_ts_select),
                spec_export_thresh: alu.spec_export_thresh,
                spec_check_attr_0_gran: attr_gran_cfg_to_normal(&alu.spec_check_attr_0_gran),
                spec_check_attr_1_gran: attr_gran_cfg_to_normal(&alu.spec_check_attr_1_gran),
                replace_check_0_byte_count: alu.replace_check_0_byte_count,
                replace_check_1_byte_count: alu.replace_check_1_byte_count,
                replace_update_byte_count: alu.replace_update_byte_count

            };

            sw.alu32profiles[alu.alu_idx as usize].write_alu_profile(alu.profile_idx, &prof);
        }
        //END ALU SETUP

        //BEGIN PDD/PDE SETUP
        //let alu_profiles: Vec<ALUProfile> = query.alus.iter().map(|a| { alus[a.alu_idx as usize] }).collect();
        //let ts_profiles: 
        let alu_profiles = query.pde_pdd_prof.pdd_profile.alus.iter().enumerate().filter(|(_idx, enabled)| **enabled == true).map(|(idx, _enabled)|{
            match idx {
                0 => ALUProfile::ALU0,
                1 => ALUProfile::ALU1,
                2 => ALUProfile::ALU2,
                3 => ALUProfile::ALU3,
                4 => ALUProfile::ALU4,
                5 => ALUProfile::ALU5,
                6 => ALUProfile::ALU6,
                7 => ALUProfile::ALU7,
                8 => ALUProfile::ALU8,
                9 => ALUProfile::ALU9,
                10 => ALUProfile::ALU10,
                11 => ALUProfile::ALU11,
                _ => ALUProfile::ALU0
            }
        }).collect();

        let ts_profiles = query.pde_pdd_prof.pdd_profile.ts_engines.iter().enumerate().filter(|(_idx, enabled)| **enabled == true).map(|(idx, _enabled)|{
            match idx {
                0 => TSProfile::TS0,
                1 => TSProfile::TS1,
                2 => TSProfile::TS2,
                3 => TSProfile::TS3,
                _ => TSProfile::TS0
            }
        }).collect();

        sw.pdd_profile_ift.write_config(session_data_type, &alu_profiles, &ts_profiles);

        let pde_shifts = query.pde_pdd_prof.pde_profile.iter().map(|p| {
            let src_offset = pde_entity_to_shifts(&p.source);
            if src_offset.len() != p.dsts.len() {
                println!("ERROR, WRONG NUMBER OF DSTS FOR ENTITY {:?}", p.source);
                panic!("can't set up pde profile");
            }
            let mut shifts = Vec::new();
            for (s, d) in src_offset.iter().zip(p.dsts.iter()) {
                shifts.push(ByteShift{ byte_in_storage_bank: *d as usize, destination_byte: *s}); //i have all the wording reversed in the variables, the byte in the storage bank (dst) gets written to the byte in the alu (src)
            }
            shifts
        }).into_iter().flatten().collect();


        sw.pde_profiles_ift.write_profile(session_data_type, &pde_shifts);

        let export_shifts = query.pde_pdd_prof.export_pde.iter().map(|p| {
            let src_offset = pde_entity_to_shifts(&p.source);
            if src_offset.len() != p.dsts.len() {
                println!("ERROR, WRONG NUMBER OF DSTS FOR ENTITY {:?}", p.source);
                panic!("can't set up export profile");
            }
            let mut shifts = Vec::new();
            for (s, d) in src_offset.iter().zip(p.dsts.iter()) {
                shifts.push(ByteShift{ byte_in_storage_bank: *d as usize, destination_byte: *s}); //i have all the wording reversed in the variables, the byte in the storage bank (dst) gets written to the byte in the alu (src)
            }
            shifts
        }).into_iter().flatten().collect();

        sw.export_pde_profiles_ift.write_profile(session_data_type, &export_shifts);
        // let export_shifts = query.pde_pdd_prof.export_pde.iter().map(|p|{
        //     ByteShift { byte_in_storage_bank: p.sa_idx as usize, destination_byte: p.sa_val}
        // }).collect();
        // sw.export_pde_profiles_ift.write_profile(session_data_type, &export_shifts);

        println!("EFT PDD: {:#?}", query.eft_pde_pdd_prof);
        println!("EFT ALUS: {:#?}", query.eft_alus);
        if query.eft_alus.is_some() && query.eft_pde_pdd_prof.is_some() {
            for alu in query.eft_alus.as_ref().unwrap().iter() {
                let eft_prof = ALUConf {
                    check_0_attr_select: alu.check_0_attr_select,
                    check_1_attr_select: alu.check_1_attr_select,
                    check_0_op: check_op_cfg_to_normal(&alu.check_0_op),
                    check_1_op: check_op_cfg_to_normal(&alu.check_1_op),
                    timestamp_0_trig: alu.timestamp_0_trig,
                    timestamp_1_trig: alu.timestamp_1_trig,
                    timestamp_2_trig: alu.timestamp_2_trig,
                    timestamp_3_trig: alu.timestamp_3_trig,
                    update_attr_select: alu.update_attr_select,
                    update_op: update_op_cfg_to_normal(&alu.update_op),
                    update_op_param: alu.update_op_param,
                    export_op: alu.export_op,
                    export_check_mode: alu.export_check_mode,
                    clear_on_export: alu.clear_on_export,
                    clear_on_periodic_export: alu.clear_on_periodic_export,
                    load_trigger: alu.load_trigger,
                    spec_check_0_thresh: alu.spec_check_0_thresh,
                    spec_check_1_thresh: alu.spec_check_1_thresh,
                    spec_first_delay_op: delay_op_cfg_to_normal(&alu.spec_first_delay_op),
                    spec_second_delay_op: delay_op_cfg_to_normal(&alu.spec_second_delay_op),
                    spec_delay_mode: delay_mode_cfg_to_normal(&alu.spec_delay_mode),
                    spec_delay_gran: alu.spec_delay_gran,
                    spec_delay_offset: alu.spec_delay_offset,
                    spec_check_attr_0_ts_select: delay_attr_cfg_to_normal(&alu.spec_check_attr_0_ts_select),
                    spec_check_attr_1_ts_select: delay_attr_cfg_to_normal(&alu.spec_check_attr_1_ts_select),
                    spec_update_attr_1_ts_select: delay_attr_cfg_to_normal(&alu.spec_update_attr_1_ts_select),
                    spec_export_thresh: alu.spec_export_thresh,
                    spec_check_attr_0_gran: attr_gran_cfg_to_normal(&alu.spec_check_attr_0_gran),
                    spec_check_attr_1_gran: attr_gran_cfg_to_normal(&alu.spec_check_attr_1_gran),
                    replace_check_0_byte_count: alu.replace_check_0_byte_count,
                    replace_check_1_byte_count: alu.replace_check_1_byte_count,
                    replace_update_byte_count: alu.replace_update_byte_count
    
                };
    
                sw.alu32profiles[alu.alu_idx as usize].write_alu_profile(alu.profile_idx, &eft_prof);
            }

            let eft_alu_profiles = query.eft_pde_pdd_prof.as_ref().unwrap().pdd_profile.alus.iter().enumerate().filter(|(_idx, enabled)| **enabled == true).map(|(idx, _enabled)|{
                match idx {
                    0 => ALUProfile::ALU0,
                    1 => ALUProfile::ALU1,
                    2 => ALUProfile::ALU2,
                    3 => ALUProfile::ALU3,
                    4 => ALUProfile::ALU4,
                    5 => ALUProfile::ALU5,
                    6 => ALUProfile::ALU6,
                    7 => ALUProfile::ALU7,
                    8 => ALUProfile::ALU8,
                    9 => ALUProfile::ALU9,
                    10 => ALUProfile::ALU10,
                    11 => ALUProfile::ALU11,
                    _ => ALUProfile::ALU0
                }
            }).collect();
    
            let eft_ts_profiles = query.eft_pde_pdd_prof.as_ref().unwrap().pdd_profile.ts_engines.iter().enumerate().filter(|(_idx, enabled)| **enabled == true).map(|(idx, _enabled)|{
                match idx {
                    0 => TSProfile::TS0,
                    1 => TSProfile::TS1,
                    2 => TSProfile::TS2,
                    3 => TSProfile::TS3,
                    _ => TSProfile::TS0
                }
            }).collect();
    
            sw.pdd_profile_eft.write_config(session_data_type, &eft_alu_profiles, &eft_ts_profiles);
    
            let eft_pde_shifts = query.eft_pde_pdd_prof.as_ref().unwrap().pde_profile.iter().map(|p| {
                let src_offset = pde_entity_to_shifts(&p.source);
                if src_offset.len() != p.dsts.len() {
                    println!("ERROR, WRONG NUMBER OF DSTS FOR ENTITY {:?}", p.source);
                    panic!("can't set up pde profile");
                }
                let mut shifts = Vec::new();
                for (s, d) in src_offset.iter().zip(p.dsts.iter()) {
                    shifts.push(ByteShift{ byte_in_storage_bank: *d as usize, destination_byte: *s}); //i have all the wording reversed in the variables, the byte in the storage bank (dst) gets written to the byte in the alu (src)
                }
                shifts
            }).into_iter().flatten().collect();
    
    
            sw.pde_profiles_eft.write_profile(session_data_type, &eft_pde_shifts);
    
            let eft_export_shifts = query.eft_pde_pdd_prof.as_ref().unwrap().export_pde.iter().map(|p| {
                let src_offset = pde_entity_to_shifts(&p.source);
                if src_offset.len() != p.dsts.len() {
                    println!("ERROR, WRONG NUMBER OF DSTS FOR ENTITY {:?}", p.source);
                    panic!("can't set up export profile");
                }
                let mut shifts = Vec::new();
                for (s, d) in src_offset.iter().zip(p.dsts.iter()) {
                    shifts.push(ByteShift{ byte_in_storage_bank: *d as usize, destination_byte: *s}); //i have all the wording reversed in the variables, the byte in the storage bank (dst) gets written to the byte in the alu (src)
                }
                shifts
            }).into_iter().flatten().collect();
    
            sw.export_pde_profiles_eft.write_profile(session_data_type, &eft_export_shifts);
            // let export_shifts_eft = query.eft_pde_pdd_prof.as_ref().unwrap().export_pde.iter().map(|p|{
            //     ByteShift { byte_in_storage_bank: p.sa_idx as usize, destination_byte: p.sa_val}
            // }).collect();
            // sw.export_pde_profiles_ift.write_profile(session_data_type, &export_shifts_eft);
        }
        //DONE
    }

    sw.apply_config()?;

    Ok(())
}
#[derive(Debug)]
pub struct BSC_DG_GroupALU32Profile_info {
    pub alu_idx: u32,
    pub profile_idx: u32,
    pub check_0_attr_select: u32,
    pub check_0_op: u32,
    pub check_1_attr_select: u32,
    pub check_1_op: u32,
    pub timestamp_2_trig: bool,
    pub timestamp_3_trig: bool,
    pub update_attr_select: u32,
    pub update_op: u32,
    pub update_op_param: u32,
    pub export_op: u32,
    pub export_check_mode: u32,
    pub clear_on_export: bool,
    pub clear_on_periodic_export: bool,
    pub load_trigger: u32,
    pub timestamp_0_trig: bool,
    pub timestamp_1_trig: bool,
    pub replace_check_0_byte_count: bool,
    pub replace_check_1_byte_count: bool,
    pub replace_update_byte_count: bool,
    
    //specific controls
    pub spec_check_0_thresh: u32,
    pub spec_check_1_thresh: u32,
    pub spec_export_thresh: u32,
    pub spec_first_delay_op: u32,
    pub spec_second_delay_op: u32,
    pub spec_delay_mode: u32,
    pub spec_delay_gran: u32,
    pub spec_delay_offset: u32,
    pub spec_check_attr_0_ts_select: u32,
    pub spec_check_attr_1_ts_select: u32,
    pub spec_update_attr_1_ts_select: u32
}

#[derive(Debug)]
pub struct BSC_DG_GroupTable_info {
    pub idx: u32,
    pub group_valid: bool,
    pub timestamp_profile_idx: u32,
    pub collector_id: u32,
    pub alu_profile_idxs: [u32; 12]
}

#[derive(Debug)]
pub struct BSC_DG_GroupTimestampProfileTable_info {
    pub idx: u32,
    pub ts_select_0: u32,
    pub data_0: u32,
    pub ts_select_1: u32,
    pub data_1: u32,
    pub ts_select_2: u32,
    pub data_2: u32,
    pub ts_select_3: u32,
    pub data_3: u32
}

#[derive(Debug)]
pub struct BSC_DT_PDEProfileTable_info {
    pub idx: u32,
    pub ft: u32,
    pub shift_amounts: [(u32, u32); 64] //(SHIFT_AMOUNT_BYTE_n, shift_value)
}

#[derive(Debug)]
pub struct BSC_DT_ExportPDEProfileTable_info {
    pub idx: u32,
    pub ft: u32,
    pub shift_amounts: [(u32, u32); 64] //(SHIFT_AMOUNT_BYTE_n, shift_value)
}

#[derive(Debug)]
pub struct BSC_EX_CollectorConfig_info {
    pub idx: u32,
    pub time_interval_enable: bool,
    pub ra_enable: bool,
    pub pkt_build_enable: bool,
    pub max_records_per_pkt: u32,
    pub max_ptr: u32,
    pub enable: bool,
    pub time_interval: u32
}

#[derive(Debug)]
pub struct BSC_EX_HdrConstructCfg_info {
    pub idx: u32,
    pub hdr_byte_len: u32,
    pub hdr_update_ctrl: [u32; 8]
}

#[derive(Debug)]
pub struct BSC_KG_AgeOutProfile_info {
    pub idx: u32,
    pub enable: bool,
    pub interval: u32,
    pub export_age_out: bool,
}

#[derive(Debug)]
pub struct BSC_KG_FlowExceedProfile_info {
    pub idx: u32,
    pub flow_exceed_profile: u32
}

#[derive(Debug)]
pub struct BSC_KG_GroupTable_info {
    pub idx: u32,
    pub age_out_profile_idx: u32,
    pub flow_exceed_profile_idx: u32,
    pub collector_idx: u32,
    pub export_learn: bool,
    pub session_data_type: u32,
    pub set_id: u32,
    pub group_valid: bool,
    pub periodic_export_enabled: bool
}

#[derive(Debug)]
pub struct BSC_PolicyActionProfile_info {
    pub idx: u32,
    pub ft: u32,
    pub alus: [bool; 12],
    pub ts_engines: [bool; 4]
}

#[derive(Debug)]
pub struct BSK_ALUDataLTSMuxCtrlPlusMask_info {
    pub idx: u32,
    pub extract_cs: [u32; 16],
    pub mask: u16
}

#[derive(Debug)]
pub struct BSK_FTFP_LTS_LogicalTblSelSRAM_info {
    pub idx: u32,
    pub group_key_type: u32,
    pub delay_mode: u32,
    pub group_lookup_enable: bool
}

#[derive(Debug)]
pub struct BSK_FTFP_LTS_LogicalTblSelTCAM_info {
    pub idx: u32,
    pub valid: u32,
    pub parser1_l4_valid: bool,
    pub parser1_l4_valid_mask: u32,
    pub l3_type_mask: u32,
    pub hve_results_1_z1_uc_mask: u32,
    pub hve_results_1_z1_uc: bool,
    pub hve_results_1_z1_bc_mask: u32
}

#[derive(Debug)]
pub struct BSK_FTFP_LTS_Mask0_info {
    pub idx: u32,
    pub mask: [u8; 16]
}

#[derive(Debug)]
pub struct BSK_FTFP_LTS_MuxCtrl0_info {
    pub idx: u32,
    pub extracts: [(u32, u32, u32); 10] //(sel, mode, section)
}

#[derive(Debug)]
pub struct BSK_FTFP_Policy_info {
    pub idx: u32,
    pub group_id: u32,
    pub bidir_flow: bool,
    pub session_key_lts_profile: u32,
    pub session_data_lts_profile: u32,
    pub alu_data_lts_profile: u32,
    pub session_key_type: u32,
    pub do_not_ft: bool,
    pub learn_diable: bool,
    pub uflow_fields_valid: bool
}

#[derive(Debug)]
pub struct BSK_FTFP_TCAM_info {
    pub idx: u32,
    pub key: [u8; 24],
    pub mask: [u8; 24],
    pub key_type: u32,
    pub mask_type: u32,
    pub valid: u32
}

#[derive(Debug)]
pub struct BSK_SessionDataLTSMask0_info {
    pub idx: u32,
    pub mask: [u8; 16]
}

#[derive(Debug)]
pub struct BSK_SessionDataLTSMuxCtrl0_info {
    pub idx: u32,
    pub extracts: [(u32, u32, u32); 10] //(sel, mode, section)
}

#[derive(Debug)]
pub struct BSK_SessionKeyLTSMask0_info {
    pub idx: u32,
    pub mask: [u8; 16]
}

#[derive(Debug)]
pub struct BSK_SessionKeyLTSMuxCtrl0_info {
    pub idx: u32,
    pub extracts: [(u32, u32, u32); 10] //(sel, mode, section)
}

#[derive(Debug)]
pub struct BSC_AG_PeriodicExport_info {
    pub export_period: u32
}

#[derive(Debug)]
pub struct BSC_DT_ALU32ToInstanceMappingControl_info {
    pub alu_to_ft: [(u32, u32); 10],
}

#[derive(Debug)]
pub struct BSC_DT_BankToInstanceMappingControl_info {
    pub bank_0_ft: u32,
    pub bank_1_ft: u32,
    pub bank_2_ft: u32,
    pub bank_3_ft: u32
}

#[derive(Debug)]
pub struct BSC_DT_ExportBankValidBitmap_info {
    pub ft_0_bank_valid: u32,
    pub ft_1_bank_valid: u32,
    pub ft_2_bank_valid: u32,
}

#[derive(Debug)]
pub struct BSC_DT_TimestampEngineToInstanceMappingControl_info {
    pub ts_0_ft: u32,
    pub ts_1_ft: u32,
    pub ts_2_ft: u32,
    pub ts_3_ft: u32
}

#[derive(Debug)]
pub struct BSC_EX_BroadscanRAFTEnable_info {
    pub ft_0_enable: bool,
    pub ft_1_enable: bool,
    pub ft_2_enable: bool
}



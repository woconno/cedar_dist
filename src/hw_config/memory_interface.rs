use crate::hw_config::wrapper::{SOC_BLOCK_ALL, SOC_BLOCK_ANY};
use crate::hw_config::wrapper::{
    soc_format_field32_get, 
    soc_format_field32_set,
    soc_format_field_set,
    soc_mem_field32_get,
    soc_mem_field32_set, 
    soc_mem_field_set,
    soc_mem_field_get,
    soc_mem_write, 
    soc_mem_write_range, 
    soc_mem_read, 
    soc_mem_read_range,
    soc_reg_get, 
    soc_format_field_get,
    soc_reg64_field32_set, 
    soc_reg_set, 
    bcmi_ft_init
};

use core::fmt;
use std::ffi::c_void;

//LOCAL CONSTS
const MEM_BLOCK_ANY: i32 = SOC_BLOCK_ANY;
const MEM_BLOCK_ALL: i32 = SOC_BLOCK_ALL;
const REG_PORT_ANY: i32 = -10;


//UTILITIES
pub unsafe fn any_as_u8_slice<T: Sized>(p: &T) -> &[u8] {
    ::std::slice::from_raw_parts(
        (p as *const T) as *const u8,
        ::std::mem::size_of::<T>(),
    )
}

pub fn memory_to_string(mem: u32) -> String {
    let unknown_str = format!("UNKNOWN MEM: {}", mem);
    let str = match mem {
        BSK_FTFP_TCAMm => "BSK_FTFP_TCAMm",
        BSK_FTFP_POLICYm => "BSK_FTFP_POLICYm",
        BSK_SESSION_KEY_LTS_MUX_CTRL_0m => "BSK_SESSION_KEY_LTS_MUX_CTRL_0m",
        BSK_SESSION_KEY_LTS_MASK_0m => "BSK_SESSION_KEY_LTS_MASK_0m",
        BSK_SESSION_DATA_LTS_MUX_CTRL_0m => "BSK_SESSION_DATA_LTS_MUX_CTRL_0m",
        BSK_SESSION_DATA_LTS_MASK_0m => "BSK_SESSION_DATA_LTS_MASK_0m",
        BSC_KG_GROUP_TABLEm => "BSC_KG_GROUP_TABLEm",
        BSC_DG_GROUP_TABLEm => "BSC_DG_GROUP_TABLEm",
        BSC_KG_AGE_OUT_PROFILEm => "BSC_KG_AGE_OUT_PROFILEm",
        BSC_KG_FLOW_EXCEED_PROFILEm => "BSC_KG_FLOW_EXCEED_PROFILEm",
        BSC_KG_FLOW_LEARNED_COUNTERm => "BSC_KG_FLOW_LEARNED_COUNTERm",
        BSK_FTFP_LTS_LOGICAL_TBL_SEL_TCAMm => "BSK_FTFP_LTS_LOGICAL_TBL_SEL_TCAMm",
        BSK_FTFP_LTS_LOGICAL_TBL_SEL_SRAMm => "BSK_FTFP_LTS_LOGICAL_TBL_SEL_SRAMm",
        BSK_FTFP_LTS_MUX_CTRL_0m => "BSK_FTFP_LTS_MUX_CTRL_0m",
        BSK_FTFP_LTS_MASK_0m => "BSK_FTFP_LTS_MASK_0m",
        BSC_DG_GROUP_ALU32_PROFILE_0m => "BSC_DG_GROUP_ALU32_PROFILE_0m",
        BSC_DG_GROUP_ALU32_PROFILE_1m => "BSC_DG_GROUP_ALU32_PROFILE_1m",
        BSC_DG_GROUP_ALU32_PROFILE_2m => "BSC_DG_GROUP_ALU32_PROFILE_2m",
        BSC_DG_GROUP_ALU32_PROFILE_3m => "BSC_DG_GROUP_ALU32_PROFILE_3m",
        BSC_DG_GROUP_ALU32_PROFILE_4m => "BSC_DG_GROUP_ALU32_PROFILE_4m",
        BSC_DT_PDE_PROFILE_0m => "BSC_DT_PDE_PROFILE_0m",
        BSC_DT_EXPORT_PDE_PROFILE_0m => "BSC_DT_EXPORT_PDE_PROFILE_0m",
        BSD_POLICY_ACTION_PROFILE_0m => "BSD_POLICY_ACTION_PROFILE_0m",
        BSC_EX_COLLECTOR_IPFIX_PACKET_BUILDm => "BSC_EX_COLLECTOR_IPFIX_PACKET_BUILDm",
        BSC_EX_COLLECTOR_CONFIGm => "BSC_EX_COLLECTOR_CONFIGm",
        BSC_EX_HDR_CONSTRUCT_CFGm => "BSC_EX_HDR_CONSTRUCT_CFGm",

        _ => unknown_str.as_str()
    };
    String::from(str)
}

#[derive(Debug)]
pub enum SOCError {
    SOC_CODE(i32),
    INTERNAL,
    ALLOC_FAILED
}

impl std::error::Error for SOCError {}

impl std::fmt::Display for SOCError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> fmt::Result {
        match self {
            SOCError::SOC_CODE(code) => write!(f, "SOC MEMORY ERROR, CODE={}", code),
            SOCError::INTERNAL => write!(f, "INTERNAL ERROR IN SOC INTERFACE CODE (NOT PROBLEM WITH SOC)"),
            SOCError::ALLOC_FAILED => write!(f, "SOC FAILED TO ALLOCATE MEMORY (out of memory???)")
        }
    }
}

//END UTILITIES

//BEGIN MEMORY FUNCTIONS

//memory is allocated on the CPU corresponding to swith hardware memory, the soc_mem_set and format_set functions are used to write values in to it, the mem_write and mem_write_range
//functions are used to write it to the switch hardware.
//mem_read can read values from switch hardware, and then soc_mem_get and format_get functions can extract specific fields

//MEMORY DMA ACCESS
//WRITES
pub fn bsca_soc_mem_write<T: Sized>(unit: u32, mem: u32, index: u32, data: &T) -> Result<(), SOCError> {
    unsafe {
        let mut entry_data = any_as_u8_slice(data).to_owned();
        let ret = soc_mem_write(unit as i32, mem as i32, MEM_BLOCK_ANY, index as i32, entry_data.as_mut_ptr() as *mut c_void);
        if ret == 0 {
            Ok(())
        }
        else {
            Err(SOCError::SOC_CODE(ret))
        }
    }
}

pub fn bsca_soc_mem_write_range<T: Sized + std::fmt::Debug>(unit: u32, mem: u32, index: usize, data: &mut [T]) -> Result<(), SOCError> {
    if data.len() == 0 {
        println!("MEM WRITE RANGE ASKED TO WRITE 0 LENGTH OBJECT FOR MEMORY {}", memory_to_string(mem as u32));
        return Ok(());
    }
    println!("WRITING TO {}: {:?}", memory_to_string(mem as u32), data);
    unsafe {
        let entry_data = data.as_mut_ptr();
        let rv = soc_mem_write_range(unit as i32, mem as i32, MEM_BLOCK_ANY, index as i32, index as i32 + data.len() as i32 - 1, entry_data as *mut c_void);
        if rv != 0 {
            return Err(SOCError::SOC_CODE(rv));
        }
    }

    return Ok(());
}

//READS

pub fn bsca_soc_mem_read<T: Sized>(unit: u32, mem: u32, index: u32, data: &mut T) -> Result<(), SOCError>{
    unsafe {
        let ptr = std::ptr::addr_of_mut!(*data);
        let val = soc_mem_read(unit as i32, mem as i32, MEM_BLOCK_ANY, index as i32, ptr as *mut c_void);
        if val != 0 {
            return Err(SOCError::SOC_CODE(val));
        }
    }
    Ok(())
}

pub fn bsca_soc_mem_read_range<T: Sized>(unit: u32, mem: u32, start_index: usize, data: &mut [T]) -> Result<(), SOCError> {
    unsafe {
        let ptr = std::ptr::addr_of_mut!(*data);
        let val = soc_mem_read_range(unit as i32, mem as i32, MEM_BLOCK_ANY, start_index as i32, start_index as i32 + data.len() as i32 - 1, ptr as *mut c_void);
        if val != 0 {
            return Err(SOCError::SOC_CODE(val));
        }
    }
    Ok(())
}

//REGISTERS
pub fn bsca_reg64_field32_read_set_write(unit: u32, reg: u32, field: u32, value: u32) -> Result<(), SOCError>{
    let mut val: u64 = 0;
    unsafe {
        let ptr = std::ptr::addr_of_mut!(val);
        let get_rv = soc_reg_get(unit as i32, reg as i32, REG_PORT_ANY, 0, ptr);
        if get_rv != 0 { return Err(SOCError::SOC_CODE(get_rv)); }
        soc_reg64_field32_set(unit as i32, reg as i32, ptr, field as i32, value);
        let set_rv = soc_reg_set(unit as i32, reg as i32, REG_PORT_ANY, 0, val);
        if set_rv != 0 { return Err(SOCError::SOC_CODE(set_rv)); }
    }

    Ok(())
}

pub fn bsca_reg64_read(unit: u32, reg: u32) -> Result<u64, SOCError> {
    let mut val: u64 = 0;
    unsafe {
        let ptr = std::ptr::addr_of_mut!(val);
        let get_rv = soc_reg_get(unit as i32, reg as i32, REG_PORT_ANY, 0, ptr);
        if get_rv != 0 { return Err(SOCError::SOC_CODE(get_rv)); }
    }
    Ok(val)
}

//END DMA STUFF


//MEMORY BUILDING

//MEMORY 32
pub fn bsca_soc_mem_field32_set<T: Sized>(unit: u32, mem: u32, obj: &mut T, field: u32, value: u32){
    unsafe {
        let ptr = obj as *mut T;
        soc_mem_field32_set(unit as i32, mem as i32, ptr as *mut c_void, field as i32, value);
    }
}

pub fn bsca_soc_mem_field32_get<T: Sized>(unit: u32, mem: u32, obj: &T, field: u32) -> u32 {
    unsafe {
        let ptr = std::ptr::addr_of!(*obj);
        return soc_mem_field32_get(unit as i32, mem as i32, ptr as *const c_void, field as i32);
    }
}

//FORMAT 32
pub fn bsca_format_field32_set<T: Sized>(unit: u32, fmt: u32, obj: &mut T, field: u32, value: u32){
    unsafe {
        let ptr = obj as *mut T;
        soc_format_field32_set(unit as i32, fmt as i32, ptr as *mut c_void, field as i32, value);
    }
}

pub fn bsca_format_field32_get<T: Sized>(unit: u32, mem: u32, field: u32, data: &T) -> u32{
    unsafe {
        let data_ptr = std::ptr::addr_of!(*data);
        return soc_format_field32_get(unit as i32, mem as i32, data_ptr as *const c_void, field as i32);
    }
}

//FORMAT
pub fn bsca_format_field_set<T: Sized, V: Sized>(unit: u32, fmt: u32, obj: &mut T, field: u32, value: &mut V){
    unsafe {
        let ptr = std::ptr::addr_of_mut!(*obj) as *mut u32;
        let val_ptr = std::ptr::addr_of_mut!(*value) as *mut u32;
        soc_format_field_set(unit as i32, fmt as i32, ptr, field as i32, val_ptr);
    }
}

pub fn bsca_format_field_get<T: Sized, V: Sized>(unit: u32, fmt: u32, obj: &T, field: u32, dst: &mut V){
    unsafe {
        let ptr = std::ptr::addr_of!(*obj) as *mut u32;
        let dst_ptr = std::ptr::addr_of_mut!(*dst) as *mut u32;
        soc_format_field_get(unit as i32, fmt as i32, ptr, field as i32, dst_ptr);
    }
}

//MEMORY
pub fn bsca_soc_mem_field_set<T: Sized, V: Sized>(unit: u32, fmt: u32, obj: &mut T, field: u32, value: &mut V){
    unsafe {
        let ptr = std::ptr::addr_of_mut!(*obj) as *mut u32;
        let val_ptr = std::ptr::addr_of_mut!(*value) as *mut u32;
        soc_mem_field_set(unit as i32, fmt as i32, ptr, field as i32, val_ptr);
    }
}

pub fn bsca_soc_mem_field_get<T: Sized, V: Sized>(unit: u32, fmt: u32, obj: &T, field: u32, dst: &mut V){
    unsafe {
        let ptr = std::ptr::addr_of!(*obj) as *mut u32;
        let dst_ptr = std::ptr::addr_of_mut!(*dst) as *mut u32;
        soc_mem_field_get(unit as i32, fmt as i32, ptr, field as i32, dst_ptr);
    }
}


//DOES OVERALL INIT FOR FLOWTRACKER
pub fn bsca_ft_init(unit: u32) -> Result<(), SOCError> {
    let result = unsafe { bcmi_ft_init(unit as i32) };
    if result != 0 {
        return Err(SOCError::SOC_CODE(result));
    }
    Ok(())
}



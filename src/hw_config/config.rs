use std::ptr::addr_of_mut;
use crate::hw_config::wrapper::{bcm_time_interface_add, bcm_time_interface_t_init, bcm_time_interface_t, BCM_TIME_ENABLE};

pub fn enable_timer(unit: u32) -> i32 {
    let mut ti: bcm_time_interface_t = bcm_time_interface_t {
        flags: 0,
        offset: super::wrapper::bcm_time_spec_s { isnegative: 0, seconds: 0, nanoseconds: 0 },
        drift: super::wrapper::bcm_time_spec_s { isnegative: 0, seconds: 0, nanoseconds: 0 },
        id: 0,
        accuracy: super::wrapper::bcm_time_spec_s { isnegative: 0, seconds: 0, nanoseconds: 0},
        heartbeat_hz: 0,
        clk_resolution: 0,
        bitclock_hz: 0,
        status: 0,
        ntp_offset: super::wrapper::bcm_time_spec_s { isnegative: 0, seconds: 0, nanoseconds: 0 },
        bs_time: super::wrapper::bcm_time_spec_s { isnegative: 0, seconds: 0, nanoseconds: 0 },
    };
    
    unsafe { bcm_time_interface_t_init(addr_of_mut!(ti)) };
    ti.flags |= BCM_TIME_ENABLE;

    println!("TIME STRUCT: {:#?}", ti);
    unsafe { bcm_time_interface_add(unit as i32, addr_of_mut!(ti)) }
}
use crate::hw_config::memory_interface::{
    bsca_soc_mem_write,
    bsca_soc_mem_read,
    bsca_reg64_read
};

use crate::hw_config::memory_interface::SOCError;

pub trait HWArrayTable<T: Sized> {
    //write configuration to the switch using the list of modified indexes (fast)
    //TODO: consider wiping the modified indexes here
    fn apply(&self) -> Result<(), SOCError> {
        let data = self.get_data();
        let indexes = self.get_indexes();
        let (unit, mem, _max_size) = self.get_info();


        for idx in indexes {
            bsca_soc_mem_write(unit, mem, *idx, &data[*idx as usize])?;
        }

        Ok(())
    }

    //write the entire state to the switch (including empty config rows, slow)
    //WARNING: the switch pre-populates some TCAM match entries (namely BSK_FTFP_TCAM 0-3), this will overwrite them unless you read them in to the config first
    fn apply_all(&self) -> Result<(), SOCError> {
        let data = self.get_data();
        let (unit, mem, _max_size) = self.get_info();

        for idx in 0..data.len() {
            bsca_soc_mem_write(unit, mem, idx as u32, &data[idx])?;
        }
        
        Ok(())
    }

    //reads the provided list of indexes from the switch in to the configuration object
    //if read_indexes == None, will read all indexes for object
    fn read(&mut self, read_indexes: Option<&Vec<u32>>) -> Result<(), SOCError> {
        let (unit, mem, max_size) = self.get_info();

        let tmp_indexes = match read_indexes { Some(_) => Vec::<u32>::new(), None => (0..max_size).collect()};

        let indexes = match read_indexes {
            Some(ri) => ri,
            None => &tmp_indexes
        };

        let data = self.get_data_mut();
        for idx in indexes {
            bsca_soc_mem_read(unit, mem, *idx, &mut data[*idx as usize])?;
        }
        Ok(())
    }

    //gets a pointer to the table's CPU memory data location
    fn get_data(&self) -> &[T];

    //gets a mutable pointer to the tables CPU memory data location
    fn get_data_mut(&mut self) -> &mut [T];

    //gets the list of modified indexes in this table
    //modified indexes are set manually when a write_* call is made
    //if you directly modify the table, the list of modified indexes is not updated
    fn get_indexes(&self) -> &Vec<u32>;

    //returns (unit, swithc_mem_id, table_size)
    fn get_info(&self) -> (u32, u32, u32);

    //zeros the table and applys it
    fn wipe(&mut self) -> Result<(), SOCError>;
}

pub trait HWRegister {
    fn read(&mut self) -> Result<(), SOCError> {
        let unit = self.get_unit();
        let rid = self.get_reg_id();
        let data = self.get_data_mut();
        *data = bsca_reg64_read(unit, rid)?;
        Ok(())
    }

    fn get_data_mut(&mut self) -> &mut u64;

    fn get_reg_id(&self) -> u32;

    fn get_unit(&self) -> u32;
}

#[derive(Clone, Copy)]
pub enum ALUProfile {
    ALU0,
    ALU1,
    ALU2,
    ALU3,
    ALU4,
    ALU5,
    ALU6,
    ALU7,
    ALU8,
    ALU9,
    ALU10,
    ALU11
}

pub enum Flowtracker {
    IFT,
    MFT,
    EFT
}

pub enum TSProfile {
    TS0,
    TS1,
    TS2,
    TS3
}

pub enum ExtractMode {
    E16Bit,
    E8Bit,
    E4Bit
}

//4,5,6,7 only used in double wide mode
pub enum Banks {
    BANK_0,
    BANK_1,
    BANK_2,
    BANK_3,
    BANK_4,
    BANK_5,
    BANK_6,
    BANK_7
}

pub struct Extract {
    pub offset_1b: u32,
    pub mode: ExtractMode,
    pub offset_16b: u32
}

pub mod bsc_dg_group_alu32_profile_table {

    //ALU IMPORTS
    use crate::hw_config::wrapper::{
        bsc_dg_group_alu32_profile_0_entry_t,
        BSC_DG_GROUP_ALU32_PROFILE_0m,
        BSC_DG_GROUP_ALU32_PROFILE_1m,
        BSC_DG_GROUP_ALU32_PROFILE_2m,
        BSC_DG_GROUP_ALU32_PROFILE_3m,
        BSC_DG_GROUP_ALU32_PROFILE_4m,
        BSC_DG_GROUP_ALU32_PROFILE_5m,
        BSC_DG_GROUP_ALU32_PROFILE_6m,
        BSC_DG_GROUP_ALU32_PROFILE_7m,
        BSC_DG_GROUP_ALU32_PROFILE_8m,
        BSC_DG_GROUP_ALU32_PROFILE_9m,
        BSC_DG_GROUP_ALU32_PROFILE_10m,
        BSC_DG_GROUP_ALU32_PROFILE_11m,
        ALU_BASE_CONTROL_BUSf,
        ALU32_SPECIFIC_CONTROL_BUSf,
        BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt,
        BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt,
        CHECK_0_THRESHOLDf,
        CHECK_1_THRESHOLDf,
        EXPORT_THRESHOLDf,
        FIRST_DELAY_OPERANDf,
        SECOND_DELAY_OPERANDf,
        DELAY_MODEf,
        DELAY_GRANULARITYf,
        DELAY_OFFSETf,
        CHECK_ATTR_0_TS_SELECTf,
        CHECK_ATTR_1_TS_SELECTf,
        UPDATE_ATTR_1_TS_SELECTf,
        CHECK_0_ATTR_SELECTf,
        CHECK_1_ATTR_SELECTf,
        CHECK_0_OPERATIONf,
        CHECK_1_OPERATIONf,
        TIMESTAMP_0_TRIGf,
        TIMESTAMP_1_TRIGf,
        TIMESTAMP_2_TRIGf,
        TIMESTAMP_3_TRIGf,
        UPDATE_ATTR_SELECTf,
        UPDATE_OPERATIONf,
        UPDATE_OP_PARAMf,
        EXPORT_OPERATIONf,
        EXPORT_CHECK_MODEf,
        CLEAR_ON_PERIODIC_EXPORT_ENABLEf,
        CLEAR_ON_EXPORT_ENABLEf,
        LOAD_TRIGGERf,
        FLEX_CHECK_LOGIC_KMAP_CONTROLf,
        DATAf,
        BSC_TL_DG_TO_DT_ALU32_CONTROL_BUSfmt,
        BYTE_COUNT_CHECK_0_SELECTf,
        BYTE_COUNT_CHECK_1_SELECTf,
        BYTE_COUNT_UPDATE_ALU_SELECTf
    };

    use crate::hw_config::table_abstraction::{HWArrayTable, ALUProfile};
    use crate::hw_config::memory_interface::{
        bsca_format_field32_set,
        bsca_soc_mem_field_set,
        bsca_soc_mem_field_get,
        bsca_format_field_get,
        SOCError, 
        bsca_format_field32_get,
        bsca_format_field_set
    };
    use crate::hw_config::info_dump_structs::BSC_DG_GroupALU32Profile_info;

    const NUM_ALU_PROFILES: usize = 64;

    //ALU PROFILE LOGIC
    pub struct ALUConf {
        pub check_0_attr_select: u32,
        pub check_0_op: CheckOp,
        pub check_1_attr_select: u32,
        pub check_1_op: CheckOp,
        pub timestamp_2_trig: bool,
        pub timestamp_3_trig: bool,
        pub update_attr_select: u32,
        pub update_op: UpdateOp,
        pub update_op_param: u32,
        pub export_op: u32,
        pub export_check_mode: u32,
        pub clear_on_export: bool,
        pub clear_on_periodic_export: bool,
        pub load_trigger: u32,
        pub timestamp_0_trig: bool,
        pub timestamp_1_trig: bool,
        pub replace_check_0_byte_count: bool,
        pub replace_check_1_byte_count: bool,
        pub replace_update_byte_count: bool,

        //specific controls
        pub spec_check_0_thresh: u32,
        pub spec_check_1_thresh: u32,
        pub spec_export_thresh: u32,
        pub spec_first_delay_op: DelayOperand,
        pub spec_second_delay_op: DelayOperand,
        pub spec_delay_mode: DelayMode,
        pub spec_delay_gran: u32,
        pub spec_delay_offset: u32,
        pub spec_check_attr_0_ts_select: DelayAttrSelect,
        pub spec_check_attr_1_ts_select: DelayAttrSelect,
        pub spec_update_attr_1_ts_select: DelayAttrSelect,
        pub spec_check_attr_0_gran: AttrGran,
        pub spec_check_attr_1_gran: AttrGran,
    }

    pub enum CheckOp {
        GREATER,
        GEQ,
        EQ,
        NEQ,
        LESS,
        LEQ,
        PASS,
        NONE,
        MASK
    }

    fn check_op_to_u32(op: &CheckOp) -> u32 {
        match op {
            &CheckOp::GREATER => 0,
            &CheckOp::GEQ => 1,
            &CheckOp::EQ => 2,
            &CheckOp::NEQ => 3,
            &CheckOp::LESS => 4,
            &CheckOp::LEQ => 5,
            &CheckOp::PASS => 6,
            &CheckOp::NONE => 7,
            &CheckOp::MASK => 8
        }
    }

    pub enum UpdateOp {
        NOOP,
        INCREMENT,
        ADD_BYTES,
        ADD_VALUE,
        UPDATE_MIN,
        UPDATE_MAX,
        UPDATE_VAL,
        UPDATE_EWMA
    }

    fn update_op_to_u32(op: &UpdateOp) -> u32 {
        match op {
            &UpdateOp::NOOP => 0,
            &UpdateOp::INCREMENT => 1,
            &UpdateOp::ADD_BYTES => 2,
            &UpdateOp::ADD_VALUE => 3,
            &UpdateOp::UPDATE_MIN => 4,
            &UpdateOp::UPDATE_MAX => 5,
            &UpdateOp::UPDATE_VAL => 6,
            &UpdateOp::UPDATE_EWMA => 7
        }
    }

    pub enum DelayAttrSelect {
        USE_DELAY_VAL, //read/write the delay result instead of the selected attr
        USE_SELECTED_ATTR_VAL //read/write the attr defined in the ALU config
    }

    fn delay_attr_select_to_u32(das: &DelayAttrSelect) -> u32 {
        match das {
            &DelayAttrSelect::USE_DELAY_VAL => 1,
            &DelayAttrSelect::USE_SELECTED_ATTR_VAL => 0
        }
    }

    pub enum DelayOperand {
        PKT_TS0, //timestamp in the packet
        PKT_TS1, //timestamp of when the packet hit the ingress port
        SESSION_TS0, //value stored in timestamp 0
        SESSION_TS1, //value stored in timestamp 1
        SESSION_TS2, //value stored in timestamp 2
        SESSION_TS3, //value stored in timestamp 3
        CMIC_TS, //current time from CMIC, use this one for current time by default
        LTS_MODE_TOD, //current time
        LIVE_IPROC1, //current time
        LIVE_IPROC2, //current time
        BSC_TS_UTC_CONVERSION_TOD, //current time
    }

    fn delay_operand_to_u32(op: &DelayOperand) -> u32 {
        match op {
            &DelayOperand::PKT_TS0 => 0, //timestamp in the packet
            &DelayOperand::PKT_TS1 => 1, //timestamp of when the packet hit the ingress port
            &DelayOperand::SESSION_TS0 => 2, //value stored in timestamp 0
            &DelayOperand::SESSION_TS1 => 3, //value stored in timestamp 1
            &DelayOperand::SESSION_TS2 => 4, //value stored in timestamp 2
            &DelayOperand::SESSION_TS3 => 5, //value stored in timestamp 3
            &DelayOperand::CMIC_TS => 6, //current time from CMIC, use this one for current time by default
            &DelayOperand::LTS_MODE_TOD => 7, //current time
            &DelayOperand::LIVE_IPROC1 => 8, //current time
            &DelayOperand::LIVE_IPROC2 => 9, //current time
            &DelayOperand::BSC_TS_UTC_CONVERSION_TOD => 10, //current time
        }
    }

    pub enum DelayMode {
        PTP_64,
        NTP_64,
        GENERIC_48BIT, //USE THIS ONE IF UNSURE
        LTS_MODE
    }

    fn delay_mode_to_u32(mode: &DelayMode) -> u32 {
        match mode {
            &DelayMode::PTP_64 => 0,
            &DelayMode::NTP_64 => 1,
            &DelayMode::GENERIC_48BIT => 2,
            &DelayMode::LTS_MODE => 3
        }
    }

    pub enum AttrGran {
        G32Bit,
        G16Bit
    }

    fn attr_gran_to_u32(gran: &AttrGran) -> u32 {
        match gran {
            &AttrGran::G32Bit => 1,
            &AttrGran::G16Bit => 0
        }
    }

    fn alu_profile_to_mem(prof: &ALUProfile) -> u32 {
        match prof {
            &ALUProfile::ALU0 => BSC_DG_GROUP_ALU32_PROFILE_0m,
            &ALUProfile::ALU1 => BSC_DG_GROUP_ALU32_PROFILE_1m,
            &ALUProfile::ALU2 => BSC_DG_GROUP_ALU32_PROFILE_2m,
            &ALUProfile::ALU3 => BSC_DG_GROUP_ALU32_PROFILE_3m,
            &ALUProfile::ALU4 => BSC_DG_GROUP_ALU32_PROFILE_4m,
            &ALUProfile::ALU5 => BSC_DG_GROUP_ALU32_PROFILE_5m,
            &ALUProfile::ALU6 => BSC_DG_GROUP_ALU32_PROFILE_6m,
            &ALUProfile::ALU7 => BSC_DG_GROUP_ALU32_PROFILE_7m,
            &ALUProfile::ALU8 => BSC_DG_GROUP_ALU32_PROFILE_8m,
            &ALUProfile::ALU9 => BSC_DG_GROUP_ALU32_PROFILE_9m,
            &ALUProfile::ALU10 => BSC_DG_GROUP_ALU32_PROFILE_10m,
            &ALUProfile::ALU11 => BSC_DG_GROUP_ALU32_PROFILE_11m,
        }
    }

    fn mem_to_alu_idx(mem: u32) -> u32 {
        match mem {
            BSC_DG_GROUP_ALU32_PROFILE_0m => 0,
            BSC_DG_GROUP_ALU32_PROFILE_1m => 1,
            BSC_DG_GROUP_ALU32_PROFILE_2m => 2,
            BSC_DG_GROUP_ALU32_PROFILE_3m => 3,
            BSC_DG_GROUP_ALU32_PROFILE_4m => 4,
            BSC_DG_GROUP_ALU32_PROFILE_5m => 5,
            BSC_DG_GROUP_ALU32_PROFILE_6m => 6,
            BSC_DG_GROUP_ALU32_PROFILE_7m => 7,
            BSC_DG_GROUP_ALU32_PROFILE_8m => 8,
            BSC_DG_GROUP_ALU32_PROFILE_9m => 9,
            BSC_DG_GROUP_ALU32_PROFILE_10m => 10,
            BSC_DG_GROUP_ALU32_PROFILE_11m => 11,
            _ => 999
        }
    }

    pub struct BSC_DG_GroupALU32Profile {
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsc_dg_group_alu32_profile_0_entry_t; NUM_ALU_PROFILES],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsc_dg_group_alu32_profile_0_entry_t> for BSC_DG_GroupALU32Profile {
        fn get_data(&self) -> &[bsc_dg_group_alu32_profile_0_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsc_dg_group_alu32_profile_0_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsc_dg_group_alu32_profile_0_entry_t { entry_data: [0; 11]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSC_DG_GroupALU32Profile {
        pub fn new(unit: u32, prof: &ALUProfile) -> Self {
            BSC_DG_GroupALU32Profile { 
                unit: unit, 
                mem: alu_profile_to_mem(prof), 
                max_size: NUM_ALU_PROFILES as u32, 
                data: [bsc_dg_group_alu32_profile_0_entry_t { entry_data: [0; 11]}; NUM_ALU_PROFILES], 
                indexes: Vec::new() 
            }
        }

        pub fn write_alu_profile(&mut self, index: u32, conf: &ALUConf){
            let mut alu_base_control: [u32; 5] = [0; 5];
            let mut alu_specific_control: [u32; 5] = [0; 5];
            let mut alu_ctrl_fmt: [u32; 10] = [0; 10];

            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, &mut alu_base_control, CHECK_0_ATTR_SELECTf, conf.check_0_attr_select);
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, &mut alu_base_control, CHECK_0_OPERATIONf, check_op_to_u32(&conf.check_0_op));
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, &mut alu_base_control, CHECK_1_ATTR_SELECTf, conf.check_1_attr_select);
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, &mut alu_base_control, CHECK_1_OPERATIONf, check_op_to_u32(&conf.check_1_op));
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, &mut alu_base_control, TIMESTAMP_2_TRIGf, match conf.timestamp_2_trig { true => 1, false => 0});
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, &mut alu_base_control, TIMESTAMP_3_TRIGf, match conf.timestamp_3_trig { true => 1, false => 0});
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, &mut alu_base_control, UPDATE_OPERATIONf, update_op_to_u32(&conf.update_op));
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, &mut alu_base_control, UPDATE_ATTR_SELECTf, conf.update_attr_select);
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, &mut alu_base_control, UPDATE_OP_PARAMf, conf.update_op_param);
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, &mut alu_base_control, EXPORT_OPERATIONf, conf.export_op);
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, &mut alu_base_control, EXPORT_CHECK_MODEf, conf.export_check_mode);
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, &mut alu_base_control, CLEAR_ON_PERIODIC_EXPORT_ENABLEf, match conf.clear_on_periodic_export{ true => 1, false => 0});
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, &mut alu_base_control, CLEAR_ON_EXPORT_ENABLEf, match conf.clear_on_export{ true => 1, false => 0});
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, &mut alu_base_control, LOAD_TRIGGERf, conf.load_trigger);
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, &mut alu_base_control, TIMESTAMP_0_TRIGf, match conf.timestamp_0_trig { true => 1, false => 0});
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, &mut alu_base_control, TIMESTAMP_1_TRIGf, match conf.timestamp_1_trig { true => 1, false => 0});
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, &mut alu_base_control, BYTE_COUNT_CHECK_0_SELECTf, match conf.replace_check_0_byte_count{ true => 1, false => 0});
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, &mut alu_base_control, BYTE_COUNT_CHECK_1_SELECTf, match conf.replace_check_1_byte_count{ true => 1, false => 0});
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, &mut alu_base_control, BYTE_COUNT_UPDATE_ALU_SELECTf, match conf.replace_update_byte_count{ true => 1, false => 0});

            //consts
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, &mut alu_base_control, FLEX_CHECK_LOGIC_KMAP_CONTROLf, 0x8000);

            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, &mut alu_specific_control, CHECK_0_THRESHOLDf, conf.spec_check_0_thresh);
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, &mut alu_specific_control, CHECK_1_THRESHOLDf, conf.spec_check_1_thresh);
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, &mut alu_specific_control, EXPORT_THRESHOLDf, conf.spec_export_thresh);
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, &mut alu_specific_control, FIRST_DELAY_OPERANDf, delay_operand_to_u32(&conf.spec_first_delay_op));
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, &mut alu_specific_control, SECOND_DELAY_OPERANDf, delay_operand_to_u32(&conf.spec_second_delay_op));
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, &mut alu_specific_control, DELAY_MODEf, delay_mode_to_u32(&conf.spec_delay_mode));
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, &mut alu_specific_control, DELAY_GRANULARITYf, conf.spec_delay_gran);
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, &mut alu_specific_control, DELAY_OFFSETf, conf.spec_delay_offset);
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, &mut alu_specific_control, CHECK_ATTR_0_TS_SELECTf, delay_attr_select_to_u32(&conf.spec_check_attr_0_ts_select));
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, &mut alu_specific_control, CHECK_ATTR_1_TS_SELECTf, delay_attr_select_to_u32(&conf.spec_check_attr_1_ts_select));
            bsca_format_field32_set(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, &mut alu_specific_control, UPDATE_ATTR_1_TS_SELECTf, delay_attr_select_to_u32(&conf.spec_update_attr_1_ts_select));

            bsca_format_field_set(self.unit, BSC_TL_DG_TO_DT_ALU32_CONTROL_BUSfmt, &mut alu_ctrl_fmt, ALU32_SPECIFIC_CONTROL_BUSf, &mut alu_specific_control);
            bsca_format_field_set(self.unit, BSC_TL_DG_TO_DT_ALU32_CONTROL_BUSfmt, &mut alu_ctrl_fmt, ALU_BASE_CONTROL_BUSf, &mut alu_base_control);

            bsca_soc_mem_field_set(self.unit, self.mem, &mut self.data[index as usize], DATAf, &mut alu_ctrl_fmt);
            self.indexes.push(index);
        }

        pub fn gen_info_struct(&self, index: u32) -> BSC_DG_GroupALU32Profile_info {
            let mut agg_bus: [u32; 10] = [0; 10];
            let mut alu_base_control: [u32; 5] = [0; 5];
            let mut alu_specific_control: [u32; 5] = [0; 5];
            bsca_soc_mem_field_get(self.unit, self.mem, &self.data[index as usize], DATAf, &mut agg_bus);
            bsca_format_field_get(self.unit, BSC_TL_DG_TO_DT_ALU32_CONTROL_BUSfmt, &agg_bus, ALU_BASE_CONTROL_BUSf, &mut alu_base_control);
            bsca_format_field_get(self.unit, BSC_TL_DG_TO_DT_ALU32_CONTROL_BUSfmt, &agg_bus, ALU32_SPECIFIC_CONTROL_BUSf, &mut alu_specific_control);

            BSC_DG_GroupALU32Profile_info { 
                alu_idx: mem_to_alu_idx(self.mem), 
                profile_idx: index, 
                check_0_attr_select: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, CHECK_0_ATTR_SELECTf, &mut alu_base_control), 
                check_0_op: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, CHECK_0_OPERATIONf, &mut alu_base_control), 
                check_1_attr_select: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, CHECK_1_ATTR_SELECTf, &mut alu_base_control), 
                check_1_op: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, CHECK_1_OPERATIONf, &mut alu_base_control), 
                timestamp_2_trig: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, TIMESTAMP_2_TRIGf, &mut alu_base_control) == 1, 
                timestamp_3_trig: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, TIMESTAMP_3_TRIGf, &mut alu_base_control) == 1, 
                update_attr_select: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, UPDATE_ATTR_SELECTf, &mut alu_base_control), 
                update_op: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, UPDATE_OPERATIONf, &mut alu_base_control), 
                update_op_param: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, UPDATE_OP_PARAMf, &mut alu_base_control), 
                export_op: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, EXPORT_OPERATIONf, &mut alu_base_control), 
                export_check_mode: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, EXPORT_CHECK_MODEf, &mut alu_base_control), 
                clear_on_export: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, CLEAR_ON_EXPORT_ENABLEf, &mut alu_base_control) == 1, 
                clear_on_periodic_export: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, CLEAR_ON_PERIODIC_EXPORT_ENABLEf, &mut alu_base_control) == 1, 
                load_trigger: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, LOAD_TRIGGERf, &mut alu_base_control), 
                timestamp_0_trig: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, TIMESTAMP_0_TRIGf, &mut alu_base_control) == 1, 
                timestamp_1_trig: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, TIMESTAMP_1_TRIGf, &mut alu_base_control) == 1, 
                replace_check_0_byte_count: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, BYTE_COUNT_CHECK_0_SELECTf, &mut alu_base_control) == 1, 
                replace_check_1_byte_count: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, BYTE_COUNT_CHECK_1_SELECTf, &mut alu_base_control) == 1, 
                replace_update_byte_count: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU_BASE_CONTROL_BUSfmt, BYTE_COUNT_UPDATE_ALU_SELECTf, &mut alu_base_control) == 1, 
                
                
                spec_check_0_thresh: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, CHECK_0_THRESHOLDf, &mut alu_specific_control), 
                spec_check_1_thresh: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, CHECK_1_THRESHOLDf, &mut alu_specific_control), 
                spec_export_thresh: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, EXPORT_THRESHOLDf, &mut alu_specific_control), 
                spec_first_delay_op: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, FIRST_DELAY_OPERANDf, &mut alu_specific_control), 
                spec_second_delay_op: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, SECOND_DELAY_OPERANDf, &mut alu_specific_control), 
                spec_delay_mode: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, DELAY_MODEf, &mut alu_specific_control), 
                spec_delay_gran: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, DELAY_GRANULARITYf, &mut alu_specific_control), 
                spec_delay_offset: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, DELAY_OFFSETf, &mut alu_specific_control), 
                spec_check_attr_0_ts_select: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, CHECK_ATTR_0_TS_SELECTf, &mut alu_specific_control), 
                spec_check_attr_1_ts_select: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, CHECK_ATTR_1_TS_SELECTf, &mut alu_specific_control), 
                spec_update_attr_1_ts_select: bsca_format_field32_get(self.unit, BSC_TL_DG_TO_DT_ALU32_SPECIFIC_CONTROL_BUSfmt, UPDATE_ATTR_1_TS_SELECTf, &mut alu_specific_control) 
            }
        }
    }
}

pub mod bsc_dg_group_table {
    use crate::hw_config::wrapper::{
        GROUP_ALU32_PROFILE_0_IDXf,
        GROUP_ALU32_PROFILE_1_IDXf,
        GROUP_ALU32_PROFILE_2_IDXf,
        GROUP_ALU32_PROFILE_3_IDXf,
        GROUP_ALU32_PROFILE_4_IDXf,
        GROUP_ALU32_PROFILE_5_IDXf,
        GROUP_ALU32_PROFILE_6_IDXf,
        GROUP_ALU32_PROFILE_7_IDXf,
        GROUP_ALU32_PROFILE_8_IDXf,
        GROUP_ALU32_PROFILE_9_IDXf,
        GROUP_ALU32_PROFILE_10_IDXf,
        GROUP_ALU32_PROFILE_11_IDXf,
        bsc_dg_group_table_entry_t,
        GROUP_VALIDf,
        TIMESTAMP_PROFILE_IDXf,
        COLLECTOR_IDf,
        BSC_DG_GROUP_TABLEm
    };

    use crate::hw_config::table_abstraction::{HWArrayTable, ALUProfile};
    use crate::hw_config::memory_interface::{
        bsca_soc_mem_field32_set,
        //bsca_soc_mem_field_set,
        bsca_soc_mem_field32_get,
        SOCError
    };
    use crate::hw_config::info_dump_structs::BSC_DG_GroupTable_info;

    fn alu_profile_to_idx(prof: &ALUProfile) -> u32 {
        match prof {
            &ALUProfile::ALU0 => GROUP_ALU32_PROFILE_0_IDXf,
            &ALUProfile::ALU1 => GROUP_ALU32_PROFILE_1_IDXf,
            &ALUProfile::ALU2 => GROUP_ALU32_PROFILE_2_IDXf,
            &ALUProfile::ALU3 => GROUP_ALU32_PROFILE_3_IDXf,
            &ALUProfile::ALU4 => GROUP_ALU32_PROFILE_4_IDXf,
            &ALUProfile::ALU5 => GROUP_ALU32_PROFILE_5_IDXf,
            &ALUProfile::ALU6 => GROUP_ALU32_PROFILE_6_IDXf,
            &ALUProfile::ALU7 => GROUP_ALU32_PROFILE_7_IDXf,
            &ALUProfile::ALU8 => GROUP_ALU32_PROFILE_8_IDXf,
            &ALUProfile::ALU9 => GROUP_ALU32_PROFILE_9_IDXf,
            &ALUProfile::ALU10 => GROUP_ALU32_PROFILE_10_IDXf,
            &ALUProfile::ALU11 => GROUP_ALU32_PROFILE_11_IDXf,
        }
    }

    const DG_GROUP_TABLE_LEN: usize = 1024;

    pub struct BSC_DG_GroupTable {
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsc_dg_group_table_entry_t; DG_GROUP_TABLE_LEN],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsc_dg_group_table_entry_t> for BSC_DG_GroupTable {
        fn get_data(&self) -> &[bsc_dg_group_table_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsc_dg_group_table_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsc_dg_group_table_entry_t { entry_data: [0; 6]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSC_DG_GroupTable {
        pub fn new(unit: u32) -> Self {
            BSC_DG_GroupTable { 
                unit: unit, 
                mem: BSC_DG_GROUP_TABLEm, 
                max_size: DG_GROUP_TABLE_LEN as u32, 
                data: [bsc_dg_group_table_entry_t { entry_data: [0; 6]}; DG_GROUP_TABLE_LEN], 
                indexes: Vec::new() 
            }
        }

        pub fn write_group(&mut self, gid: u32, alus: &Vec<(ALUProfile, u32)>, ts_prof_idx: u32, collector_id: u32){
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[gid as usize], GROUP_VALIDf, 1);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[gid as usize], TIMESTAMP_PROFILE_IDXf, ts_prof_idx);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[gid as usize], COLLECTOR_IDf, collector_id);
            for (alu, profile_idx) in alus.iter(){
                let field = alu_profile_to_idx(alu);
                bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[gid as usize], field, *profile_idx);
            }
            self.indexes.push(gid);
        }

        pub fn gen_info_struct(&self, gid: u32) -> BSC_DG_GroupTable_info {
            BSC_DG_GroupTable_info {
                idx: gid,
                group_valid: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], GROUP_VALIDf) == 1,
                timestamp_profile_idx: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], TIMESTAMP_PROFILE_IDXf),
                collector_id: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], COLLECTOR_IDf),
                alu_profile_idxs: [
                    bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], GROUP_ALU32_PROFILE_0_IDXf),
                    bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], GROUP_ALU32_PROFILE_1_IDXf),
                    bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], GROUP_ALU32_PROFILE_2_IDXf),
                    bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], GROUP_ALU32_PROFILE_3_IDXf),
                    bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], GROUP_ALU32_PROFILE_4_IDXf),
                    bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], GROUP_ALU32_PROFILE_5_IDXf),
                    bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], GROUP_ALU32_PROFILE_6_IDXf),
                    bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], GROUP_ALU32_PROFILE_7_IDXf),
                    bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], GROUP_ALU32_PROFILE_8_IDXf),
                    bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], GROUP_ALU32_PROFILE_9_IDXf),
                    bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], GROUP_ALU32_PROFILE_10_IDXf),
                    bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], GROUP_ALU32_PROFILE_11_IDXf),
                ]

            }
        }

    }
}

pub mod bsc_dg_group_timestamp_profile_table {
    use crate::hw_config::wrapper::{
        bsc_dg_group_timestamp_profile_entry_t,
        BSC_DG_GROUP_TIMESTAMP_PROFILEm,
        //DATAf,
        TS_SELECT_3f,
        TS_SELECT_2f,
        TS_SELECT_1f,
        TS_SELECT_0f,
        DATA_3f,
        DATA_2f,
        DATA_1f,
        DATA_0f
    };

    use crate::hw_config::table_abstraction::{HWArrayTable};
    use crate::hw_config::memory_interface::{
        bsca_soc_mem_field32_set,
        //bsca_soc_mem_field_set,
        bsca_soc_mem_field32_get,
        SOCError
    };
    use crate::hw_config::info_dump_structs::BSC_DG_GroupTimestampProfileTable_info;

    const DG_GROUP_TS_TABLE_LEN: usize = 64;

    pub struct BSC_DG_GroupTimestampProfileTable {
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsc_dg_group_timestamp_profile_entry_t; DG_GROUP_TS_TABLE_LEN],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsc_dg_group_timestamp_profile_entry_t> for BSC_DG_GroupTimestampProfileTable {
        fn get_data(&self) -> &[bsc_dg_group_timestamp_profile_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsc_dg_group_timestamp_profile_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsc_dg_group_timestamp_profile_entry_t { entry_data: [0; 5]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSC_DG_GroupTimestampProfileTable {
        pub fn new(unit: u32) -> Self {
            BSC_DG_GroupTimestampProfileTable { 
                unit: unit, 
                mem: BSC_DG_GROUP_TIMESTAMP_PROFILEm, 
                max_size: DG_GROUP_TS_TABLE_LEN as u32, 
                data: [bsc_dg_group_timestamp_profile_entry_t { entry_data: [0; 5]}; DG_GROUP_TS_TABLE_LEN], 
                indexes: Vec::new() 
            }
        }

        pub fn write_profile(&mut self, index: u32, ts_0: u32, data_0: u32, ts_1: u32, data_1: u32, ts_2: u32, data_2: u32, ts_3: u32, data_3: u32){
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], TS_SELECT_0f, ts_0);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], DATA_0f, data_0);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], TS_SELECT_1f, ts_1);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], DATA_1f, data_1);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], TS_SELECT_2f, ts_2);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], DATA_2f, data_2);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], TS_SELECT_3f, ts_3);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], DATA_3f, data_3);
            self.indexes.push(index);
        }

        pub fn gen_info_struct(&self, index: u32) -> BSC_DG_GroupTimestampProfileTable_info {
            BSC_DG_GroupTimestampProfileTable_info {
                idx: index,
                ts_select_0: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], TS_SELECT_0f),
                ts_select_1: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], TS_SELECT_1f),
                ts_select_2: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], TS_SELECT_2f),
                ts_select_3: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], TS_SELECT_3f),
                data_0: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], DATA_0f),
                data_1: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], DATA_1f),
                data_2: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], DATA_2f),
                data_3: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], DATA_3f),
            }
        }
    }
}

pub mod bsc_dt_pde_profiles {
    use crate::hw_config::wrapper::{
        BSC_DT_EXPORT_PDE_PROFILE_0m,
        BSC_DT_EXPORT_PDE_PROFILE_1m,
        BSC_DT_EXPORT_PDE_PROFILE_2m,
        BSC_DT_PDE_PROFILE_0m,
        BSC_DT_PDE_PROFILE_1m,
        BSC_DT_PDE_PROFILE_2m,
        bsc_dt_pde_profile_0_entry_t,
        bsc_dt_export_pde_profile_0_entry_t
    };

    use crate::hw_config::wrapper::{
        SHIFT_AMOUNT_BYTE_63f, SHIFT_AMOUNT_BYTE_62f, SHIFT_AMOUNT_BYTE_61f, SHIFT_AMOUNT_BYTE_60f,
        SHIFT_AMOUNT_BYTE_59f, SHIFT_AMOUNT_BYTE_58f, SHIFT_AMOUNT_BYTE_57f, SHIFT_AMOUNT_BYTE_56f,
        SHIFT_AMOUNT_BYTE_55f, SHIFT_AMOUNT_BYTE_54f, SHIFT_AMOUNT_BYTE_53f, SHIFT_AMOUNT_BYTE_52f,
        SHIFT_AMOUNT_BYTE_51f, SHIFT_AMOUNT_BYTE_50f, SHIFT_AMOUNT_BYTE_49f, SHIFT_AMOUNT_BYTE_48f,
        SHIFT_AMOUNT_BYTE_47f, SHIFT_AMOUNT_BYTE_46f, SHIFT_AMOUNT_BYTE_45f, SHIFT_AMOUNT_BYTE_44f,
        SHIFT_AMOUNT_BYTE_43f, SHIFT_AMOUNT_BYTE_42f, SHIFT_AMOUNT_BYTE_41f, SHIFT_AMOUNT_BYTE_40f,
        SHIFT_AMOUNT_BYTE_39f, SHIFT_AMOUNT_BYTE_38f, SHIFT_AMOUNT_BYTE_37f, SHIFT_AMOUNT_BYTE_36f,
        SHIFT_AMOUNT_BYTE_35f, SHIFT_AMOUNT_BYTE_34f, SHIFT_AMOUNT_BYTE_33f, SHIFT_AMOUNT_BYTE_32f,
        SHIFT_AMOUNT_BYTE_31f, SHIFT_AMOUNT_BYTE_30f, SHIFT_AMOUNT_BYTE_29f, SHIFT_AMOUNT_BYTE_28f,
        SHIFT_AMOUNT_BYTE_27f, SHIFT_AMOUNT_BYTE_26f, SHIFT_AMOUNT_BYTE_25f, SHIFT_AMOUNT_BYTE_24f,
        SHIFT_AMOUNT_BYTE_23f, SHIFT_AMOUNT_BYTE_22f, SHIFT_AMOUNT_BYTE_21f, SHIFT_AMOUNT_BYTE_20f,
        SHIFT_AMOUNT_BYTE_19f, SHIFT_AMOUNT_BYTE_18f, SHIFT_AMOUNT_BYTE_17f, SHIFT_AMOUNT_BYTE_16f,
        SHIFT_AMOUNT_BYTE_15f, SHIFT_AMOUNT_BYTE_14f, SHIFT_AMOUNT_BYTE_13f, SHIFT_AMOUNT_BYTE_12f,
        SHIFT_AMOUNT_BYTE_11f, SHIFT_AMOUNT_BYTE_10f, SHIFT_AMOUNT_BYTE_9f,  SHIFT_AMOUNT_BYTE_8f,
        SHIFT_AMOUNT_BYTE_7f,  SHIFT_AMOUNT_BYTE_6f,  SHIFT_AMOUNT_BYTE_5f,  SHIFT_AMOUNT_BYTE_4f,
        SHIFT_AMOUNT_BYTE_3f,  SHIFT_AMOUNT_BYTE_2f,  SHIFT_AMOUNT_BYTE_1f,  SHIFT_AMOUNT_BYTE_0f
    };

    const byte_shifts: [u32; 64] = [
        SHIFT_AMOUNT_BYTE_63f, SHIFT_AMOUNT_BYTE_62f, SHIFT_AMOUNT_BYTE_61f, SHIFT_AMOUNT_BYTE_60f,
        SHIFT_AMOUNT_BYTE_59f, SHIFT_AMOUNT_BYTE_58f, SHIFT_AMOUNT_BYTE_57f, SHIFT_AMOUNT_BYTE_56f,
        SHIFT_AMOUNT_BYTE_55f, SHIFT_AMOUNT_BYTE_54f, SHIFT_AMOUNT_BYTE_53f, SHIFT_AMOUNT_BYTE_52f,
        SHIFT_AMOUNT_BYTE_51f, SHIFT_AMOUNT_BYTE_50f, SHIFT_AMOUNT_BYTE_49f, SHIFT_AMOUNT_BYTE_48f,
        SHIFT_AMOUNT_BYTE_47f, SHIFT_AMOUNT_BYTE_46f, SHIFT_AMOUNT_BYTE_45f, SHIFT_AMOUNT_BYTE_44f,
        SHIFT_AMOUNT_BYTE_43f, SHIFT_AMOUNT_BYTE_42f, SHIFT_AMOUNT_BYTE_41f, SHIFT_AMOUNT_BYTE_40f,
        SHIFT_AMOUNT_BYTE_39f, SHIFT_AMOUNT_BYTE_38f, SHIFT_AMOUNT_BYTE_37f, SHIFT_AMOUNT_BYTE_36f,
        SHIFT_AMOUNT_BYTE_35f, SHIFT_AMOUNT_BYTE_34f, SHIFT_AMOUNT_BYTE_33f, SHIFT_AMOUNT_BYTE_32f,
        SHIFT_AMOUNT_BYTE_31f, SHIFT_AMOUNT_BYTE_30f, SHIFT_AMOUNT_BYTE_29f, SHIFT_AMOUNT_BYTE_28f,
        SHIFT_AMOUNT_BYTE_27f, SHIFT_AMOUNT_BYTE_26f, SHIFT_AMOUNT_BYTE_25f, SHIFT_AMOUNT_BYTE_24f,
        SHIFT_AMOUNT_BYTE_23f, SHIFT_AMOUNT_BYTE_22f, SHIFT_AMOUNT_BYTE_21f, SHIFT_AMOUNT_BYTE_20f,
        SHIFT_AMOUNT_BYTE_19f, SHIFT_AMOUNT_BYTE_18f, SHIFT_AMOUNT_BYTE_17f, SHIFT_AMOUNT_BYTE_16f,
        SHIFT_AMOUNT_BYTE_15f, SHIFT_AMOUNT_BYTE_14f, SHIFT_AMOUNT_BYTE_13f, SHIFT_AMOUNT_BYTE_12f,
        SHIFT_AMOUNT_BYTE_11f, SHIFT_AMOUNT_BYTE_10f, SHIFT_AMOUNT_BYTE_9f,  SHIFT_AMOUNT_BYTE_8f,
        SHIFT_AMOUNT_BYTE_7f,  SHIFT_AMOUNT_BYTE_6f,  SHIFT_AMOUNT_BYTE_5f,  SHIFT_AMOUNT_BYTE_4f,
        SHIFT_AMOUNT_BYTE_3f,  SHIFT_AMOUNT_BYTE_2f,  SHIFT_AMOUNT_BYTE_1f,  SHIFT_AMOUNT_BYTE_0f
    ];

    use crate::hw_config::table_abstraction::{HWArrayTable, Flowtracker};
    use crate::hw_config::memory_interface::{
        bsca_soc_mem_field32_set,
        //bsca_soc_mem_field_set,
        bsca_soc_mem_field32_get,
        SOCError
    };
    use crate::hw_config::info_dump_structs::{BSC_DT_ExportPDEProfileTable_info, BSC_DT_PDEProfileTable_info};

    const PDE_PROFILE_TABLE_LEN: usize = 64;

    fn ft_to_pde_mem(ft: &Flowtracker) -> u32 {
        match ft {
            &Flowtracker::IFT => BSC_DT_PDE_PROFILE_0m,
            &Flowtracker::MFT => BSC_DT_PDE_PROFILE_1m,
            &Flowtracker::EFT => BSC_DT_PDE_PROFILE_2m
        }
    }

    fn ft_to_export_pde_mem(ft: &Flowtracker) -> u32 {
        match ft {
            &Flowtracker::IFT => BSC_DT_EXPORT_PDE_PROFILE_0m,
            &Flowtracker::MFT => BSC_DT_EXPORT_PDE_PROFILE_1m,
            &Flowtracker::EFT => BSC_DT_EXPORT_PDE_PROFILE_2m
        }
    }

    fn pde_mem_to_ft(mem: u32) -> u32 {
        match mem {
            BSC_DT_PDE_PROFILE_0m => 0,
            BSC_DT_PDE_PROFILE_1m => 1,
            BSC_DT_PDE_PROFILE_2m => 2,
            _ => 999
        }
    }

    fn export_pde_mem_to_ft(mem: u32) -> u32 {
        match mem {
            BSC_DT_EXPORT_PDE_PROFILE_0m => 0,
            BSC_DT_EXPORT_PDE_PROFILE_1m => 1,
            BSC_DT_EXPORT_PDE_PROFILE_2m => 2,
            _ => 999
        }
    }

    pub struct ByteShift {
        pub byte_in_storage_bank: usize, //CORRESPONDS TO SHIFT_BYTE_n
        pub destination_byte: u32
    }

    pub struct BSC_DT_ExportPDEProfileTable {
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsc_dt_export_pde_profile_0_entry_t; PDE_PROFILE_TABLE_LEN],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsc_dt_export_pde_profile_0_entry_t> for BSC_DT_ExportPDEProfileTable {
        fn get_data(&self) -> &[bsc_dt_export_pde_profile_0_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsc_dt_export_pde_profile_0_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsc_dt_export_pde_profile_0_entry_t { entry_data: [0; 17]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSC_DT_ExportPDEProfileTable {
        pub fn new(unit: u32, ft: &Flowtracker) -> Self {
            BSC_DT_ExportPDEProfileTable { 
                unit: unit, 
                mem: ft_to_export_pde_mem(ft), 
                max_size: PDE_PROFILE_TABLE_LEN as u32, 
                data: [bsc_dt_export_pde_profile_0_entry_t { entry_data: [0; 17]}; PDE_PROFILE_TABLE_LEN], 
                indexes: Vec::new() 
            }
        }

        pub fn write_profile(&mut self, index: u32, shifts: &Vec<ByteShift>){
            for shift in shifts.iter() {
                bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], byte_shifts[63 - shift.byte_in_storage_bank], shift.destination_byte);
            }
            self.indexes.push(index);
        }

        pub fn gen_info_struct(&self, index: u32) -> BSC_DT_ExportPDEProfileTable_info {
            let mut table = BSC_DT_ExportPDEProfileTable_info {
                idx: index,
                ft: export_pde_mem_to_ft(self.mem),
                shift_amounts: [(0,0); 64]
            };

            for (idx_rev, shift_field) in byte_shifts.iter().enumerate() {
                let idx = 63 - idx_rev; //fields are in reverse order in the array and I'm too lazy to fix it
                let shift = bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], *shift_field);
                table.shift_amounts[idx_rev] = (idx as u32, shift);
            }

            table
        }
    }

    pub struct BSC_DT_PDEProfileTable {
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsc_dt_pde_profile_0_entry_t; PDE_PROFILE_TABLE_LEN],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsc_dt_pde_profile_0_entry_t> for BSC_DT_PDEProfileTable {
        fn get_data(&self) -> &[bsc_dt_pde_profile_0_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsc_dt_pde_profile_0_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsc_dt_pde_profile_0_entry_t { entry_data: [0; 17]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSC_DT_PDEProfileTable {
        pub fn new(unit: u32, ft: &Flowtracker) -> Self {
            BSC_DT_PDEProfileTable { 
                unit: unit, 
                mem: ft_to_pde_mem(ft), 
                max_size: PDE_PROFILE_TABLE_LEN as u32, 
                data: [bsc_dt_pde_profile_0_entry_t { entry_data: [0; 17]}; PDE_PROFILE_TABLE_LEN], 
                indexes: Vec::new() 
            }
        }

        pub fn write_profile(&mut self, index: u32, shifts: &Vec<ByteShift>){
            for shift in shifts.iter() {
                bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], byte_shifts[63 - shift.byte_in_storage_bank], shift.destination_byte);
            }
            self.indexes.push(index);
        }

        pub fn gen_info_struct(&self, index: u32) -> BSC_DT_PDEProfileTable_info {
            let mut table = BSC_DT_PDEProfileTable_info {
                idx: index,
                ft: pde_mem_to_ft(self.mem),
                shift_amounts: [(0,0); 64]
            };

            for (idx_rev, shift_field) in byte_shifts.iter().enumerate() {
                let idx = 63 - idx_rev; //fields are in reverse order in the array and I'm too lazy to fix it
                let shift = bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], *shift_field);
                table.shift_amounts[idx_rev] = (idx as u32, shift);
            }

            table
        }
    }
}

pub mod bsc_ex_collector_config {
    use crate::hw_config::wrapper::{
        bsc_ex_collector_config_entry_t,
        BSC_EX_COLLECTOR_CONFIGm,
        RA_ENABLEf,
        MAX_RECORDS_PER_PACKETf,
        TIME_INTERVAL_ENABLEf,
        TIME_INTERVALf,
        PACKET_BUILD_ENABLEf,
        MAX_POINTERf,
        ENABLEf,
    };

    use crate::hw_config::table_abstraction::{HWArrayTable};
    use crate::hw_config::memory_interface::{
        bsca_soc_mem_field32_set,
        //bsca_soc_mem_field_set,
        bsca_soc_mem_field32_get,
        SOCError
    };
    use crate::hw_config::info_dump_structs::BSC_EX_CollectorConfig_info;

    const COLLECTOR_TABLE_LEN: usize = 8;

    pub struct BSC_EX_CollectorConfig {
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsc_ex_collector_config_entry_t; COLLECTOR_TABLE_LEN],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsc_ex_collector_config_entry_t> for BSC_EX_CollectorConfig {
        fn get_data(&self) -> &[bsc_ex_collector_config_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsc_ex_collector_config_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsc_ex_collector_config_entry_t { entry_data: [0; 1]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSC_EX_CollectorConfig {
        pub fn new(unit: u32) -> Self {
            BSC_EX_CollectorConfig { 
                unit: unit, 
                mem: BSC_EX_COLLECTOR_CONFIGm, 
                max_size: COLLECTOR_TABLE_LEN as u32, 
                data: [bsc_ex_collector_config_entry_t { entry_data: [0; 1]}; COLLECTOR_TABLE_LEN], 
                indexes: Vec::new() 
            }
        }

        pub fn write_config(&mut self, index: u32){
            //deal with this later
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], TIME_INTERVAL_ENABLEf, 1);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], RA_ENABLEf, 1);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], PACKET_BUILD_ENABLEf, 1);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], MAX_RECORDS_PER_PACKETf, 9);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], MAX_POINTERf, 0x7f);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], ENABLEf, 1);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], TIME_INTERVALf, 2);
            self.indexes.push(index);
        }

        pub fn gen_info_struct(&self, index: u32) -> BSC_EX_CollectorConfig_info {
            BSC_EX_CollectorConfig_info {
                idx: index,
                time_interval_enable: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], TIME_INTERVAL_ENABLEf) == 1,
                ra_enable: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], RA_ENABLEf) == 1,
                pkt_build_enable: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], PACKET_BUILD_ENABLEf) == 1,
                max_records_per_pkt: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], MAX_RECORDS_PER_PACKETf),
                max_ptr: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], MAX_POINTERf),
                enable: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], ENABLEf) == 1,
                time_interval: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], TIME_INTERVALf)
            }
        }
    }
}

//BSC_EX_COLLECTOR_IPFIX_PACKET_BUILD
pub mod bsc_ex_collector_ipfix_packet_build {
    use crate::hw_config::wrapper::{
        BSC_EX_COLLECTOR_IPFIX_PACKET_BUILDm
    };
    use crate::hw_config::table_abstraction::{HWArrayTable};
    use crate::hw_config::memory_interface::{
        SOCError
    };

    const PKT_BUILD_SIZE: usize = 8;
    const NUM_PKT_BUILD: usize = 128;

    pub struct BSC_EX_CollectorIPFIXPacketBuild {
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [[u8; PKT_BUILD_SIZE]; NUM_PKT_BUILD],
        indexes: Vec<u32>
    }

    impl HWArrayTable<[u8; PKT_BUILD_SIZE]> for BSC_EX_CollectorIPFIXPacketBuild {
        fn get_data(&self) -> &[[u8; PKT_BUILD_SIZE]] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [[u8; PKT_BUILD_SIZE]] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = [0; PKT_BUILD_SIZE];
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSC_EX_CollectorIPFIXPacketBuild {
        pub fn new(unit: u32) -> Self {
            BSC_EX_CollectorIPFIXPacketBuild { 
                unit: unit, 
                mem: BSC_EX_COLLECTOR_IPFIX_PACKET_BUILDm, 
                max_size: NUM_PKT_BUILD as u32, 
                data: [[0; PKT_BUILD_SIZE]; NUM_PKT_BUILD], 
                indexes: Vec::new() 
            }
        }

        pub fn write_config(&mut self, index: u32, data: &[u8; PKT_BUILD_SIZE]){
            self.data[index as usize] = *data;
            self.indexes.push(index);
        }
    }

}

//BSC_EX_HDR_CONSTRUCT_CFG
pub mod bsc_ex_hdr_construct_cfg {
    use crate::hw_config::wrapper::{
        BSC_EX_HDR_CONSTRUCT_CFGm,
        bsc_ex_hdr_construct_cfg_entry_t,
        HDR_BYTE_LENGTHf,
        HDR_UPDATE_CTRL_7f,
        HDR_UPDATE_CTRL_6f,
        HDR_UPDATE_CTRL_5f,
        HDR_UPDATE_CTRL_4f,
        HDR_UPDATE_CTRL_3f,
        HDR_UPDATE_CTRL_2f,
        HDR_UPDATE_CTRL_1f,
        HDR_UPDATE_CTRL_0f,
    };
    use crate::hw_config::table_abstraction::{HWArrayTable};
    use crate::hw_config::memory_interface::{
        bsca_soc_mem_field32_set,
        SOCError
    };

    const NUM_HDR_CONSTRUCT: usize = 8;

    pub struct BSC_EX_HdrConstructCfg {
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsc_ex_hdr_construct_cfg_entry_t; NUM_HDR_CONSTRUCT],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsc_ex_hdr_construct_cfg_entry_t> for BSC_EX_HdrConstructCfg {
        fn get_data(&self) -> &[bsc_ex_hdr_construct_cfg_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsc_ex_hdr_construct_cfg_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsc_ex_hdr_construct_cfg_entry_t { entry_data: [0; 7]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSC_EX_HdrConstructCfg {
        pub fn new(unit: u32) -> Self {
            BSC_EX_HdrConstructCfg { 
                unit: unit, 
                mem: BSC_EX_HDR_CONSTRUCT_CFGm, 
                max_size: NUM_HDR_CONSTRUCT as u32, 
                data: [bsc_ex_hdr_construct_cfg_entry_t { entry_data: [0; 7]}; NUM_HDR_CONSTRUCT], 
                indexes: Vec::new() 
            }
        }

        pub fn write_config(&mut self, index: u32, hdr_byte_len: u32, hdr_update_ctls: (u32, u32, u32, u32, u32, u32, u32, u32)){
            let (c0, c1, c2, c3, c4, c5, c6, c7) = hdr_update_ctls;
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], HDR_BYTE_LENGTHf, hdr_byte_len);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], HDR_UPDATE_CTRL_0f, c0);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], HDR_UPDATE_CTRL_1f, c1);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], HDR_UPDATE_CTRL_2f, c2);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], HDR_UPDATE_CTRL_3f, c3);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], HDR_UPDATE_CTRL_4f, c4);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], HDR_UPDATE_CTRL_5f, c5);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], HDR_UPDATE_CTRL_6f, c6);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], HDR_UPDATE_CTRL_7f, c7);

            self.indexes.push(index);
        }
    }
}

//BSC_KG_AGE_OUT_PROFILE
pub mod bsc_kg_age_out_profile {
    use crate::hw_config::wrapper::{
        BSC_KG_AGE_OUT_PROFILEm,
        bsc_kg_age_out_profile_entry_t,
        DATAf,        
        BSC_TL_KG_TO_KT_AGE_CONTROL_BUSfmt,
        AGE_OUT_ENABLEf,
        AGE_OUT_INTERVALf,
        EXPORT_AGE_OUTf
    };

    use crate::hw_config::table_abstraction::{HWArrayTable};
    use crate::hw_config::memory_interface::{
        bsca_soc_mem_field32_set,
        bsca_soc_mem_field32_get,
        bsca_format_field32_set,
        bsca_format_field32_get,
        SOCError
    };
    use crate::hw_config::info_dump_structs::BSC_KG_AgeOutProfile_info;

    const NUM_AGE_OUT: usize = 64;

    pub struct BSC_KG_AgeOutProfile{
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsc_kg_age_out_profile_entry_t; NUM_AGE_OUT],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsc_kg_age_out_profile_entry_t> for BSC_KG_AgeOutProfile {
        fn get_data(&self) -> &[bsc_kg_age_out_profile_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsc_kg_age_out_profile_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsc_kg_age_out_profile_entry_t { entry_data: [0; 1]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSC_KG_AgeOutProfile {
        pub fn new(unit: u32) -> Self {
            BSC_KG_AgeOutProfile { 
                unit: unit, 
                mem: BSC_KG_AGE_OUT_PROFILEm, 
                max_size: NUM_AGE_OUT as u32, 
                data: [bsc_kg_age_out_profile_entry_t { entry_data: [0; 1]}; NUM_AGE_OUT], 
                indexes: Vec::new() 
            }
        }

        //inerval is defined stragely
        /*
            interval -> ms to age out
            0 -> 100
            1 -> 1000 //second
            2 -> 10000 //10s
            3 -> 60000 //minute
            4 -> 600000 //10min
            5 -> 1800000 //half hour
            6 -> 3600000 //hour
            7 -> 36000000 //10 hours
            8 -> 86400000 //day
        */
        pub fn write_config(&mut self, index: u32, enable: bool, interval: u32, export: bool){
            bsca_format_field32_set(self.unit, BSC_TL_KG_TO_KT_AGE_CONTROL_BUSfmt, &mut self.data[index as usize], AGE_OUT_ENABLEf, match enable { true => 1, false => 0});
            bsca_format_field32_set(self.unit, BSC_TL_KG_TO_KT_AGE_CONTROL_BUSfmt, &mut self.data[index as usize], AGE_OUT_INTERVALf, interval);
            bsca_format_field32_set(self.unit, BSC_TL_KG_TO_KT_AGE_CONTROL_BUSfmt, &mut self.data[index as usize], EXPORT_AGE_OUTf, match export { true => 1, false => 0});

            //bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], DATAf, age_out_profile);
            self.indexes.push(index);
        }

        pub fn gen_info_struct(&self, index: u32) -> BSC_KG_AgeOutProfile_info {
            
            BSC_KG_AgeOutProfile_info {
                idx: index,
                enable: bsca_format_field32_get(self.unit, BSC_TL_KG_TO_KT_AGE_CONTROL_BUSfmt, AGE_OUT_ENABLEf, &self.data[index as usize]) == 1,
                export_age_out: bsca_format_field32_get(self.unit, BSC_TL_KG_TO_KT_AGE_CONTROL_BUSfmt, EXPORT_AGE_OUTf, &self.data[index as usize]) == 1,
                interval: bsca_format_field32_get(self.unit, BSC_TL_KG_TO_KT_AGE_CONTROL_BUSfmt, AGE_OUT_INTERVALf, &self.data[index as usize])
            }
        }
    }
}

//BSC_KG_FLOW_EXCEED_PROFILE
pub mod bsc_kg_flow_exceed_profile {
    use crate::hw_config::wrapper::{
        BSC_KG_FLOW_EXCEED_PROFILEm,
        bsc_kg_flow_exceed_profile_entry_t,
        DATAf,        
    };

    use crate::hw_config::table_abstraction::{HWArrayTable};
    use crate::hw_config::memory_interface::{
        bsca_soc_mem_field32_set,
        bsca_soc_mem_field32_get,
        SOCError
    };
    use crate::hw_config::info_dump_structs::BSC_KG_FlowExceedProfile_info;

    const NUM_FLOW_EXCEED: usize = 64;

    pub struct BSC_KG_FlowExceedProfile{
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsc_kg_flow_exceed_profile_entry_t; NUM_FLOW_EXCEED],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsc_kg_flow_exceed_profile_entry_t> for BSC_KG_FlowExceedProfile {
        fn get_data(&self) -> &[bsc_kg_flow_exceed_profile_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsc_kg_flow_exceed_profile_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsc_kg_flow_exceed_profile_entry_t { entry_data: [0; 1]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSC_KG_FlowExceedProfile {
        pub fn new(unit: u32) -> Self {
            BSC_KG_FlowExceedProfile { 
                unit: unit, 
                mem: BSC_KG_FLOW_EXCEED_PROFILEm, 
                max_size: NUM_FLOW_EXCEED as u32, 
                data: [bsc_kg_flow_exceed_profile_entry_t { entry_data: [0; 1]}; NUM_FLOW_EXCEED], 
                indexes: Vec::new() 
            }
        }

        pub fn write_config(&mut self, index: u32, flow_exceed_profile: u32){
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], DATAf, flow_exceed_profile);
            self.indexes.push(index);
        }

        pub fn gen_info_struct(&self, index: u32) -> BSC_KG_FlowExceedProfile_info {
            BSC_KG_FlowExceedProfile_info {
                idx: index,
                flow_exceed_profile: bsca_soc_mem_field32_get(self.unit, self.mem,  &self.data[index as usize], DATAf)
            }
        }
    }
}

//BSC_KG_GROUP_TABLE
pub mod bsc_kg_group_table {
    use crate::hw_config::wrapper::{
        BSC_KG_GROUP_TABLEm,
        bsc_kg_group_table_entry_t,
        AGE_OUT_PROFILE_IDXf,
        FLOW_EXCEED_PROFILE_IDXf,
        COLLECTOR_IDf,
        EXPORT_LEARNf,
        PERIODIC_EXPORT_ENf,
        //DATA_MODEf, //always 0
        SESSION_DATA_TYPEf,
        SET_IDf,
        GROUP_VALIDf
    };

    use crate::hw_config::table_abstraction::{HWArrayTable};
    use crate::hw_config::memory_interface::{
        bsca_soc_mem_field32_set,
        bsca_soc_mem_field32_get,
        SOCError
    };
    use crate::hw_config::info_dump_structs::BSC_KG_GroupTable_info;

    const NUM_GROUPS: usize = 1024;

    pub struct BSC_KG_GroupTable{
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsc_kg_group_table_entry_t; NUM_GROUPS],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsc_kg_group_table_entry_t> for BSC_KG_GroupTable {
        fn get_data(&self) -> &[bsc_kg_group_table_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsc_kg_group_table_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsc_kg_group_table_entry_t { entry_data: [0; 2]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSC_KG_GroupTable {
        pub fn new(unit: u32) -> Self {
            BSC_KG_GroupTable { 
                unit: unit, 
                mem: BSC_KG_GROUP_TABLEm, 
                max_size: NUM_GROUPS as u32, 
                data: [bsc_kg_group_table_entry_t { entry_data: [0; 2]}; NUM_GROUPS], 
                indexes: Vec::new() 
            }
        }

        pub fn write_config(&mut self, gid: u32, age_out_profile_idx: u32, flow_exceed_profile_idx: u32, collector_idx: u32, session_data_type: u32, set_id: u32){
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[gid as usize], AGE_OUT_PROFILE_IDXf, age_out_profile_idx);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[gid as usize], FLOW_EXCEED_PROFILE_IDXf, flow_exceed_profile_idx);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[gid as usize], COLLECTOR_IDf, collector_idx);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[gid as usize], EXPORT_LEARNf, 1);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[gid as usize], SESSION_DATA_TYPEf, session_data_type);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[gid as usize], SET_IDf, set_id);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[gid as usize], GROUP_VALIDf, 1);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[gid as usize], PERIODIC_EXPORT_ENf, 1);
            self.indexes.push(gid);
        }

        pub fn gen_info_struct(&self, gid: u32) -> BSC_KG_GroupTable_info {
            BSC_KG_GroupTable_info {
                idx: gid,
                age_out_profile_idx: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], AGE_OUT_PROFILE_IDXf),
                flow_exceed_profile_idx: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], FLOW_EXCEED_PROFILE_IDXf),
                collector_idx: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], COLLECTOR_IDf),
                export_learn: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], EXPORT_LEARNf) == 1,
                session_data_type: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], SESSION_DATA_TYPEf),
                set_id: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], SET_IDf),
                group_valid: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], GROUP_VALIDf) == 1,
                periodic_export_enabled: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[gid as usize], PERIODIC_EXPORT_ENf) == 1,
            }
        }
    }
}

//BSD_POLICY_ACTION_PROFILE_0/1/2
pub mod bsd_policy_action_profile {
    use crate::hw_config::wrapper::{
        BSD_POLICY_ACTION_PROFILE_0m,
        BSD_POLICY_ACTION_PROFILE_1m,
        BSD_POLICY_ACTION_PROFILE_2m,
        bsd_policy_action_profile_0_entry_t,
        BSD_FLEX_TIMESTAMP_0f,
        BSD_FLEX_TIMESTAMP_1f,
        BSD_FLEX_TIMESTAMP_2f,
        BSD_FLEX_TIMESTAMP_3f,
        BSD_FLEX_ALU32_TRACKING_PARAM_0f,
        BSD_FLEX_ALU32_TRACKING_PARAM_1f,
        BSD_FLEX_ALU32_TRACKING_PARAM_2f,
        BSD_FLEX_ALU32_TRACKING_PARAM_3f,
        BSD_FLEX_ALU32_TRACKING_PARAM_4f,
        BSD_FLEX_ALU32_TRACKING_PARAM_5f,
        BSD_FLEX_ALU32_TRACKING_PARAM_6f,
        BSD_FLEX_ALU32_TRACKING_PARAM_7f,
        BSD_FLEX_ALU32_TRACKING_PARAM_8f,
        BSD_FLEX_ALU32_TRACKING_PARAM_9f,
        BSD_FLEX_ALU32_TRACKING_PARAM_10f,
        BSD_FLEX_ALU32_TRACKING_PARAM_11f
    };

    use crate::hw_config::table_abstraction::{HWArrayTable, Flowtracker, ALUProfile, TSProfile};
    use crate::hw_config::memory_interface::{
        bsca_soc_mem_field32_set,
        bsca_soc_mem_field32_get,
        SOCError
    };
    use crate::hw_config::info_dump_structs::BSC_PolicyActionProfile_info;

    const TRACKING_PARAM_ARR: [u32; 12] = [
        BSD_FLEX_ALU32_TRACKING_PARAM_0f,
        BSD_FLEX_ALU32_TRACKING_PARAM_1f,
        BSD_FLEX_ALU32_TRACKING_PARAM_2f,
        BSD_FLEX_ALU32_TRACKING_PARAM_3f,
        BSD_FLEX_ALU32_TRACKING_PARAM_4f,
        BSD_FLEX_ALU32_TRACKING_PARAM_5f,
        BSD_FLEX_ALU32_TRACKING_PARAM_6f,
        BSD_FLEX_ALU32_TRACKING_PARAM_7f,
        BSD_FLEX_ALU32_TRACKING_PARAM_8f,
        BSD_FLEX_ALU32_TRACKING_PARAM_9f,
        BSD_FLEX_ALU32_TRACKING_PARAM_10f,
        BSD_FLEX_ALU32_TRACKING_PARAM_11f
    ];

    const TIMESTAMP_ENGINE_ARR: [u32; 4] = [
        BSD_FLEX_TIMESTAMP_0f,
        BSD_FLEX_TIMESTAMP_1f,
        BSD_FLEX_TIMESTAMP_2f,
        BSD_FLEX_TIMESTAMP_3f,
    ];

    const NUM_PROFILES: usize = 64;

    fn flowtracker_to_policy_action_profile(ft: &Flowtracker) -> u32 {
        match ft {
            &Flowtracker::EFT => BSD_POLICY_ACTION_PROFILE_2m,
            &Flowtracker::MFT => BSD_POLICY_ACTION_PROFILE_1m,
            &Flowtracker::IFT => BSD_POLICY_ACTION_PROFILE_0m
        }
    }

    fn action_profile_to_ft(mem: u32) -> u32 {
        match mem {
            BSD_POLICY_ACTION_PROFILE_0m => 0,
            BSD_POLICY_ACTION_PROFILE_1m => 1,
            BSD_POLICY_ACTION_PROFILE_2m => 2,
            _ => 999
        }
    }

    pub struct BSC_PolicyActionProfile{
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsd_policy_action_profile_0_entry_t; NUM_PROFILES],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsd_policy_action_profile_0_entry_t> for BSC_PolicyActionProfile {
        fn get_data(&self) -> &[bsd_policy_action_profile_0_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsd_policy_action_profile_0_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsd_policy_action_profile_0_entry_t { entry_data: [0; 2]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSC_PolicyActionProfile {
        pub fn new(unit: u32, ft: &Flowtracker) -> Self {
            BSC_PolicyActionProfile { 
                unit: unit, 
                mem: flowtracker_to_policy_action_profile(ft), 
                max_size: NUM_PROFILES as u32, 
                data: [bsd_policy_action_profile_0_entry_t { entry_data: [0; 2]}; NUM_PROFILES], 
                indexes: Vec::new() 
            }
        }

        pub fn write_config(&mut self, index: u32, alu_profiles: &Vec<ALUProfile>, ts_profiles: &Vec<TSProfile>){
            for alu_prof in alu_profiles.iter() {
                let field = match alu_prof {
                    &ALUProfile::ALU0 => BSD_FLEX_ALU32_TRACKING_PARAM_0f,
                    &ALUProfile::ALU1 => BSD_FLEX_ALU32_TRACKING_PARAM_1f,
                    &ALUProfile::ALU2 => BSD_FLEX_ALU32_TRACKING_PARAM_2f,
                    &ALUProfile::ALU3 => BSD_FLEX_ALU32_TRACKING_PARAM_3f,
                    &ALUProfile::ALU4 => BSD_FLEX_ALU32_TRACKING_PARAM_4f,
                    &ALUProfile::ALU5 => BSD_FLEX_ALU32_TRACKING_PARAM_5f,
                    &ALUProfile::ALU6 => BSD_FLEX_ALU32_TRACKING_PARAM_6f,
                    &ALUProfile::ALU7 => BSD_FLEX_ALU32_TRACKING_PARAM_7f,
                    &ALUProfile::ALU8 => BSD_FLEX_ALU32_TRACKING_PARAM_8f,
                    &ALUProfile::ALU9 => BSD_FLEX_ALU32_TRACKING_PARAM_9f,
                    &ALUProfile::ALU10 => BSD_FLEX_ALU32_TRACKING_PARAM_10f,
                    &ALUProfile::ALU11 => BSD_FLEX_ALU32_TRACKING_PARAM_11f,
                };
                bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], field, 1);
            }
            
            for ts_prof in ts_profiles.iter() {
                let field = match ts_prof {
                    &TSProfile::TS0 => BSD_FLEX_TIMESTAMP_0f,
                    &TSProfile::TS1 => BSD_FLEX_TIMESTAMP_1f,
                    &TSProfile::TS2 => BSD_FLEX_TIMESTAMP_2f,
                    &TSProfile::TS3 => BSD_FLEX_TIMESTAMP_3f,
                };
                bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], field, 1);
            }
            self.indexes.push(index);
        }

        pub fn gen_info_struct(&self, index: u32) -> BSC_PolicyActionProfile_info {
            let mut alus = [false; 12];
            let mut ts_engines = [false; 4];

            for (idx, param) in TRACKING_PARAM_ARR.iter().enumerate() {
                alus[idx] = bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], *param) == 1;
            }

            for (idx, param) in TIMESTAMP_ENGINE_ARR.iter().enumerate() {
                ts_engines[idx] = bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], *param) == 1;
            }

            BSC_PolicyActionProfile_info {
                idx: index,
                ft: action_profile_to_ft(self.mem),
                alus: alus,
                ts_engines: ts_engines
            }
        }
    }
}

//BSK_ALU_DATA_LTS_MUX_CTRL_PLUS_MASK
pub mod bsk_alu_data_lts_mux_ctrl_plus_mask {
    use crate::hw_config::wrapper::{
        BSK_ALU_DATA_LTS_MUX_CTRL_PLUS_MASKm,
        bsk_alu_data_lts_mux_ctrl_plus_mask_entry_t,
        EXTRACT_TYPE_C_0_L1_E1_SELf,
        EXTRACT_TYPE_C_1_L1_E1_SELf,
        EXTRACT_TYPE_C_2_L1_E1_SELf,
        EXTRACT_TYPE_C_3_L1_E1_SELf,
        EXTRACT_TYPE_C_4_L1_E1_SELf,
        EXTRACT_TYPE_C_5_L1_E1_SELf,
        EXTRACT_TYPE_C_6_L1_E1_SELf,
        EXTRACT_TYPE_C_7_L1_E1_SELf,
        EXTRACT_TYPE_C_8_L1_E1_SELf,
        EXTRACT_TYPE_C_9_L1_E1_SELf,
        EXTRACT_TYPE_C_10_L1_E1_SELf,
        EXTRACT_TYPE_C_11_L1_E1_SELf,
        EXTRACT_TYPE_C_12_L1_E1_SELf,
        EXTRACT_TYPE_C_13_L1_E1_SELf,
        EXTRACT_TYPE_C_14_L1_E1_SELf,
        EXTRACT_TYPE_C_15_L1_E1_SELf,
        MASKf
    };

    use crate::hw_config::table_abstraction::{HWArrayTable};
    use crate::hw_config::memory_interface::{
        bsca_soc_mem_field32_set,
        bsca_soc_mem_field32_get,
        //bsca_soc_mem_field_set,
        SOCError
    };
    use crate::hw_config::info_dump_structs::BSK_ALUDataLTSMuxCtrlPlusMask_info;

    const EXTRACT_C_SELS: [u32; 16] = [
        EXTRACT_TYPE_C_0_L1_E1_SELf,
        EXTRACT_TYPE_C_1_L1_E1_SELf,
        EXTRACT_TYPE_C_2_L1_E1_SELf,
        EXTRACT_TYPE_C_3_L1_E1_SELf,
        EXTRACT_TYPE_C_4_L1_E1_SELf,
        EXTRACT_TYPE_C_5_L1_E1_SELf,
        EXTRACT_TYPE_C_6_L1_E1_SELf,
        EXTRACT_TYPE_C_7_L1_E1_SELf,
        EXTRACT_TYPE_C_8_L1_E1_SELf,
        EXTRACT_TYPE_C_9_L1_E1_SELf,
        EXTRACT_TYPE_C_10_L1_E1_SELf,
        EXTRACT_TYPE_C_11_L1_E1_SELf,
        EXTRACT_TYPE_C_12_L1_E1_SELf,
        EXTRACT_TYPE_C_13_L1_E1_SELf,
        EXTRACT_TYPE_C_14_L1_E1_SELf,
        EXTRACT_TYPE_C_15_L1_E1_SELf
    ];

    const NUM_PROFILES: usize = 128;

    pub struct BSK_ALUDataLTSMuxCtrlPlusMask{
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsk_alu_data_lts_mux_ctrl_plus_mask_entry_t; NUM_PROFILES],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsk_alu_data_lts_mux_ctrl_plus_mask_entry_t> for BSK_ALUDataLTSMuxCtrlPlusMask {
        fn get_data(&self) -> &[bsk_alu_data_lts_mux_ctrl_plus_mask_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsk_alu_data_lts_mux_ctrl_plus_mask_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsk_alu_data_lts_mux_ctrl_plus_mask_entry_t { entry_data: [0; 5]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSK_ALUDataLTSMuxCtrlPlusMask {
        pub fn new(unit: u32) -> Self {
            BSK_ALUDataLTSMuxCtrlPlusMask { 
                unit: unit, 
                mem: BSK_ALU_DATA_LTS_MUX_CTRL_PLUS_MASKm, 
                max_size: NUM_PROFILES as u32, 
                data: [bsk_alu_data_lts_mux_ctrl_plus_mask_entry_t { entry_data: [0; 5]}; NUM_PROFILES], 
                indexes: Vec::new() 
            }
        }

        pub fn write_config(&mut self, index: u32, extract_sels: &Vec<u32>, mask: u16){
            for (idx, extract) in extract_sels.iter().enumerate() {
                bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], EXTRACT_C_SELS[idx], *extract);
            }
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], MASKf, mask as u32);
            self.indexes.push(index);
        }

        pub fn gen_info_struct(&self, index: u32) -> BSK_ALUDataLTSMuxCtrlPlusMask_info {
            let mut extract_cs: [u32; 16] = [0; 16];
            for (idx, f) in EXTRACT_C_SELS.iter().enumerate() {
                extract_cs[idx] = bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], *f);
            }
            
            BSK_ALUDataLTSMuxCtrlPlusMask_info {
                idx: index,
                extract_cs: extract_cs,
                mask: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], MASKf) as u16
            }
        }
    }
}

//BSK_FTFP_LTS_LOGICAL_TBL_SEL_SRAM
pub mod bsk_ftfp_lts_logical_tbl_sel_sram {
    use crate::hw_config::wrapper::{
        BSK_FTFP_LTS_LOGICAL_TBL_SEL_SRAMm,
        bsk_ftfp_lts_logical_tbl_sel_sram_entry_t,
        GROUP_LOOKUP_ENABLEf,
        GROUP_KEY_TYPEf,
        DELAY_MODEf,
    };

    use crate::hw_config::table_abstraction::{HWArrayTable};
    use crate::hw_config::memory_interface::{
        bsca_soc_mem_field32_set,
        bsca_soc_mem_field32_get,
        SOCError
    };
    use crate::hw_config::info_dump_structs::BSK_FTFP_LTS_LogicalTblSelSRAM_info;

    const NUM_ENTRIES: usize = 128;

    pub struct BSK_FTFP_LTS_LogicalTblSelSRAM{
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsk_ftfp_lts_logical_tbl_sel_sram_entry_t; NUM_ENTRIES],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsk_ftfp_lts_logical_tbl_sel_sram_entry_t> for BSK_FTFP_LTS_LogicalTblSelSRAM {
        fn get_data(&self) -> &[bsk_ftfp_lts_logical_tbl_sel_sram_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsk_ftfp_lts_logical_tbl_sel_sram_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsk_ftfp_lts_logical_tbl_sel_sram_entry_t { entry_data: [0; 1]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSK_FTFP_LTS_LogicalTblSelSRAM {
        pub fn new(unit: u32) -> Self {
            BSK_FTFP_LTS_LogicalTblSelSRAM { 
                unit: unit, 
                mem: BSK_FTFP_LTS_LOGICAL_TBL_SEL_SRAMm, 
                max_size: NUM_ENTRIES as u32, 
                data: [bsk_ftfp_lts_logical_tbl_sel_sram_entry_t { entry_data: [0; 1]}; NUM_ENTRIES], 
                indexes: Vec::new() 
            }
        }

        pub fn write_config(&mut self, index: u32, group_key_type: u32, delay_mode: u32){
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], GROUP_KEY_TYPEf, group_key_type);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], DELAY_MODEf, delay_mode);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], GROUP_LOOKUP_ENABLEf, 1);
            self.indexes.push(index);
        }

        pub fn gen_info_struct(&self, index: u32) -> BSK_FTFP_LTS_LogicalTblSelSRAM_info {
            BSK_FTFP_LTS_LogicalTblSelSRAM_info { 
                idx: index, 
                group_key_type: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], GROUP_KEY_TYPEf), 
                delay_mode: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], DELAY_MODEf), 
                group_lookup_enable: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], GROUP_LOOKUP_ENABLEf) == 1 
            }
        }
    }
}

//BSK_FTFP_LTS_LOGICAL_TBL_SEL_TCAM
pub mod bsk_ftfp_lts_logical_tbl_sel_tcam {
    use crate::hw_config::wrapper::{
        BSK_FTFP_LTS_LOGICAL_TBL_SEL_TCAMm,
        bsk_ftfp_lts_logical_tbl_sel_tcam_entry_t,
        HVE_RESULTS_1_Z1_BC_MASKf,
        HVE_RESULTS_1_Z1_UCf,
        HVE_RESULTS_1_Z1_UC_MASKf,
        L3_TYPE_MASKf,
        PARSER1_L4_VALID_MASKf,
        PARSER1_L4_VALIDf,
        VALIDf,
        L3_TYPEf
    };

    use crate::hw_config::table_abstraction::{HWArrayTable};
    use crate::hw_config::memory_interface::{
        bsca_soc_mem_field32_set,
        bsca_soc_mem_field32_get,
        SOCError
    };
    use crate::hw_config::info_dump_structs::BSK_FTFP_LTS_LogicalTblSelTCAM_info;

    const NUM_ENTRIES: usize = 128;

    pub struct BSK_FTFP_LTS_LogicalTblSelTCAM{
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsk_ftfp_lts_logical_tbl_sel_tcam_entry_t; NUM_ENTRIES],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsk_ftfp_lts_logical_tbl_sel_tcam_entry_t> for BSK_FTFP_LTS_LogicalTblSelTCAM {
        fn get_data(&self) -> &[bsk_ftfp_lts_logical_tbl_sel_tcam_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsk_ftfp_lts_logical_tbl_sel_tcam_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsk_ftfp_lts_logical_tbl_sel_tcam_entry_t { entry_data: [0; 13]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSK_FTFP_LTS_LogicalTblSelTCAM {
        pub fn new(unit: u32) -> Self {
            BSK_FTFP_LTS_LogicalTblSelTCAM { 
                unit: unit, 
                mem: BSK_FTFP_LTS_LOGICAL_TBL_SEL_TCAMm, 
                max_size: NUM_ENTRIES as u32, 
                data: [bsk_ftfp_lts_logical_tbl_sel_tcam_entry_t { entry_data: [0; 13]}; NUM_ENTRIES], 
                indexes: Vec::new() 
            }
        }

        pub fn write_config(&mut self, index: u32, l3_type_mask: u32, l3_type: u32){
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], VALIDf, 3);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], PARSER1_L4_VALIDf, 1); //we want valid outer headers
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], PARSER1_L4_VALID_MASKf, 1); //we care that the outer header is valid
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], L3_TYPE_MASKf, l3_type_mask);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], L3_TYPEf, l3_type);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], HVE_RESULTS_1_Z1_UC_MASKf, 1);//we care about unicast packets
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], HVE_RESULTS_1_Z1_UCf, 1); //we want unicast packets
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], HVE_RESULTS_1_Z1_BC_MASKf, 1); //ignore broadcast packets by leaving BC = 0 but BC_MASK = 1
            self.indexes.push(index);
        }

        pub fn gen_info_struct(&self, index: u32) -> BSK_FTFP_LTS_LogicalTblSelTCAM_info{
            BSK_FTFP_LTS_LogicalTblSelTCAM_info { 
                idx: index, 
                valid: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], VALIDf), 
                parser1_l4_valid: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], PARSER1_L4_VALIDf) == 1, 
                parser1_l4_valid_mask: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], PARSER1_L4_VALID_MASKf), 
                l3_type_mask: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], L3_TYPE_MASKf), 
                hve_results_1_z1_uc_mask: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], HVE_RESULTS_1_Z1_UC_MASKf), 
                hve_results_1_z1_uc: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], HVE_RESULTS_1_Z1_UCf) == 1, 
                hve_results_1_z1_bc_mask: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], HVE_RESULTS_1_Z1_BC_MASKf)
            }
        }
    }
}


//BSK_FTFP_LTS_MASK_0
pub mod bsk_ftfp_lts_mask_0 {
    use crate::hw_config::wrapper::{
        BSK_FTFP_LTS_MASK_0m,
        bsk_ftfp_lts_mask_0_entry_t,
        MASKf
    };

    use crate::hw_config::table_abstraction::{HWArrayTable};
    use crate::hw_config::memory_interface::{
        bsca_soc_mem_field_set,
        bsca_soc_mem_field_get,
        SOCError
    };
    use crate::hw_config::info_dump_structs::BSK_FTFP_LTS_Mask0_info;

    const NUM_ENTRIES: usize = 128;

    pub struct BSK_FTFP_LTS_Mask0{
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsk_ftfp_lts_mask_0_entry_t; NUM_ENTRIES],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsk_ftfp_lts_mask_0_entry_t> for BSK_FTFP_LTS_Mask0 {
        fn get_data(&self) -> &[bsk_ftfp_lts_mask_0_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsk_ftfp_lts_mask_0_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsk_ftfp_lts_mask_0_entry_t { entry_data: [0; 5]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSK_FTFP_LTS_Mask0 {
        pub fn new(unit: u32) -> Self {
            BSK_FTFP_LTS_Mask0 { 
                unit: unit, 
                mem: BSK_FTFP_LTS_MASK_0m, 
                max_size: NUM_ENTRIES as u32, 
                data: [bsk_ftfp_lts_mask_0_entry_t { entry_data: [0; 5]}; NUM_ENTRIES], 
                indexes: Vec::new() 
            }
        }

        pub fn write_config(&mut self, index: u32){
            //the mask is 128 bits
            let mut mask: [u32; 4] = [0xFFFFFFFF; 4];
            bsca_soc_mem_field_set(self.unit, self.mem, &mut self.data[index as usize], MASKf, &mut mask);
            self.indexes.push(index);
        }

        pub fn gen_info_struct(&self, index: u32) -> BSK_FTFP_LTS_Mask0_info{
            let mut mask: [u8; 16] = [0; 16];
            bsca_soc_mem_field_get(self.unit, self.mem, &self.data[index as usize], MASKf, &mut mask);
            BSK_FTFP_LTS_Mask0_info {
                idx: index,
                mask: mask
            }
        }
    }
}

//BSK_FTFP_LTS_MUX_CTRL_0
pub mod bsk_ftfp_lts_mux_ctrl_0 {
    use crate::hw_config::wrapper::{
        BSK_FTFP_LTS_MUX_CTRL_0m,
        bsk_ftfp_lts_mux_ctrl_0_entry_t,
        EXTRACT_TYPE_A_9_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_8_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_7_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_6_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_5_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_4_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_3_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_2_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_1_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_0_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_9_L1_E16_SELf,
        EXTRACT_TYPE_A_8_L1_E16_SELf,
        EXTRACT_TYPE_A_7_L1_E16_SELf,
        EXTRACT_TYPE_A_6_L1_E16_SELf,
        EXTRACT_TYPE_A_5_L1_E16_SELf,
        EXTRACT_TYPE_A_4_L1_E16_SELf,
        EXTRACT_TYPE_A_3_L1_E16_SELf,
        EXTRACT_TYPE_A_2_L1_E16_SELf,
        EXTRACT_TYPE_A_1_L1_E16_SELf,
        EXTRACT_TYPE_A_0_L1_E16_SELf,
        EXTRACT_TYPE_A_9_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_8_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_7_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_6_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_5_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_4_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_3_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_2_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_1_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_0_L2_E16_EXTRACT_MODEf,
    };

    use crate::hw_config::table_abstraction::{HWArrayTable, Extract, ExtractMode};
    use crate::hw_config::memory_interface::{
        bsca_soc_mem_field32_set,
        bsca_soc_mem_field32_get,
        SOCError
    };
    use crate::hw_config::info_dump_structs::BSK_FTFP_LTS_MuxCtrl0_info;

    // const EXTRACTS_A: [(u32, u32, u32); 10] = [
    //     (EXTRACT_TYPE_A_9_L1_E16_SELf, EXTRACT_TYPE_A_9_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_9_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_8_L1_E16_SELf, EXTRACT_TYPE_A_8_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_8_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_7_L1_E16_SELf, EXTRACT_TYPE_A_7_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_7_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_6_L1_E16_SELf, EXTRACT_TYPE_A_6_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_6_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_5_L1_E16_SELf, EXTRACT_TYPE_A_5_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_5_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_4_L1_E16_SELf, EXTRACT_TYPE_A_4_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_4_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_3_L1_E16_SELf, EXTRACT_TYPE_A_3_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_3_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_2_L1_E16_SELf, EXTRACT_TYPE_A_2_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_2_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_1_L1_E16_SELf, EXTRACT_TYPE_A_1_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_1_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_0_L1_E16_SELf, EXTRACT_TYPE_A_0_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_0_L2_E16_EXTRACT_SECTIONf),
    // ];

    const EXTRACTS_A: [(u32, u32, u32); 10] = [
        (EXTRACT_TYPE_A_0_L1_E16_SELf, EXTRACT_TYPE_A_0_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_0_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_1_L1_E16_SELf, EXTRACT_TYPE_A_1_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_1_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_2_L1_E16_SELf, EXTRACT_TYPE_A_2_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_2_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_3_L1_E16_SELf, EXTRACT_TYPE_A_3_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_3_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_4_L1_E16_SELf, EXTRACT_TYPE_A_4_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_4_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_5_L1_E16_SELf, EXTRACT_TYPE_A_5_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_5_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_6_L1_E16_SELf, EXTRACT_TYPE_A_6_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_6_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_7_L1_E16_SELf, EXTRACT_TYPE_A_7_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_7_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_8_L1_E16_SELf, EXTRACT_TYPE_A_8_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_8_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_9_L1_E16_SELf, EXTRACT_TYPE_A_9_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_9_L2_E16_EXTRACT_SECTIONf),
    ];

    

    const NUM_ENTRIES: usize = 128;

    pub struct BSK_FTFP_LTS_MuxCtrl0{
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsk_ftfp_lts_mux_ctrl_0_entry_t; NUM_ENTRIES],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsk_ftfp_lts_mux_ctrl_0_entry_t> for BSK_FTFP_LTS_MuxCtrl0 {
        fn get_data(&self) -> &[bsk_ftfp_lts_mux_ctrl_0_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsk_ftfp_lts_mux_ctrl_0_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsk_ftfp_lts_mux_ctrl_0_entry_t { entry_data: [0; 4]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSK_FTFP_LTS_MuxCtrl0 {
        pub fn new(unit: u32) -> Self {
            BSK_FTFP_LTS_MuxCtrl0 { 
                unit: unit, 
                mem: BSK_FTFP_LTS_MUX_CTRL_0m, 
                max_size: NUM_ENTRIES as u32, 
                data: [bsk_ftfp_lts_mux_ctrl_0_entry_t { entry_data: [0; 4]}; NUM_ENTRIES], 
                indexes: Vec::new() 
            }
        }

        pub fn write_config(&mut self, index: u32, extracts: &Vec<Extract>){
            for (idx, e) in extracts.iter().enumerate() {
                let (sel_f, mode_f, section_f) = EXTRACTS_A[idx];
                let mode = match e.mode {
                    ExtractMode::E16Bit => 0,
                    ExtractMode::E8Bit => 1,
                    ExtractMode::E4Bit => 2
                };
                bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], sel_f, e.offset_16b);
                bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], mode_f, mode);
                bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], section_f, e.offset_1b);
            }
            self.indexes.push(index);
        }

        pub fn gen_info_struct(&self, index: u32) -> BSK_FTFP_LTS_MuxCtrl0_info {
            let mut extracts = [(0,0,0); 10];
            for (idx, (sel_f, mode_f, section_f)) in EXTRACTS_A.iter().enumerate() {
                extracts[idx] = (
                    bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], *sel_f),
                    bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], *mode_f),
                    bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], *section_f)
                );
            }

            BSK_FTFP_LTS_MuxCtrl0_info {
                idx: index,
                extracts: extracts
            }
        }
    }
}

//BSK_FTFP_POLICY
pub mod bsk_ftfp_policy {
    use crate::hw_config::wrapper::{
        BSK_FTFP_POLICYm,
        bsk_ftfp_policy_entry_t,
        BIDIRECTIONAL_FLOWf,
        SESSION_DATA_LTS_PROFILEf,
        SESSION_KEY_LTS_PROFILEf,
        ALU_DATA_LTS_PROFILEf,
        DO_NOT_FTf,
        GROUP_IDf,
        SESSION_KEY_TYPEf,
        LEARN_DISABLEf,
        UFLOW_FIELDS_VALIDf
    };

    use crate::hw_config::table_abstraction::{HWArrayTable};
    use crate::hw_config::memory_interface::{
        bsca_soc_mem_field32_set,
        bsca_soc_mem_field32_get,
        SOCError
    };
    use crate::hw_config::info_dump_structs::BSK_FTFP_Policy_info;

    const NUM_ENTRIES: usize = 2048;

    pub struct BSK_FTFP_Policy{
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsk_ftfp_policy_entry_t; NUM_ENTRIES],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsk_ftfp_policy_entry_t> for BSK_FTFP_Policy {
        fn get_data(&self) -> &[bsk_ftfp_policy_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsk_ftfp_policy_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsk_ftfp_policy_entry_t { entry_data: [0; 6]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSK_FTFP_Policy {
        pub fn new(unit: u32) -> Self {
            BSK_FTFP_Policy { 
                unit: unit, 
                mem: BSK_FTFP_POLICYm, 
                max_size: NUM_ENTRIES as u32, 
                data: [bsk_ftfp_policy_entry_t { entry_data: [0; 6]}; NUM_ENTRIES], 
                indexes: Vec::new() 
            }
        }

        pub fn write_config(&mut self, index: u32, group_id: u32, bidir: bool, session_key_idx: u32, session_data_idx: u32, alu_data_idx: u32, session_key_type: u32){
            if index < 4 {
                println!("ERROR: BSK_FTFP_POLICY INDEX 0-3 ARE RESERVED BY THE HARDWARE");
                return;
            }
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], GROUP_IDf, group_id);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], BIDIRECTIONAL_FLOWf, match bidir { true => 1, false => 0});
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], SESSION_KEY_LTS_PROFILEf, session_key_idx);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], SESSION_DATA_LTS_PROFILEf, session_data_idx);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], ALU_DATA_LTS_PROFILEf, alu_data_idx);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], SESSION_KEY_TYPEf, session_key_type);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], DO_NOT_FTf, 0);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], LEARN_DISABLEf, 0);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], UFLOW_FIELDS_VALIDf, 1);
            self.indexes.push(index);
        }

        pub fn gen_info_struct(&self, index: u32) -> BSK_FTFP_Policy_info {
            BSK_FTFP_Policy_info { 
                idx: index, 
                group_id: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], GROUP_IDf), 
                bidir_flow: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], BIDIRECTIONAL_FLOWf) == 1, 
                session_key_lts_profile: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], SESSION_KEY_LTS_PROFILEf), 
                session_data_lts_profile: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], SESSION_DATA_LTS_PROFILEf), 
                alu_data_lts_profile: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], ALU_DATA_LTS_PROFILEf), 
                session_key_type: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], SESSION_KEY_TYPEf), 
                do_not_ft: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], DO_NOT_FTf) == 1, 
                learn_diable: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], LEARN_DISABLEf) == 1, 
                uflow_fields_valid: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], UFLOW_FIELDS_VALIDf) == 1
            }
        }
    }
}

//BSK_FTFP_TCAM
pub mod bsk_ftfp_tcam {
    use crate::hw_config::wrapper::{
        BSK_FTFP_TCAMm,
        bsk_ftfp_tcam_entry_t,
        VALIDf,
        KEY_TYPEf,
        KEY_KEYf,
        MASK_MODEf,
        MASK_TYPEf,
        MASK_KEYf,
        KEY_MODEf,
    };

    use crate::hw_config::table_abstraction::{HWArrayTable};
    use crate::hw_config::memory_interface::{
        bsca_soc_mem_field32_set,
        bsca_soc_mem_field_set,
        bsca_soc_mem_field_get,
        bsca_soc_mem_field32_get,
        SOCError
    };
    use crate::hw_config::info_dump_structs::BSK_FTFP_TCAM_info;

    const NUM_ENTRIES: usize = 2048;
    pub const BSCA_KEY_LEN_BYTES: usize = 24;

    pub struct BSK_FTFP_TCAM{
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsk_ftfp_tcam_entry_t; NUM_ENTRIES],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsk_ftfp_tcam_entry_t> for BSK_FTFP_TCAM {
        fn get_data(&self) -> &[bsk_ftfp_tcam_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsk_ftfp_tcam_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsk_ftfp_tcam_entry_t { entry_data: [0; 13]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSK_FTFP_TCAM {
        pub fn new(unit: u32) -> Self {
            BSK_FTFP_TCAM { 
                unit: unit, 
                mem: BSK_FTFP_TCAMm, 
                max_size: NUM_ENTRIES as u32, 
                data: [bsk_ftfp_tcam_entry_t { entry_data: [0; 13]}; NUM_ENTRIES], 
                indexes: Vec::new() 
            }
        }

        pub fn write_config(&mut self, index: u32, key: &mut [u8; BSCA_KEY_LEN_BYTES], mask: &mut [u8; BSCA_KEY_LEN_BYTES]){
            if index < 4 {
                println!("ERROR: BSK_FTFP_TCAM INDEX 0-3 ARE RESERVED BY THE HARDWARE");
                return;
            }
            bsca_soc_mem_field_set(self.unit, self.mem, &mut self.data[index as usize], KEY_KEYf, key);
            bsca_soc_mem_field_set(self.unit, self.mem, &mut self.data[index as usize], MASK_KEYf, mask);

            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], VALIDf, 3);
            
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], KEY_TYPEf, 0);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], KEY_MODEf, 0);
            
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], MASK_TYPEf, 0x7f);
            bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], MASK_MODEf, 1);
            
            self.indexes.push(index);
        }

        pub fn gen_info_struct(&self, index: u32) -> BSK_FTFP_TCAM_info {
            let mut key = [0; BSCA_KEY_LEN_BYTES];
            let mut mask = [0; BSCA_KEY_LEN_BYTES];
            
            bsca_soc_mem_field_get(self.unit, self.mem, &self.data[index as usize], KEY_KEYf, &mut key);
            bsca_soc_mem_field_get(self.unit, self.mem, &self.data[index as usize], MASK_KEYf, &mut mask);            

            BSK_FTFP_TCAM_info { 
                idx: index, 
                key: key, 
                mask: mask, 
                key_type: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], KEY_TYPEf), 
                mask_type: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], MASK_TYPEf), 
                valid: bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], VALIDf)
            }
        }
    }
}

//BSK_SESSION_DATA_LTS_MASK_0
pub mod bsk_session_data_lts_mask_0 {
    use crate::hw_config::wrapper::{
        BSK_SESSION_DATA_LTS_MASK_0m,
        bsk_session_data_lts_mask_0_entry_t,
        MASKf
    };

    use crate::hw_config::table_abstraction::{HWArrayTable};
    use crate::hw_config::memory_interface::{
        bsca_soc_mem_field_set,
        bsca_soc_mem_field_get,
        SOCError
    };
    use crate::hw_config::info_dump_structs::BSK_SessionDataLTSMask0_info;

    const NUM_ENTRIES: usize = 128;

    pub struct BSK_SessionDataLTSMask0{
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsk_session_data_lts_mask_0_entry_t; NUM_ENTRIES],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsk_session_data_lts_mask_0_entry_t> for BSK_SessionDataLTSMask0 {
        fn get_data(&self) -> &[bsk_session_data_lts_mask_0_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsk_session_data_lts_mask_0_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsk_session_data_lts_mask_0_entry_t { entry_data: [0; 5]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSK_SessionDataLTSMask0 {
        pub fn new(unit: u32) -> Self {
            BSK_SessionDataLTSMask0 { 
                unit: unit, 
                mem: BSK_SESSION_DATA_LTS_MASK_0m, 
                max_size: NUM_ENTRIES as u32, 
                data: [bsk_session_data_lts_mask_0_entry_t { entry_data: [0; 5]}; NUM_ENTRIES], 
                indexes: Vec::new() 
            }
        }

        pub fn write_config(&mut self, index: u32, mask: &mut [u8; 16]){
            //the mask is 128 bits
            bsca_soc_mem_field_set(self.unit, self.mem, &mut self.data[index as usize], MASKf, mask);
            self.indexes.push(index);
        }

        pub fn gen_info_struct(&self, index: u32) -> BSK_SessionDataLTSMask0_info {
            let mut mask = [0; 16];
            bsca_soc_mem_field_get(self.unit, self.mem, &self.data[index as usize], MASKf, &mut mask);
            BSK_SessionDataLTSMask0_info { idx: index, mask: mask }
        }
    }
}

//BSK_SESSION_DATA_LTS_MUX_CTRL_0
pub mod bsk_session_data_lts_mux_ctrl_0 {
    use crate::hw_config::wrapper::{
        BSK_SESSION_DATA_LTS_MUX_CTRL_0m,
        bsk_session_data_lts_mux_ctrl_0_entry_t,
        EXTRACT_TYPE_A_9_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_8_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_7_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_6_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_5_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_4_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_3_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_2_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_1_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_0_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_9_L1_E16_SELf,
        EXTRACT_TYPE_A_8_L1_E16_SELf,
        EXTRACT_TYPE_A_7_L1_E16_SELf,
        EXTRACT_TYPE_A_6_L1_E16_SELf,
        EXTRACT_TYPE_A_5_L1_E16_SELf,
        EXTRACT_TYPE_A_4_L1_E16_SELf,
        EXTRACT_TYPE_A_3_L1_E16_SELf,
        EXTRACT_TYPE_A_2_L1_E16_SELf,
        EXTRACT_TYPE_A_1_L1_E16_SELf,
        EXTRACT_TYPE_A_0_L1_E16_SELf,
        EXTRACT_TYPE_A_9_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_8_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_7_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_6_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_5_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_4_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_3_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_2_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_1_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_0_L2_E16_EXTRACT_MODEf,
    };

    use crate::hw_config::table_abstraction::{HWArrayTable, Extract, ExtractMode};
    use crate::hw_config::memory_interface::{
        bsca_soc_mem_field32_set,
        bsca_soc_mem_field32_get,
        SOCError
    };
    use crate::hw_config::info_dump_structs::BSK_SessionDataLTSMuxCtrl0_info;

    // const EXTRACTS_A: [(u32, u32, u32); 10] = [
    //     (EXTRACT_TYPE_A_9_L1_E16_SELf, EXTRACT_TYPE_A_9_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_9_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_8_L1_E16_SELf, EXTRACT_TYPE_A_8_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_8_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_7_L1_E16_SELf, EXTRACT_TYPE_A_7_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_7_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_6_L1_E16_SELf, EXTRACT_TYPE_A_6_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_6_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_5_L1_E16_SELf, EXTRACT_TYPE_A_5_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_5_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_4_L1_E16_SELf, EXTRACT_TYPE_A_4_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_4_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_3_L1_E16_SELf, EXTRACT_TYPE_A_3_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_3_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_2_L1_E16_SELf, EXTRACT_TYPE_A_2_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_2_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_1_L1_E16_SELf, EXTRACT_TYPE_A_1_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_1_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_0_L1_E16_SELf, EXTRACT_TYPE_A_0_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_0_L2_E16_EXTRACT_SECTIONf),
    // ];
    
    const EXTRACTS_A: [(u32, u32, u32); 10] = [
        (EXTRACT_TYPE_A_0_L1_E16_SELf, EXTRACT_TYPE_A_0_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_0_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_1_L1_E16_SELf, EXTRACT_TYPE_A_1_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_1_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_2_L1_E16_SELf, EXTRACT_TYPE_A_2_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_2_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_3_L1_E16_SELf, EXTRACT_TYPE_A_3_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_3_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_4_L1_E16_SELf, EXTRACT_TYPE_A_4_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_4_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_5_L1_E16_SELf, EXTRACT_TYPE_A_5_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_5_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_6_L1_E16_SELf, EXTRACT_TYPE_A_6_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_6_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_7_L1_E16_SELf, EXTRACT_TYPE_A_7_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_7_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_8_L1_E16_SELf, EXTRACT_TYPE_A_8_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_8_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_9_L1_E16_SELf, EXTRACT_TYPE_A_9_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_9_L2_E16_EXTRACT_SECTIONf),
    ];
    

    const NUM_ENTRIES: usize = 128;

    pub struct BSK_SessionDataLTSMuxCtrl0{
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsk_session_data_lts_mux_ctrl_0_entry_t; NUM_ENTRIES],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsk_session_data_lts_mux_ctrl_0_entry_t> for BSK_SessionDataLTSMuxCtrl0 {
        fn get_data(&self) -> &[bsk_session_data_lts_mux_ctrl_0_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsk_session_data_lts_mux_ctrl_0_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsk_session_data_lts_mux_ctrl_0_entry_t { entry_data: [0; 4]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSK_SessionDataLTSMuxCtrl0 {
        pub fn new(unit: u32) -> Self {
            BSK_SessionDataLTSMuxCtrl0 { 
                unit: unit, 
                mem: BSK_SESSION_DATA_LTS_MUX_CTRL_0m, 
                max_size: NUM_ENTRIES as u32, 
                data: [bsk_session_data_lts_mux_ctrl_0_entry_t { entry_data: [0; 4]}; NUM_ENTRIES], 
                indexes: Vec::new() 
            }
        }

        pub fn write_config(&mut self, index: u32, extracts: &Vec<Extract>){
            for (idx, e) in extracts.iter().enumerate() {
                let (sel_f, mode_f, section_f) = EXTRACTS_A[idx];
                let mode = match e.mode {
                    ExtractMode::E16Bit => 0,
                    ExtractMode::E8Bit => 1,
                    ExtractMode::E4Bit => 2
                };
                bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], sel_f, e.offset_16b);
                bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], mode_f, mode);
                bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], section_f, e.offset_1b);
            }
            self.indexes.push(index);
        }

        pub fn gen_info_struct(&self, index: u32) -> BSK_SessionDataLTSMuxCtrl0_info {
            let mut extracts = [(0,0,0); 10];
            for (idx, (sel_f, mode_f, section_f)) in EXTRACTS_A.iter().enumerate() {
                extracts[idx] = (
                    bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], *sel_f),
                    bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], *mode_f),
                    bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], *section_f)
                );
            }

            BSK_SessionDataLTSMuxCtrl0_info {
                idx: index,
                extracts: extracts
            }
        }
    }
}

//BSK_SESSION_KEY_LTS_MASK_0
pub mod bsk_session_key_lts_mask_0 {
    use crate::hw_config::wrapper::{
        BSK_SESSION_KEY_LTS_MASK_0m,
        bsk_session_key_lts_mask_0_entry_t,
        MASKf
    };

    use crate::hw_config::table_abstraction::{HWArrayTable};
    use crate::hw_config::memory_interface::{
        bsca_soc_mem_field_set,
        bsca_soc_mem_field_get,
        SOCError
    };
    use crate::hw_config::info_dump_structs::BSK_SessionKeyLTSMask0_info;

    const NUM_ENTRIES: usize = 128;

    pub struct BSK_SessionKeyLTSMask0{
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsk_session_key_lts_mask_0_entry_t; NUM_ENTRIES],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsk_session_key_lts_mask_0_entry_t> for BSK_SessionKeyLTSMask0 {
        fn get_data(&self) -> &[bsk_session_key_lts_mask_0_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsk_session_key_lts_mask_0_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsk_session_key_lts_mask_0_entry_t { entry_data: [0; 5]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSK_SessionKeyLTSMask0 {
        pub fn new(unit: u32) -> Self {
            BSK_SessionKeyLTSMask0 { 
                unit: unit, 
                mem: BSK_SESSION_KEY_LTS_MASK_0m, 
                max_size: NUM_ENTRIES as u32, 
                data: [bsk_session_key_lts_mask_0_entry_t { entry_data: [0; 5]}; NUM_ENTRIES], 
                indexes: Vec::new() 
            }
        }

        pub fn write_config(&mut self, index: u32, mask: &mut [u8; 16]){
            //the mask is 128 bits
            bsca_soc_mem_field_set(self.unit, self.mem, &mut self.data[index as usize], MASKf, mask);
            self.indexes.push(index);
        }

        pub fn gen_info_struct(&self, index: u32) -> BSK_SessionKeyLTSMask0_info {
            let mut mask = [0; 16];
            bsca_soc_mem_field_get(self.unit, self.mem, &self.data[index as usize], MASKf, &mut mask);
            BSK_SessionKeyLTSMask0_info { idx: index, mask: mask }
        }
    }
}

//BSK_SESSION_KEY_LTS_MUX_CTRL_0
pub mod bsk_session_key_lts_mux_ctrl_0 {
    use crate::hw_config::wrapper::{
        BSK_SESSION_KEY_LTS_MUX_CTRL_0m,
        bsk_session_key_lts_mux_ctrl_0_entry_t,
        EXTRACT_TYPE_A_9_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_8_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_7_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_6_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_5_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_4_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_3_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_2_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_1_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_0_L2_E16_EXTRACT_SECTIONf,
        EXTRACT_TYPE_A_9_L1_E16_SELf,
        EXTRACT_TYPE_A_8_L1_E16_SELf,
        EXTRACT_TYPE_A_7_L1_E16_SELf,
        EXTRACT_TYPE_A_6_L1_E16_SELf,
        EXTRACT_TYPE_A_5_L1_E16_SELf,
        EXTRACT_TYPE_A_4_L1_E16_SELf,
        EXTRACT_TYPE_A_3_L1_E16_SELf,
        EXTRACT_TYPE_A_2_L1_E16_SELf,
        EXTRACT_TYPE_A_1_L1_E16_SELf,
        EXTRACT_TYPE_A_0_L1_E16_SELf,
        EXTRACT_TYPE_A_9_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_8_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_7_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_6_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_5_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_4_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_3_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_2_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_1_L2_E16_EXTRACT_MODEf,
        EXTRACT_TYPE_A_0_L2_E16_EXTRACT_MODEf,
    };

    use crate::hw_config::table_abstraction::{HWArrayTable, Extract, ExtractMode};
    use crate::hw_config::memory_interface::{
        bsca_soc_mem_field32_set,
        bsca_soc_mem_field32_get,
        SOCError
    };

    use crate::hw_config::info_dump_structs::BSK_SessionKeyLTSMuxCtrl0_info;

    // const EXTRACTS_A: [(u32, u32, u32); 10] = [
    //     (EXTRACT_TYPE_A_9_L1_E16_SELf, EXTRACT_TYPE_A_9_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_9_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_8_L1_E16_SELf, EXTRACT_TYPE_A_8_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_8_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_7_L1_E16_SELf, EXTRACT_TYPE_A_7_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_7_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_6_L1_E16_SELf, EXTRACT_TYPE_A_6_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_6_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_5_L1_E16_SELf, EXTRACT_TYPE_A_5_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_5_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_4_L1_E16_SELf, EXTRACT_TYPE_A_4_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_4_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_3_L1_E16_SELf, EXTRACT_TYPE_A_3_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_3_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_2_L1_E16_SELf, EXTRACT_TYPE_A_2_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_2_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_1_L1_E16_SELf, EXTRACT_TYPE_A_1_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_1_L2_E16_EXTRACT_SECTIONf),
    //     (EXTRACT_TYPE_A_0_L1_E16_SELf, EXTRACT_TYPE_A_0_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_0_L2_E16_EXTRACT_SECTIONf),
    // ];

    const EXTRACTS_A: [(u32, u32, u32); 10] = [
        (EXTRACT_TYPE_A_0_L1_E16_SELf, EXTRACT_TYPE_A_0_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_0_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_1_L1_E16_SELf, EXTRACT_TYPE_A_1_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_1_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_2_L1_E16_SELf, EXTRACT_TYPE_A_2_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_2_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_3_L1_E16_SELf, EXTRACT_TYPE_A_3_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_3_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_4_L1_E16_SELf, EXTRACT_TYPE_A_4_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_4_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_5_L1_E16_SELf, EXTRACT_TYPE_A_5_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_5_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_6_L1_E16_SELf, EXTRACT_TYPE_A_6_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_6_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_7_L1_E16_SELf, EXTRACT_TYPE_A_7_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_7_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_8_L1_E16_SELf, EXTRACT_TYPE_A_8_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_8_L2_E16_EXTRACT_SECTIONf),
        (EXTRACT_TYPE_A_9_L1_E16_SELf, EXTRACT_TYPE_A_9_L2_E16_EXTRACT_MODEf, EXTRACT_TYPE_A_9_L2_E16_EXTRACT_SECTIONf),
    ];

    

    const NUM_ENTRIES: usize = 128;

    pub struct BSK_SessionKeyLTSMuxCtrl0{
        unit: u32,
        mem: u32,
        max_size: u32,
        data: [bsk_session_key_lts_mux_ctrl_0_entry_t; NUM_ENTRIES],
        indexes: Vec<u32>
    }

    impl HWArrayTable<bsk_session_key_lts_mux_ctrl_0_entry_t> for BSK_SessionKeyLTSMuxCtrl0 {
        fn get_data(&self) -> &[bsk_session_key_lts_mux_ctrl_0_entry_t] {
            &self.data
        }
        
        fn get_data_mut(&mut self) -> &mut [bsk_session_key_lts_mux_ctrl_0_entry_t] {
            &mut self.data
        }

        fn get_indexes(&self) -> &Vec<u32> {
            &self.indexes
        }

        fn get_info(&self) -> (u32, u32, u32) {
            (self.unit, self.mem, self.max_size)
        }

        fn wipe(&mut self) -> Result<(), SOCError> {
            for idx in self.indexes.iter() {
                self.data[*idx as usize] = bsk_session_key_lts_mux_ctrl_0_entry_t { entry_data: [0; 4]};
            }
            self.apply()?;
            self.indexes = Vec::new();
            Ok(())
        }
    }

    impl BSK_SessionKeyLTSMuxCtrl0 {
        pub fn new(unit: u32) -> Self {
            BSK_SessionKeyLTSMuxCtrl0 { 
                unit: unit, 
                mem: BSK_SESSION_KEY_LTS_MUX_CTRL_0m, 
                max_size: NUM_ENTRIES as u32, 
                data: [bsk_session_key_lts_mux_ctrl_0_entry_t { entry_data: [0; 4]}; NUM_ENTRIES], 
                indexes: Vec::new() 
            }
        }

        pub fn write_config(&mut self, index: u32, extracts: &Vec<Extract>){
            for (idx, e) in extracts.iter().enumerate() {
                let (sel_f, mode_f, section_f) = EXTRACTS_A[idx];
                let mode = match e.mode {
                    ExtractMode::E16Bit => 0,
                    ExtractMode::E8Bit => 1,
                    ExtractMode::E4Bit => 2
                };
                bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], sel_f, e.offset_16b);
                bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], mode_f, mode);
                bsca_soc_mem_field32_set(self.unit, self.mem, &mut self.data[index as usize], section_f, e.offset_1b);
            }
            self.indexes.push(index);
        }

        pub fn gen_info_struct(&self, index: u32) -> BSK_SessionKeyLTSMuxCtrl0_info {
            let mut extracts = [(0,0,0); 10];
            for (idx, (sel_f, mode_f, section_f)) in EXTRACTS_A.iter().enumerate() {
                extracts[idx] = (
                    bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], *sel_f),
                    bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], *mode_f),
                    bsca_soc_mem_field32_get(self.unit, self.mem, &self.data[index as usize], *section_f)
                );
            }

            BSK_SessionKeyLTSMuxCtrl0_info {
                idx: index,
                extracts: extracts
            }
        }
    }
}


//REGISTERS
//BSC_AG_PERIODIC_EXPORT
pub mod bsc_ag_periodic_export {
    use crate::hw_config::{wrapper::{
        BSC_AG_PERIODIC_EXPORTr,
        //EXPORT_TIMEf,
        EXPORT_PERIODf
    }, memory_interface::bsca_reg64_field32_read_set_write, memory_interface::SOCError};

    use crate::hw_config::table_abstraction::HWRegister;

    pub struct BSC_AG_PeriodicExport {
        data: u64,
        rid: u32,
        unit: u32
    }

    impl HWRegister for BSC_AG_PeriodicExport {
        fn get_data_mut(&mut self) -> &mut u64 {
            &mut self.data
        }

        fn get_reg_id(&self) -> u32 {
            self.rid
        }

        fn get_unit(&self) -> u32 {
            self.unit
        }
    }

    impl BSC_AG_PeriodicExport {
        pub fn new(unit: u32) -> Self {
            BSC_AG_PeriodicExport {
                data: 0,
                rid: BSC_AG_PERIODIC_EXPORTr,
                unit: unit
            }
        }

        pub fn write_config(&mut self, export_period_ms: u32) -> Result<(), SOCError>{
            //EXPORT_PERIODf tracks times in 100ms granularity (e.g. EXPORT_PERIODf = 5 means 500ms between exports)
            bsca_reg64_field32_read_set_write(self.unit, self.rid, EXPORT_PERIODf, export_period_ms/100)?;
            Ok(())
        }
        
    }
}

//BSC_DT_ALU32_TO_INSTANCE_MAPPING_CONTROL
pub mod bsc_dt_alu32_to_instance_mapping_control {
    use crate::hw_config::{wrapper::{
        BSC_DT_ALU32_TO_INSTANCE_MAPPING_CONTROLr,
        ALU32_0f,
        ALU32_1f,
        ALU32_2f,
        ALU32_3f,
        ALU32_4f,
        ALU32_5f,
        ALU32_6f,
        ALU32_7f,
        ALU32_8f,
        ALU32_9f,
        ALU32_10f,
        ALU32_11f,
    }, memory_interface::bsca_reg64_field32_read_set_write, memory_interface::SOCError};

    use crate::hw_config::table_abstraction::{HWRegister, ALUProfile, Flowtracker};

    pub struct BSC_DT_ALU32ToInstanceMappingControl {
        data: u64,
        rid: u32,
        unit: u32
    }

    impl HWRegister for BSC_DT_ALU32ToInstanceMappingControl {
        fn get_data_mut(&mut self) -> &mut u64 {
            &mut self.data
        }

        fn get_reg_id(&self) -> u32 {
            self.rid
        }

        fn get_unit(&self) -> u32 {
            self.unit
        }
    }

    impl BSC_DT_ALU32ToInstanceMappingControl {
        pub fn new(unit: u32) -> Self {
            BSC_DT_ALU32ToInstanceMappingControl {
                data: 0,
                rid: BSC_DT_ALU32_TO_INSTANCE_MAPPING_CONTROLr,
                unit: unit
            }
        }
        
        //can be invoked multiple times
        pub fn assign_alu(&mut self, alu: &ALUProfile, flowtracker: &Flowtracker) -> Result<(), SOCError>{
            let field = match alu {
                &ALUProfile::ALU0 => ALU32_0f,
                &ALUProfile::ALU1 => ALU32_1f,
                &ALUProfile::ALU2 => ALU32_2f,
                &ALUProfile::ALU3 => ALU32_3f,
                &ALUProfile::ALU4 => ALU32_4f,
                &ALUProfile::ALU5 => ALU32_5f,
                &ALUProfile::ALU6 => ALU32_6f,
                &ALUProfile::ALU7 => ALU32_7f,
                &ALUProfile::ALU8 => ALU32_8f,
                &ALUProfile::ALU9 => ALU32_9f,
                &ALUProfile::ALU10 => ALU32_10f,
                &ALUProfile::ALU11 => ALU32_11f,
            };

            let ft = match flowtracker {
                &Flowtracker::EFT => 2,
                &Flowtracker::IFT => 0,
                &Flowtracker::MFT => 1
            };

            bsca_reg64_field32_read_set_write(self.unit, self.rid, field, ft)?;
            Ok(())
        }
    }
}


//BSC_DT_BANK_TO_INSTANCE_MAPPING_CONTROL
pub mod bsc_dt_bank_to_instance_mapping_control {
    use crate::hw_config::{wrapper::{
        BSC_DT_BANK_TO_INSTANCE_MAPPING_CONTROLr,
        BANK_0f,
        BANK_1f,
        BANK_2f,
        BANK_3f,
        BANK_4f,
        BANK_5f,
        BANK_6f,
        BANK_7f
    }, memory_interface::bsca_reg64_field32_read_set_write, memory_interface::SOCError};

    use crate::hw_config::table_abstraction::{HWRegister, Banks, Flowtracker};

    pub struct BSC_DT_BankToInstanceMappingControl {
        data: u64,
        rid: u32,
        unit: u32
    }

    impl HWRegister for BSC_DT_BankToInstanceMappingControl {
        fn get_data_mut(&mut self) -> &mut u64 {
            &mut self.data
        }

        fn get_reg_id(&self) -> u32 {
            self.rid
        }

        fn get_unit(&self) -> u32 {
            self.unit
        }
    }

    impl BSC_DT_BankToInstanceMappingControl {
        pub fn new(unit: u32) -> Self {
            BSC_DT_BankToInstanceMappingControl {
                data: 0,
                rid: BSC_DT_BANK_TO_INSTANCE_MAPPING_CONTROLr,
                unit: unit
            }
        }
        
        //can be invoked multiple times
        pub fn assign_bank(&mut self, bank: &Banks, flowtracker: &Flowtracker) -> Result<(), SOCError>{
            let field = match bank {
                &Banks::BANK_0 => BANK_0f,
                &Banks::BANK_1 => BANK_1f,
                &Banks::BANK_2 => BANK_2f,
                &Banks::BANK_3 => BANK_3f,
                &Banks::BANK_4 => BANK_4f,
                &Banks::BANK_5 => BANK_5f,
                &Banks::BANK_6 => BANK_6f,
                &Banks::BANK_7 => BANK_7f,
            };

            let ft = match flowtracker {
                &Flowtracker::EFT => 2,
                &Flowtracker::IFT => 0,
                &Flowtracker::MFT => 1
            };

            bsca_reg64_field32_read_set_write(self.unit, self.rid, field, ft)?;
            Ok(())
        }
    }
}


//BSC_DT_EXPORT_BANK_VALID_BITMAP
pub mod bsc_dt_export_bank_valid_bitmap {
    use crate::hw_config::{wrapper::{
        BSC_DT_EXPORT_BANK_VALID_BITMAPr,
        FT_0_BANK_VALID_BITMAPf,
        FT_1_BANK_VALID_BITMAPf,
        FT_2_BANK_VALID_BITMAPf,
        //FT_3_BANK_VALID_BITMAPf,
    }, memory_interface::bsca_reg64_field32_read_set_write, memory_interface::SOCError};

    use crate::hw_config::table_abstraction::{HWRegister, Flowtracker};

    pub struct BSC_DT_ExportBankValidBitmap {
        data: u64,
        rid: u32,
        unit: u32
    }

    impl HWRegister for BSC_DT_ExportBankValidBitmap {
        fn get_data_mut(&mut self) -> &mut u64 {
            &mut self.data
        }

        fn get_reg_id(&self) -> u32 {
            self.rid
        }

        fn get_unit(&self) -> u32 {
            self.unit
        }
    }

    impl BSC_DT_ExportBankValidBitmap {
        pub fn new(unit: u32) -> Self {
            BSC_DT_ExportBankValidBitmap {
                data: 0,
                rid: BSC_DT_EXPORT_BANK_VALID_BITMAPr,
                unit: unit
            }
        }
        
        //can be invoked multiple times
        pub fn set_ft_enabled(&mut self, flowtracker: &Flowtracker, bitmap: u32) -> Result<(), SOCError>{
            let field = match flowtracker {
                &Flowtracker::EFT => FT_2_BANK_VALID_BITMAPf,
                &Flowtracker::IFT => FT_0_BANK_VALID_BITMAPf,
                &Flowtracker::MFT => FT_1_BANK_VALID_BITMAPf
            };

            bsca_reg64_field32_read_set_write(self.unit, self.rid, field, bitmap)?;
            Ok(())
        }
    }
}

//BSC_DT_TIMESTAMP_ENGINE_TO_INSTANCE_MAPPING_CONTROL	
pub mod bsc_dt_timestamp_engine_to_instance_mapping_control {
    use crate::hw_config::{wrapper::{
        BSC_DT_TIMESTAMP_ENGINE_TO_INSTANCE_MAPPING_CONTROLr,
        TIMESTAMP_ENGINE_0f,
        TIMESTAMP_ENGINE_1f,
        TIMESTAMP_ENGINE_2f,
        TIMESTAMP_ENGINE_3f,
    }, memory_interface::bsca_reg64_field32_read_set_write, memory_interface::SOCError};

    use crate::hw_config::table_abstraction::{HWRegister, TSProfile, Flowtracker};

    pub struct BSC_DT_TimestampEngineToInstanceMappingControl {
        data: u64,
        rid: u32,
        unit: u32
    }

    impl HWRegister for BSC_DT_TimestampEngineToInstanceMappingControl {
        fn get_data_mut(&mut self) -> &mut u64 {
            &mut self.data
        }

        fn get_reg_id(&self) -> u32 {
            self.rid
        }

        fn get_unit(&self) -> u32 {
            self.unit
        }
    }

    impl BSC_DT_TimestampEngineToInstanceMappingControl {
        pub fn new(unit: u32) -> Self {
            BSC_DT_TimestampEngineToInstanceMappingControl {
                data: 0,
                rid: BSC_DT_TIMESTAMP_ENGINE_TO_INSTANCE_MAPPING_CONTROLr,
                unit: unit
            }
        }
        
        //can be invoked multiple times
        pub fn assign_ts_engine(&mut self, ts: &TSProfile, flowtracker: &Flowtracker) -> Result<(), SOCError>{
            let field = match ts {
                &TSProfile::TS0 => TIMESTAMP_ENGINE_0f,
                &TSProfile::TS1 => TIMESTAMP_ENGINE_1f,
                &TSProfile::TS2 => TIMESTAMP_ENGINE_2f,
                &TSProfile::TS3 => TIMESTAMP_ENGINE_3f,
            };

            let ft = match flowtracker {
                &Flowtracker::EFT => 2,
                &Flowtracker::IFT => 0,
                &Flowtracker::MFT => 1
            };

            bsca_reg64_field32_read_set_write(self.unit, self.rid, field, ft)?;
            Ok(())
        }
    }
}


//BSC_EX_BROADSCAN_RA_FT_ENABLE
pub mod bsc_ex_broadscan_ra_ft_enable {
    use crate::hw_config::{wrapper::{
        BSC_EX_BROADSCAN_RA_FT_ENABLEr,
        FT_0_ENABLEf,
        FT_1_ENABLEf,
        FT_2_ENABLEf,
        //FT_3_ENABLEf,
    }, memory_interface::bsca_reg64_field32_read_set_write, memory_interface::SOCError};

    use crate::hw_config::table_abstraction::{HWRegister, Flowtracker};

    pub struct BSC_EX_BroadscanRAFTEnable {
        data: u64,
        rid: u32,
        unit: u32
    }

    impl HWRegister for BSC_EX_BroadscanRAFTEnable {
        fn get_data_mut(&mut self) -> &mut u64 {
            &mut self.data
        }

        fn get_reg_id(&self) -> u32 {
            self.rid
        }

        fn get_unit(&self) -> u32 {
            self.unit
        }
    }

    impl BSC_EX_BroadscanRAFTEnable {
        pub fn new(unit: u32) -> Self {
            BSC_EX_BroadscanRAFTEnable {
                data: 0,
                rid: BSC_EX_BROADSCAN_RA_FT_ENABLEr,
                unit: unit
            }
        }
        
        //can be invoked multiple times
        pub fn set_ft_enabled(&mut self, flowtracker: &Flowtracker, enable: bool) -> Result<(), SOCError>{
            let field = match flowtracker {
                &Flowtracker::EFT => FT_2_ENABLEf,
                &Flowtracker::IFT => FT_0_ENABLEf,
                &Flowtracker::MFT => FT_1_ENABLEf
            };

            bsca_reg64_field32_read_set_write(self.unit, self.rid, field, match enable { false => 0, true => 1})?;
            Ok(())
        }
    }
}

use std::time::Instant;

use crate::hw_config::wrapper::{
    FT_KEY_SINGLEm, 
    BSC_DT_FLEX_SESSION_DATA_SINGLEm, 
    KEYf,
    KEY_TYPEf,
    BASE_VALIDf,
    OPAQUE_DATA_3f,
    OPAQUE_DATA_2f,
    OPAQUE_DATA_1f,
    OPAQUE_DATA_0f,
    OPAQUE_DATAf,
    VALIDf,
    VALID_0f,
    VALID_1f,
    VALID_2f,
    VALID_3f,
    ft_key_single_entry_t, 
    bsc_dt_flex_session_data_single_entry_t
};
use crate::hw_config::memory_interface::{
    bsca_soc_mem_field32_get, 
    bsca_soc_mem_field_get, 
    bsca_soc_mem_field_set, 
    bsca_soc_mem_field32_set, 
    bsca_soc_mem_read_range, 
    bsca_soc_mem_write,
    bsca_soc_mem_read
};

use super::memory_interface::SOCError;

pub const NUM_FT_ENTRIES: usize = 0x3FFFF;

pub struct FlowTable {
    pub keys: Vec::<ft_key_single_entry_t>, //this is a vec because it needs to be on the heap and Vecs play nicer than Box<[array]>
    pub data: Vec::<bsc_dt_flex_session_data_single_entry_t>, //same as above
    pub modified_indexes: Vec<usize>,
    unit: u32
}

impl FlowTable {
    pub fn new(unit: u32) -> Self {
        let mut ft = FlowTable {
            keys: Vec::with_capacity(NUM_FT_ENTRIES),
            data: Vec::with_capacity(NUM_FT_ENTRIES),
            modified_indexes: Vec::new(),
            unit: unit
        };

        for idx in 0..NUM_FT_ENTRIES {
            ft.keys.push(ft_key_single_entry_t { entry_data: [0; 6] });
            ft.data.push(bsc_dt_flex_session_data_single_entry_t { entry_data: [0; 10]});
        }

        ft
    }

    pub fn read_keys_to_mem(&mut self) -> Result<(), SOCError>{
        let start = Instant::now();
        bsca_soc_mem_read_range(self.unit, FT_KEY_SINGLEm, 0, self.keys.as_mut_slice())?;
        let dur = start.elapsed();
        println!("READING ENTIRE KEY TABLE TOOK {:?}", dur);
        Ok(())
    }

    pub fn scan_keys(&mut self) -> u32 {
        let start = Instant::now();
        let mut num_valid = 0;
        for (idx, k) in self.keys.iter_mut().enumerate() {
            let base_valid = bsca_soc_mem_field32_get(self.unit, FT_KEY_SINGLEm, k, BASE_VALIDf);
            if base_valid != 0 {
                num_valid += 1;
                self.modified_indexes.push(idx);
            }
        }

        let dur = start.elapsed();
        println!("SCANNING ENTIRE KEY TABLE TOOK {:?}", dur);

        num_valid
    }

    pub fn read_data_to_mem(&mut self) -> Result<(), SOCError> {
        let start = Instant::now();
        for idx in self.modified_indexes.iter() {
            bsca_soc_mem_read(self.unit, BSC_DT_FLEX_SESSION_DATA_SINGLEm, *idx as u32, &mut self.data[*idx])?;
        }

        let dur = start.elapsed();
        println!("READING {} DATA ENTRIES TOOK {:?}", self.modified_indexes.len(), dur);
        Ok(())
    }
}

pub fn read_key_value(index: u32){
    let mut key = ft_key_single_entry_t{ entry_data: [0; 6] };
    let mut val = bsc_dt_flex_session_data_single_entry_t { entry_data: [0; 10]};
    bsca_soc_mem_read(0, FT_KEY_SINGLEm, index, &mut key).expect("FAILED TO READ KEY");
    bsca_soc_mem_read(0, BSC_DT_FLEX_SESSION_DATA_SINGLEm, index, &mut val).expect("FAILED TO READ VALUE");
    println!("KEY={:08x?}", key.entry_data);
    println!("VAL={:08x?}", val.entry_data);
}
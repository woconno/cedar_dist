use std::time::{Instant};

pub use crate::hw_config::table_abstraction::{
    ALUProfile,
    bsc_dg_group_alu32_profile_table::{
        ALUConf,
        DelayAttrSelect,
        DelayMode,
        DelayOperand,
        UpdateOp,
        CheckOp
    },
    Banks,
    Flowtracker,
    TSProfile,
    ExtractMode,
    HWArrayTable,
    HWRegister,
    Extract
};

pub use crate::hw_config::table_abstraction::{
    bsc_dg_group_alu32_profile_table::BSC_DG_GroupALU32Profile,
    bsc_dg_group_table::BSC_DG_GroupTable,
    bsc_kg_group_table::BSC_KG_GroupTable,
    bsc_dg_group_timestamp_profile_table::BSC_DG_GroupTimestampProfileTable,
    bsc_dt_pde_profiles::ByteShift,
    bsc_dt_pde_profiles::BSC_DT_PDEProfileTable,
    bsc_dt_pde_profiles::BSC_DT_ExportPDEProfileTable,
    bsc_ex_collector_config::BSC_EX_CollectorConfig,
    bsc_ex_collector_ipfix_packet_build::BSC_EX_CollectorIPFIXPacketBuild,
    bsc_ex_hdr_construct_cfg::BSC_EX_HdrConstructCfg,
    bsc_kg_age_out_profile::BSC_KG_AgeOutProfile,
    bsd_policy_action_profile::BSC_PolicyActionProfile,
    bsk_alu_data_lts_mux_ctrl_plus_mask::BSK_ALUDataLTSMuxCtrlPlusMask,
    bsk_ftfp_lts_logical_tbl_sel_sram::BSK_FTFP_LTS_LogicalTblSelSRAM,
    bsk_ftfp_lts_logical_tbl_sel_tcam::BSK_FTFP_LTS_LogicalTblSelTCAM,
    bsk_ftfp_lts_mask_0::BSK_FTFP_LTS_Mask0,
    bsk_ftfp_lts_mux_ctrl_0::BSK_FTFP_LTS_MuxCtrl0,
    bsk_ftfp_policy::BSK_FTFP_Policy,
    bsk_ftfp_tcam::BSCA_KEY_LEN_BYTES,
    bsk_ftfp_tcam::BSK_FTFP_TCAM,
    bsk_session_data_lts_mask_0::BSK_SessionDataLTSMask0,
    bsk_session_data_lts_mux_ctrl_0::BSK_SessionDataLTSMuxCtrl0,
    bsk_session_key_lts_mask_0::BSK_SessionKeyLTSMask0,
    bsk_session_key_lts_mux_ctrl_0::BSK_SessionKeyLTSMuxCtrl0,
    bsc_kg_flow_exceed_profile::BSC_KG_FlowExceedProfile,

    //registers
    bsc_ag_periodic_export::BSC_AG_PeriodicExport,
    bsc_dt_alu32_to_instance_mapping_control::BSC_DT_ALU32ToInstanceMappingControl,
    bsc_dt_bank_to_instance_mapping_control::BSC_DT_BankToInstanceMappingControl,
    bsc_dt_export_bank_valid_bitmap::BSC_DT_ExportBankValidBitmap,
    bsc_dt_timestamp_engine_to_instance_mapping_control::BSC_DT_TimestampEngineToInstanceMappingControl,
    bsc_ex_broadscan_ra_ft_enable::BSC_EX_BroadscanRAFTEnable,
};

use super::memory_interface::SOCError;
use super::memory_interface::bsca_ft_init;

pub struct SwitchHardware {
    pub alu32profiles: [BSC_DG_GroupALU32Profile; 12],
    pub dg_group_table: BSC_DG_GroupTable,
    pub kg_group_table: BSC_KG_GroupTable,
    pub timestamp_profiles: BSC_DG_GroupTimestampProfileTable,
    pub pde_profiles_ift: BSC_DT_PDEProfileTable,
    pub pde_profiles_eft: BSC_DT_PDEProfileTable,
    pub export_pde_profiles_ift: BSC_DT_ExportPDEProfileTable,
    pub export_pde_profiles_eft: BSC_DT_ExportPDEProfileTable,
    pub collector_config: BSC_EX_CollectorConfig,
    pub ipfix_pkt_build: BSC_EX_CollectorIPFIXPacketBuild,
    pub ipfix_hdr_construct: BSC_EX_HdrConstructCfg,
    pub age_out_profile: BSC_KG_AgeOutProfile,
    pub pdd_profile_ift: BSC_PolicyActionProfile,
    pub pdd_profile_eft: BSC_PolicyActionProfile,
    pub alu_mux_mask: BSK_ALUDataLTSMuxCtrlPlusMask,
    pub ftfp_lts_sram: BSK_FTFP_LTS_LogicalTblSelSRAM,
    pub ftfp_lts_tcam: BSK_FTFP_LTS_LogicalTblSelTCAM,
    pub ftfp_mask: BSK_FTFP_LTS_Mask0,
    pub ftfp_mux: BSK_FTFP_LTS_MuxCtrl0,
    pub ftfp_tcam: BSK_FTFP_TCAM,
    pub ftfp_policy: BSK_FTFP_Policy,
    pub session_data_mask: BSK_SessionDataLTSMask0,
    pub session_data_mux: BSK_SessionDataLTSMuxCtrl0,
    pub session_key_mask: BSK_SessionKeyLTSMask0,
    pub session_key_mux: BSK_SessionKeyLTSMuxCtrl0,
    pub flow_exceed_profile: BSC_KG_FlowExceedProfile,

    //registers
    pub periodic_export_r: BSC_AG_PeriodicExport,
    pub alu32_mapping: BSC_DT_ALU32ToInstanceMappingControl,
    pub bank_mapping: BSC_DT_BankToInstanceMappingControl,
    pub bank_valid_bitmap: BSC_DT_ExportBankValidBitmap,
    pub ts_mapping: BSC_DT_TimestampEngineToInstanceMappingControl,
    pub ft_enable: BSC_EX_BroadscanRAFTEnable,

    unit: u32
}

impl SwitchHardware {
    pub fn new(unit: u32) -> Self {
        let alu_profiles = [
            BSC_DG_GroupALU32Profile::new(unit, &ALUProfile::ALU0),
            BSC_DG_GroupALU32Profile::new(unit, &ALUProfile::ALU1),
            BSC_DG_GroupALU32Profile::new(unit, &ALUProfile::ALU2),
            BSC_DG_GroupALU32Profile::new(unit, &ALUProfile::ALU3),
            BSC_DG_GroupALU32Profile::new(unit, &ALUProfile::ALU4),
            BSC_DG_GroupALU32Profile::new(unit, &ALUProfile::ALU5),
            BSC_DG_GroupALU32Profile::new(unit, &ALUProfile::ALU6),
            BSC_DG_GroupALU32Profile::new(unit, &ALUProfile::ALU7),
            BSC_DG_GroupALU32Profile::new(unit, &ALUProfile::ALU8),
            BSC_DG_GroupALU32Profile::new(unit, &ALUProfile::ALU9),
            BSC_DG_GroupALU32Profile::new(unit, &ALUProfile::ALU10),
            BSC_DG_GroupALU32Profile::new(unit, &ALUProfile::ALU11),
        ];

        SwitchHardware { 
            alu32profiles: alu_profiles, 
            dg_group_table: BSC_DG_GroupTable::new(unit), 
            kg_group_table: BSC_KG_GroupTable::new(unit),
            timestamp_profiles: BSC_DG_GroupTimestampProfileTable::new(unit), 
            pde_profiles_ift: BSC_DT_PDEProfileTable::new(unit, &Flowtracker::IFT), 
            pde_profiles_eft: BSC_DT_PDEProfileTable::new(unit, &Flowtracker::EFT), 
            export_pde_profiles_ift: BSC_DT_ExportPDEProfileTable::new(unit, &Flowtracker::IFT), 
            export_pde_profiles_eft: BSC_DT_ExportPDEProfileTable::new(unit, &Flowtracker::EFT), 
            collector_config: BSC_EX_CollectorConfig::new(unit), 
            ipfix_pkt_build: BSC_EX_CollectorIPFIXPacketBuild::new(unit), 
            ipfix_hdr_construct: BSC_EX_HdrConstructCfg::new(unit), 
            age_out_profile: BSC_KG_AgeOutProfile::new(unit), 
            pdd_profile_ift: BSC_PolicyActionProfile::new(unit, &Flowtracker::IFT), 
            pdd_profile_eft: BSC_PolicyActionProfile::new(unit, &Flowtracker::EFT),
            alu_mux_mask: BSK_ALUDataLTSMuxCtrlPlusMask::new(unit), 
            ftfp_lts_sram: BSK_FTFP_LTS_LogicalTblSelSRAM::new(unit), 
            ftfp_lts_tcam: BSK_FTFP_LTS_LogicalTblSelTCAM::new(unit), 
            ftfp_mask: BSK_FTFP_LTS_Mask0::new(unit), 
            ftfp_mux: BSK_FTFP_LTS_MuxCtrl0::new(unit), 
            ftfp_tcam: BSK_FTFP_TCAM::new(unit), 
            ftfp_policy: BSK_FTFP_Policy::new(unit), 
            session_data_mask: BSK_SessionDataLTSMask0::new(unit), 
            session_data_mux: BSK_SessionDataLTSMuxCtrl0::new(unit), 
            session_key_mask: BSK_SessionKeyLTSMask0::new(unit), 
            session_key_mux: BSK_SessionKeyLTSMuxCtrl0::new(unit), 
            flow_exceed_profile: BSC_KG_FlowExceedProfile::new(unit),
            
            //registers
            periodic_export_r: BSC_AG_PeriodicExport::new(unit), 
            alu32_mapping: BSC_DT_ALU32ToInstanceMappingControl::new(unit), 
            bank_mapping: BSC_DT_BankToInstanceMappingControl::new(unit), 
            bank_valid_bitmap: BSC_DT_ExportBankValidBitmap::new(unit), 
            ts_mapping: BSC_DT_TimestampEngineToInstanceMappingControl::new(unit), 
            ft_enable: BSC_EX_BroadscanRAFTEnable::new(unit),

            unit: unit
        }
    }

    pub fn apply_config(&self) -> Result<(), SOCError> {
        let start = Instant::now();

        for aluprof in self.alu32profiles.iter() {
            aluprof.apply()?;
        }

        self.dg_group_table.apply()?;
        self.kg_group_table.apply()?;
        self.timestamp_profiles.apply()?;
        self.pde_profiles_ift.apply()?;
        self.pde_profiles_eft.apply()?;
        self.export_pde_profiles_ift.apply()?;
        self.export_pde_profiles_eft.apply()?;
        self.collector_config.apply()?;
        self.ipfix_pkt_build.apply()?;
        self.ipfix_hdr_construct.apply()?;
        self.age_out_profile.apply()?;
        self.pdd_profile_ift.apply()?;
        self.pdd_profile_eft.apply()?;
        self.alu_mux_mask.apply()?;
        self.ftfp_lts_sram.apply()?;
        self.ftfp_lts_tcam.apply()?;
        self.ftfp_tcam.apply()?;
        self.ftfp_mask.apply()?;
        self.ftfp_mux.apply()?;
        self.ftfp_policy.apply()?;
        self.session_data_mask.apply()?;
        self.session_data_mux.apply()?;
        self.session_key_mask.apply()?;
        self.session_key_mux.apply()?;
        self.flow_exceed_profile.apply()?;


        let duration = start.elapsed();
        println!("INFO::apply_config: Applying took {:?}", duration);
        Ok(())
    }

    pub fn apply_all(&self) -> Result<(), SOCError> {
        let start = Instant::now();

        for aluprof in self.alu32profiles.iter() {
            aluprof.apply_all()?;
        }

        self.dg_group_table.apply_all()?;
        self.kg_group_table.apply_all()?;
        self.timestamp_profiles.apply_all()?;
        self.pde_profiles_ift.apply_all()?;
        self.pde_profiles_eft.apply_all()?;
        self.export_pde_profiles_ift.apply_all()?;
        self.export_pde_profiles_eft.apply_all()?;
        self.collector_config.apply_all()?;
        self.ipfix_pkt_build.apply_all()?;
        self.ipfix_hdr_construct.apply_all()?;
        self.age_out_profile.apply_all()?;
        self.pdd_profile_ift.apply_all()?;
        self.pdd_profile_eft.apply_all()?;
        self.alu_mux_mask.apply_all()?;
        self.ftfp_lts_sram.apply_all()?;
        self.ftfp_lts_tcam.apply_all()?;
        self.ftfp_tcam.apply_all()?;
        self.ftfp_mask.apply_all()?;
        self.ftfp_mux.apply_all()?;
        self.ftfp_policy.apply_all()?;
        self.session_data_mask.apply_all()?;
        self.session_data_mux.apply_all()?;
        self.session_key_mask.apply_all()?;
        self.session_key_mux.apply_all()?;
        self.flow_exceed_profile.apply_all()?;


        let duration = start.elapsed();
        println!("INFO::apply_config: Applying all took {:?}", duration);
        Ok(())
    }

    pub fn read_from_switch(&mut self) -> Result<(), SOCError> {
        let start = Instant::now();
        for aluprof in self.alu32profiles.iter_mut() {
            aluprof.read(None)?;
        }

        self.dg_group_table.read(None)?;
        self.kg_group_table.read(None)?;
        self.timestamp_profiles.read(None)?;
        self.pde_profiles_ift.read(None)?;
        self.pde_profiles_eft.read(None)?;
        self.export_pde_profiles_ift.read(None)?;
        self.export_pde_profiles_eft.read(None)?;
        self.collector_config.read(None)?;
        self.ipfix_pkt_build.read(None)?;
        self.ipfix_hdr_construct.read(None)?;
        self.age_out_profile.read(None)?;
        self.pdd_profile_ift.read(None)?;
        self.pdd_profile_eft.read(None)?;
        self.alu_mux_mask.read(None)?;
        self.ftfp_lts_sram.read(None)?;
        self.ftfp_lts_tcam.read(None)?;
        self.ftfp_mask.read(None)?;
        self.ftfp_mux.read(None)?;
        self.ftfp_tcam.read(None)?;
        self.ftfp_policy.read(None)?;
        self.session_data_mask.read(None)?;
        self.session_data_mux.read(None)?;
        self.session_key_mask.read(None)?;
        self.session_key_mux.read(None)?;
        self.flow_exceed_profile.read(None)?;

        let duration = start.elapsed();
        println!("INFO::read_from_switch: Reading everything took {:?}", duration);

        Ok(())
    }

    pub fn init(&mut self) -> Result<(), SOCError> {
        bsca_ft_init(self.unit)
    }
}
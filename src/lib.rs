#![crate_type = "staticlib"]
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
mod hw_config;
mod render_hw;
//mod direct_write_test;
mod server;
mod internal_config;

use std::{ffi::{CStr, CString}, thread};
use hw_config::tables::SwitchHardware;
use render_hw::{render_to_dot, walk_query};
//use direct_write_test::write_config;
use hw_config::flow_table::read_key_value;
use internal_config::{load_config};

pub unsafe extern "C" fn cmd_bsca_entry(unit: ::std::os::raw::c_int, a: *mut args_t) -> i32{
    println!("HELLO FROM BSCAv2, UNIT={}", unit);
    assert!(!a.is_null());

    let args: Vec<String> = (*a).a_argv.iter().take((*a).a_argc as usize).map(|argv|{ 
        let val = String::from(CStr::from_ptr(*argv).to_str().expect("Failed to parse string"));
        val
    }).collect();

    for s in args.iter() {
        println!("{}", s);
    }

    if args.len() == 2 {
        if args[1].starts_with("write_test") {
            let sw = SwitchHardware::new(0);
            sw.apply_all().expect("Failed to write to switch");
        }

        if args[1].starts_with("server") {
            println!("STARTING SERVER");
            let _handle = thread::spawn(||{ 
                let config = load_config(String::from("/broadcom/bsca_cfg.json"));
                server::server_thread(&config);
            });
        }

        if args[1].starts_with("stop") {

        }

        //CRASHES SWITCH, DO NOT RUN
        // if args[1].starts_with("table") {
        //     let mut ft = FlowTable::new(0);
        //     ft.read_keys_to_mem().expect("Failed to read flow table keys");
        //     ft.scan_keys();
        //     ft.read_data_to_mem().expect("Failed to read flow table data");
        // }
    }

    if args.len() == 3 {
        if args[1].starts_with("flow") {
            read_key_value(args[2].parse().expect("Failed to parse second arg as int"));
        }
    }

    if args.len() == 4 {
        if args[1].starts_with("read") {
            let mut sw = SwitchHardware::new(0);
            sw.read_from_switch().expect("Failed to read from switch");
            
            //print_setup(&sw);
            let query = walk_query(&sw, args[2].parse().expect("failed to parse arg 3"), args[3].parse().expect("failed to parse arg 4"));
            println!("{}", render_to_dot(query));
        }

        // if args[1].starts_with("bucket_test") {
        //     write_config(
        //         args[2].parse().expect("FAILED TO PARSE SPLIT POINT"), 
        //         args[3].parse().expect("FAILED TO PARSE CONFIG ID")
        //     ).expect("FAILED TO WRITE CONFIG");
        // }
    }

    // if args.len() == 3 && args[1].starts_with("alu") {
    //     print_alu_cfg(&args[2]);
    // }

    return cmd_result_e_CMD_OK;
}

#[no_mangle]
pub unsafe extern "C" fn custom_cmd(unit: ::std::os::raw::c_int) -> i32{
    let mut cmd = cmd_t {
        c_cmd: CString::new("bsca").expect("Failed to alloc cstr").into_raw(),
        c_f: Some(cmd_bsca_entry),
        c_help: CString::new("Test help string").expect("Failed to alloc cstr").into_raw(),
        c_usage: CString::new("Test help string").expect("Failed to alloc cstr").into_raw()
    };

    let ptr: *mut cmd_t = &mut cmd;

    return cmdlist_add(unit, ptr);
}
use serde::{Deserialize};
use std::fs;
use std::net::Ipv4Addr;

#[derive(Debug, Deserialize, Clone, Copy)]
pub struct ConfigFile {
    pub http_server_port: u16,
    pub http_server_ip: Ipv4Addr,
    pub ipfix_template_server_port: u16,
    pub ipfix_template_server_ip: Ipv4Addr,
    pub ipfix_template_send_interval_ms: u32,
    pub ipfix_template_dst_ip: Ipv4Addr,
    pub ipfix_template_dst_port: u16
}


pub fn load_config(path: String) -> ConfigFile {
    let contents = fs::read_to_string(path).expect("Failed to load config file");
    let config: ConfigFile = serde_json::from_str(contents.as_str()).expect("Failed to parse config file");
    config
}
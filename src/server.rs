pub mod ll_config;
mod ll_config_apply;
//mod ipfix_template_thread;
mod custom_send_thread;

use std::thread;
use std::time::{Instant};
use std::net::{SocketAddr, IpAddr};
use std::sync::{mpsc};

use tiny_http::{Server, Response};
use serde::{Deserialize, Serialize};

use crate::internal_config::ConfigFile;
use ll_config::{SWCfg};
use ll_config_apply::apply_config;
//use ipfix_template_thread::{TemplateThreadMessage, config_to_template_info, template_send_thread};
use custom_send_thread::{TemplateThreadMessage, config_to_template_info, template_send_thread};

#[derive(Deserialize, Serialize, Debug)]
pub enum RType{
    dot,
    config,
    err
}

#[derive(Serialize, Debug)]
pub struct SWRes {
    pub rtype: RType,
    pub status: String,
    pub dot: Option<String>
}

#[derive(Deserialize, Debug)]
pub struct SWReq {
    pub rtype: RType,
    pub req: Option<SWCfg>
}


pub fn server_thread(config: &ConfigFile) {
    crate::hw_config::config::enable_timer(0);
    let (template_tx, template_rx) = mpsc::sync_channel(65536);
    let t_s_ip = config.ipfix_template_server_ip;
    let t_s_port = config.ipfix_template_server_port;
    let t_d_ip = config.ipfix_template_dst_ip;
    let t_d_port = config.ipfix_template_dst_port;
    let t_int = config.ipfix_template_send_interval_ms;
    
    let _tmp_thread = thread::spawn(move ||{
        template_send_thread(t_s_ip, t_s_port, t_d_ip, t_d_port, t_int, template_rx);
    });

    let serv_sock = SocketAddr::new(IpAddr::V4(config.http_server_ip), config.http_server_port);

    let server = Server::http(serv_sock).expect("Failed to start HTTP server");

    for mut request in server.incoming_requests() {
        let start = Instant::now();
        let data: SWReq = match serde_json::from_reader(request.as_reader()) {
            Ok(d) => d,
            Err(e) => {
                let res = Response::from_string(serde_json::to_string(&SWRes { 
                    rtype: RType::err, 
                    status: format!("Error parsing json: {:?}", e), 
                    dot: None
                }).expect("failed to serialze error response json"));
                request.respond(res).expect("Failed to respond with error json");
                continue;
            }
        };

        let response = match data.rtype {
            RType::config => match apply_config(data.req.as_ref().unwrap()) {
                Err(e) => {
                    println!("SOC APPLY ERROR: {:?}", e);
                    Response::from_string(serde_json::to_string(&SWRes {
                        rtype: RType::err, 
                        status: format!("Error applying config: {:?}", e), 
                        dot: None
                    }).expect("failed to serialize soc error response json"))
                },
                Ok(_) => {
                    println!("APPLIED CONFIG");
                    let info = config_to_template_info(data.req.as_ref().unwrap());
                    template_tx.send(TemplateThreadMessage::NewCfg(info)).expect("Failed to send new query info to template thread");
                    Response::from_string(serde_json::to_string(&SWRes {
                        rtype: RType::config, 
                        status: String::from("OK"), 
                        dot: None
                    }).expect("failed to serialize soc success response json"))
                }
            },
            RType::err => {
                println!("CLIENT SENT ERROR TYPE");
                Response::from_string(serde_json::to_string(&SWRes {
                    rtype: RType::err, 
                    status: String::from("why did you send me this?"), 
                    dot: None
                }).expect("failed to serialize client sent error response json"))
            },
            RType::dot => {
                println!("NOT IMPLEMENTED YET"); 
                Response::from_string(serde_json::to_string(&SWRes {
                    rtype: RType::err, 
                    status: String::from("NOT IMPLEMENTED YET"), 
                    dot: None
                }).expect("failed to serialize dot response json"))
            }
        };

        request.respond(response).expect("Failed to respond to http message");
        let end = start.elapsed();
        println!("Handling request (including response) took {:?}", end);
    }

}
use std::collections::HashMap;

use crate::hw_config::tables::*;
use crate::hw_config::info_dump_structs::*;

pub fn _print_setup(hw: &SwitchHardware){
    println!("ALU PROFILES:");
    for prof in hw.alu32profiles.iter() {
        let alu_info = prof.gen_info_struct(1);
        if alu_info.clear_on_periodic_export {
            println!("{:#?}", alu_info);
        }
        
    }

    println!("DG_GROUP_TABLE:");
    println!("{:#?}", hw.dg_group_table.gen_info_struct(0));

    //println!("BSK_FTFP_TCAM:");
    //println!("{:#?}", hw.ftfp_tcam.gen_info_struct(4));

}


pub struct PktPath {
    //KEYGEN
    //BSK_FTFP_LTS_LOGICAL_TBL_SEL_TCAM picks [BSK_FTFP_LTS_LOGICAL_TBL_SEL_SRAM, BSK_FTFP_LTS_MUX_CTRL_0]
    log_tbl_sel_tcam: BSK_FTFP_LTS_LogicalTblSelTCAM_info,
    log_tbl_sel_sram: BSK_FTFP_LTS_LogicalTblSelSRAM_info,
    log_tbl_sel_mux_ctrl: BSK_FTFP_LTS_MuxCtrl0_info,
    log_tbl_sel_mask: BSK_FTFP_LTS_Mask0_info,

    //FILTERS
    //FTFP_TCAM selects FTFP_POLICY
    ftfp_tcam: BSK_FTFP_TCAM_info,
    ftfp_policy: BSK_FTFP_Policy_info,

    //HASH TABLE KEY GEN
    //selected by FTFP_POLICY
    ses_key_mux: BSK_SessionKeyLTSMuxCtrl0_info,
    ses_key_mask: BSK_SessionKeyLTSMask0_info,

    //DATA MUX GEN
    //selected by FTFP_POLICY
    ses_data_mux: BSK_SessionDataLTSMuxCtrl0_info,
    ses_data_mask: BSK_SessionDataLTSMask0_info,

    //ALU MUX GEN
    //selected by FTFP_POLICY
    alu_data_mux_mask: BSK_ALUDataLTSMuxCtrlPlusMask_info,

    //GROUP INFO
    dg_group: BSC_DG_GroupTable_info,
    kg_group: BSC_KG_GroupTable_info,

    //ALU Profiles
    alu_profiles: Vec<BSC_DG_GroupALU32Profile_info>,

    //Collector Profile
    collector_profile: BSC_EX_CollectorConfig_info,
    //hdr_construct: BSC_EX_HdrConstructCfg_info,

    //MISC Profiles
    age_out_profile: BSC_KG_AgeOutProfile_info,
    flow_exceed_profile: BSC_KG_FlowExceedProfile_info,
    timestamp_profile: BSC_DG_GroupTimestampProfileTable_info,

    //IFT PDD/PDE Profile
    ift_pdd_profile: BSC_PolicyActionProfile_info,
    ift_pde_profile: BSC_DT_PDEProfileTable_info,
    ift_export_pde_profile: BSC_DT_ExportPDEProfileTable_info,

    //EFT PDD/PDE Profile
    eft_pdd_profile: BSC_PolicyActionProfile_info,
    eft_pde_profile: BSC_DT_PDEProfileTable_info,
    eft_export_pde_profile: BSC_DT_ExportPDEProfileTable_info,
}

pub fn walk_query(hw: &SwitchHardware, keygen_idx: u32, filter_idx: u32) -> PktPath{
    //STEP 1: pkt parsers read packet and set flags for which fields are valid
    //BSK_FTFP_LTS_LOGICAL_TBL_SEL_TCAM reads these flags and picks an SRAM and MUX_CTRL config
    //the SRAM config decides how time info is attached to the packets ingress time field
    //the MUX config jams fields on to a 160bit bus that is used as the input in to FTFP_TCAM
    let log_tbl_sel_tcam = hw.ftfp_lts_tcam.gen_info_struct(keygen_idx);
    let log_tbl_sel_sram = hw.ftfp_lts_sram.gen_info_struct(keygen_idx);
    let log_tbl_sel_mux_ctrl = hw.ftfp_mux.gen_info_struct(keygen_idx);
    let log_tbl_sel_mask = hw.ftfp_mask.gen_info_struct(keygen_idx);

    //STEP 2: FTFP_TCAM selects an FTFP_POLICY based on if the incoming packet matches the key & mask for each row
    //FTFP POLICY picks the group ID, key gen id, alu gen id, and data gen id for this packet
    let ftfp_tcam = hw.ftfp_tcam.gen_info_struct(filter_idx);
    let ftfp_policy = hw.ftfp_policy.gen_info_struct(filter_idx);

    //group ID indexes some tables that set the behavior of ALUs and the export behavior
    let group_id = ftfp_policy.group_id;

    //each of these indexes a mux configuration for the respective thing
    let key_profile = ftfp_policy.session_key_lts_profile;
    let data_profile = ftfp_policy.session_data_lts_profile;
    let alu_profile = ftfp_policy.alu_data_lts_profile;

    

    //STEP 3: GENERATE HASH TABLE KEY
    //FTFP_POLICY picks these with session_key_idx
    //this will mux packet fields in to a 160 bit bus, mask it with the mask, and hash the result to get the
    //slot this flow maps to in the flow table
    let ses_key_mux = hw.session_key_mux.gen_info_struct(key_profile);
    let ses_key_mask = hw.session_key_mask.gen_info_struct(key_profile);

    //STEP 4: GENERATE DATA TABLE INPUT
    let ses_data_mux = hw.session_data_mux.gen_info_struct(data_profile);
    let ses_data_mask = hw.session_data_mask.gen_info_struct(data_profile);

    //STEP 5: GENERATE ALU INPUT (how is this different than the data table input)
    let alu_data_mux_mask = hw.alu_mux_mask.gen_info_struct(alu_profile);

    //STEP 6: look up group info
    let dg_group = hw.dg_group_table.gen_info_struct(group_id);
    let kg_group = hw.kg_group_table.gen_info_struct(group_id);

    //DATA TYPE picks how data is stored in the table and how it is exported
    let data_type = kg_group.session_data_type;

    //which IPFIX collector to use
    let collector_id = kg_group.collector_idx;

    let age_out_profile_idx = kg_group.age_out_profile_idx;
    let flow_exceed_profile_idx = kg_group.flow_exceed_profile_idx;

    let timestamp_profile_idx = dg_group.timestamp_profile_idx;

    let flow_exceed_profile = hw.flow_exceed_profile.gen_info_struct(flow_exceed_profile_idx);

    //Which ALU profiels to use
    let alu_profiles_idxs = &dg_group.alu_profile_idxs;
    let _collector_id_2 = dg_group.collector_id;

    //STEP 7: GRAB ALU PROFILES
    let mut alu_profiles: Vec<BSC_DG_GroupALU32Profile_info> = Vec::new();
    for (idx, alu_prof_idx) in alu_profiles_idxs.iter().enumerate() {
        alu_profiles.push(hw.alu32profiles[idx].gen_info_struct(*alu_prof_idx));
    }

    //STEP 8: GRAB COLLECTOR PROFILE
    let collector_profile = hw.collector_config.gen_info_struct(collector_id);
    //skip hdr_construct

    //STEP 9: grab misc profiles
    let age_out_profile = hw.age_out_profile.gen_info_struct(age_out_profile_idx);
    let timestamp_profile = hw.timestamp_profiles.gen_info_struct(timestamp_profile_idx);

    //STEP 10: SET IFT PDE/PDD PROFILE
    let ift_pdd_profile = hw.pdd_profile_ift.gen_info_struct(data_type);
    let ift_pde_profile = hw.pde_profiles_ift.gen_info_struct(data_type);
    let ift_export_pde_profile = hw.export_pde_profiles_ift.gen_info_struct(data_type);

    //STEP 11: SET EFT PDE/PDD PROFILE
    let eft_pdd_profile = hw.pdd_profile_eft.gen_info_struct(data_type);
    let eft_pde_profile = hw.pde_profiles_eft.gen_info_struct(data_type);
    let eft_export_pde_profile = hw.export_pde_profiles_eft.gen_info_struct(data_type);

    PktPath {
        log_tbl_sel_tcam,
        log_tbl_sel_sram,
        log_tbl_sel_mux_ctrl,
        log_tbl_sel_mask,

        ftfp_tcam,
        ftfp_policy,

        ses_key_mux,
        ses_key_mask,

        ses_data_mux,
        ses_data_mask,

        alu_data_mux_mask,

        dg_group,
        kg_group,

        alu_profiles,

        collector_profile,

        age_out_profile,
        flow_exceed_profile,
        timestamp_profile,

        ift_pdd_profile,
        ift_pde_profile,
        ift_export_pde_profile,

        eft_pdd_profile,
        eft_pde_profile,
        eft_export_pde_profile
    }
}

pub fn render_to_record(node_name: &str, title: &str, index: u32, title_port: &str, kv: &Vec<(&str, &str, &str)>) -> String {
    let mut head = format!("{{<{}>{}|[{}]}}", title_port, title, index);
    for (key, value, port) in kv {
        let cur = format!("|{{{}|<{}>{}}}", key, port, value);
        head += &cur;
    }

    format!("    {} [shape=record label=\"{}\"]\n", node_name, head)
}

pub fn render_link(src_node: &str, src_port: &str, dst_node: &str, dst_port: &str) -> String {
    format!("    {}:{} -> {}:{}\n", src_node, src_port, dst_node, dst_port)
}

pub fn render_to_dot(data: PktPath) -> String {
    let mut dot = String::from("digraph G {\n    rankdir=\"LR\"\n");
    dot += data.log_tbl_sel_tcam.render().as_str();
    dot += data.log_tbl_sel_sram.render().as_str();
    dot += data.log_tbl_sel_mux_ctrl.render().as_str();
    dot += data.log_tbl_sel_mask.render().as_str();

    dot += data.ftfp_tcam.render().as_str();
    dot += data.ftfp_policy.render().as_str();
    dot += data.kg_group.render().as_str();
    dot += data.dg_group.render().as_str();
    dot += data.collector_profile.render().as_str();
    dot += data.timestamp_profile.render().as_str();
    dot += data.ses_key_mux.render().as_str();
    dot += data.ses_key_mask.render().as_str();
    dot += data.ses_data_mux.render().as_str();
    dot += data.ses_data_mask.render().as_str();
    dot += data.alu_data_mux_mask.render().as_str();
    dot += data.age_out_profile.render().as_str();

    for alu in data.alu_profiles {
        if alu.profile_idx != 0 {
            dot += alu.render().as_str();
            dot += render_link("NODE_dg_group", format!("alu_{}_profile", alu.alu_idx).as_str(), format!("NODE_alu_{}_profile", alu.alu_idx).as_str(), "title").as_str();
        }
    }

    dot += data.ift_pdd_profile.render().as_str();
    dot += data.ift_pde_profile.render().as_str();
    dot += data.ift_export_pde_profile.render().as_str();
    dot += data.eft_pdd_profile.render().as_str();
    dot += data.eft_pde_profile.render().as_str();
    dot += data.eft_export_pde_profile.render().as_str();
    dot += data.flow_exceed_profile.render().as_str();

    dot += render_link("NODE_log_tbl_sel_tcam", "title", "NODE_log_tbl_sel_sram", "title").as_str();
    dot += render_link("NODE_log_tbl_sel_tcam", "title", "NODE_log_tbl_sel_mux_ctrl", "title").as_str();
    dot += render_link("NODE_log_tbl_sel_tcam", "title", "NODE_log_tbl_sel_mask", "title").as_str();
    dot += render_link("NODE_log_tbl_sel_mux_ctrl", "title", "NODE_ftfp_tcam", "title").as_str();

    dot += render_link("NODE_kg_group", "age_out_profile_idx", "NODE_age_out_profile", "title").as_str();
    dot += render_link("NODE_ftfp_policy", "alu_data_lts_profile", "NODE_alu_data_mux_mask", "title").as_str();
    dot += render_link("NODE_kg_group", "collector_idx", "NODE_collector_profile", "title").as_str();
    dot += render_link("NODE_dg_group", "timestamp_profile_idx", "NODE_timestamp_profile", "title").as_str();
    dot += render_link("NODE_ftfp_tcam", "title", "NODE_ftfp_policy", "title").as_str();
    dot += render_link("NODE_ftfp_policy", "group_id", "NODE_kg_group", "title").as_str();
    dot += render_link("NODE_ftfp_policy", "session_key_lts_profile", "NODE_ses_key_mux", "title").as_str();
    dot += render_link("NODE_ftfp_policy", "session_key_lts_profile", "NODE_ses_key_mask", "title").as_str();
    dot += render_link("NODE_ftfp_policy", "session_data_lts_profile", "NODE_ses_data_mux", "title").as_str();
    dot += render_link("NODE_ftfp_policy", "session_data_lts_profile", "NODE_ses_data_mask", "title").as_str();
    dot += render_link("NODE_ftfp_policy", "group_id", "NODE_dg_group", "title").as_str();
    
    dot += render_link("NODE_kg_group", "session_data_type", "NODE_IFT_pdd_profile", "title").as_str();
    dot += render_link("NODE_kg_group", "session_data_type", "NODE_IFT_pde_profile", "title").as_str();
    dot += render_link("NODE_kg_group", "session_data_type", "NODE_IFT_export_pde_profile", "title").as_str();

    dot += render_link("NODE_kg_group", "session_data_type", "NODE_EFT_pdd_profile", "title").as_str();
    dot += render_link("NODE_kg_group", "session_data_type", "NODE_EFT_pde_profile", "title").as_str();
    dot += render_link("NODE_kg_group", "session_data_type", "NODE_EFT_export_pde_profile", "title").as_str();

    dot += render_link("NODE_kg_group", "flow_exceed_profile_idx", "NODE_flow_exceed_profile", "title").as_str();

    dot += "}";
    dot
}

trait Renderable {
    fn render(&self) -> String;
}

macro_rules! render_row {
    ($self:ident, $x:ident) => { (stringify!($x), format!("{}", $self.$x).as_str(), stringify!($x)) }
}

impl Renderable for BSC_DG_GroupALU32Profile_info {
    fn render(&self) -> String {
        fn check_op_to_string(op: u32) -> String {
            String::from(match op {
                0 => "GREATER (0)",
                1 => "GEQ (1)",
                2 => "EQ (2)",
                3 => "NOT EQ (3)",
                4 => "LESS (4)",
                5 => "LEQ (5)",
                6 => "ALWAYS_PASS (6)",
                7 => "FAIL/NONE (7)",
                8 => "MASK (8)",
                _ => "UNRECOGNIZED CHECK OP"
            })
        }

        fn check_action_to_string(act: u32) -> String {
            String::from(match act {
                0 => "NOOP (0)",
                1 => "INCREMENT (1)",
                2 => "ADD_BYTES (2)",
                3 => "ADD_VALUE (3)",
                4 => "UPDATE_MIN (4)",
                5 => "UPDATE_MAX (5)",
                6 => "UPDATE_VAL (6)",
                7 => "UPDATE_EWMA_AVG (7)",
                _ => "UNRECOGNIZED ACTION"
            })
        }

        fn weight_to_string(weight: u32) -> String {
            String::from(match weight {
                0  => "λ=100%",
                1  => "λ=96%",
                2  => "λ=92%",
                3  => "λ=88%",
                4  => "λ=86%",
                5  => "λ=84%",
                6  => "λ=81%",
                7  => "λ=78%",
                8  => "λ=75%",
                9  => "λ=71%",
                10  => "λ=67%",
                11  => "λ=63%",
                12  => "λ=59%",
                13  => "λ=56%",
                14  => "λ=53%",
                15  => "λ=50%",
                16  => "λ=47%",
                17  => "λ=44%",
                18  => "λ=41%",
                19  => "λ=37%",
                20  => "λ=33%",
                21  => "λ=29%",
                22  => "λ=25%",
                23  => "λ=22%",
                24  => "λ=19%",
                25  => "λ=16%",
                26  => "λ=14%",
                27  => "λ=12%",
                28  => "λ=8%",
                29  => "λ=4%",
                30  => "λ=0%",
                _ => "UNKNOWN WEIGHTING"
            })
        }

        fn delay_update_ts_select(mode: u32) -> String {
            String::from(match mode {
                0 => "UPDATE ATTR (0)",
                1 => "UPDATE DELAY RESULT (1)",
                _ => "UNKNOWN DELAY SELECT"
            })
        }

        fn delay_read_ts_select(mode: u32) -> String {
            String::from(match mode {
                0 => "READ ATTR (0)",
                1 => "READ DELAY RESULT (1)",
                _ => "UNKNOWN DELAY SELECT"
            })
        }
        
        fn delay_mode_to_str(mode: u32) -> String {
            String::from(match mode {
                0 => "PTP_64BIT (0)",
                1 => "NTP_64BIT (1)",
                2 => "GENERIC_48BIT (2)",
                3 => "LTS_MODE (3)",
                _ => "UNKNWON MODE"
            })
        }

        fn delay_op_to_str(mode: u32) -> String {
            String::from(match mode {
                0 => "PKT_TS0 (0)",
                1 => "PKT_TS1 (1)",
                2 => "SESSION_TS0 (2)",
                3 => "SESSION_TS1 (3)",
                4 => "SESSION_TS2 (4)",
                5 => "SESSION_TS3 (5)",
                6 => "CMIC_TS (6)",
                10 => "LTS_MODE_TOD (10)",
                7 => "LIVE IPROC (7)",
                8 => "LIVE IPROC (8)",
                9 => "BSC_TS_UTC_CONVERSION TOD (9)",
                _ => "UNKNOWN DELAY OP"
            })
        }


        render_to_record(format!("NODE_alu_{}_profile", self.alu_idx).as_str(), format!("alu_{}_profile", self.alu_idx).as_str(), self.profile_idx, "title", &vec![
            render_row!(self, check_0_attr_select),
            ("check_0_op", check_op_to_string(self.check_0_op).as_str(), "check_0_op"),
            render_row!(self, check_1_attr_select),
            ("check_1_op", check_op_to_string(self.check_1_op).as_str(), "check_1_op"),
            render_row!(self, timestamp_2_trig),
            render_row!(self, timestamp_3_trig),
            render_row!(self, update_attr_select),
            ("update_op", check_action_to_string(self.update_op).as_str(), "update_op"),
            ("update_op_param", weight_to_string(self.update_op_param).as_str(), "update_op_param"),
            render_row!(self, export_op),
            render_row!(self, export_check_mode),
            render_row!(self, clear_on_export),
            render_row!(self, clear_on_periodic_export),
            render_row!(self, load_trigger),
            render_row!(self, timestamp_0_trig),
            render_row!(self, timestamp_1_trig),
            render_row!(self, replace_check_0_byte_count),
            render_row!(self, replace_check_1_byte_count),
            render_row!(self, replace_update_byte_count),

            render_row!(self, spec_check_0_thresh),
            render_row!(self, spec_check_1_thresh),
            render_row!(self, spec_export_thresh),
            ("spec_first_delay_op", delay_op_to_str(self.spec_first_delay_op).as_str(), "spec_first_delay_op"),
            ("spec_second_delay_op", delay_op_to_str(self.spec_second_delay_op).as_str(), "spec_second_delay_op"),
            ("spec_delay_mode", delay_mode_to_str(self.spec_delay_mode).as_str(), "spec_delay_mode"),
            render_row!(self, spec_delay_gran),
            render_row!(self, spec_delay_offset),
            ("spec_check_attr_0_ts_select", delay_read_ts_select(self.spec_check_attr_0_ts_select).as_str(), "spec_check_attr_0_ts_select"),
            ("spec_check_attr_1_ts_select", delay_read_ts_select(self.spec_check_attr_1_ts_select).as_str(), "spec_check_attr_1_ts_select"),
            ("spec_update_attr_1_ts_select", delay_update_ts_select(self.spec_update_attr_1_ts_select).as_str(), "spec_update_attr_1_ts_select"),
        ])
    }
}

impl Renderable for BSC_KG_GroupTable_info {
    fn render(&self) -> String {
        render_to_record("NODE_kg_group", "kg_group", self.idx, "title", &vec![
            render_row!(self, age_out_profile_idx),
            render_row!(self, flow_exceed_profile_idx),
            render_row!(self, collector_idx),
            render_row!(self, export_learn),
            render_row!(self, session_data_type),
            render_row!(self, set_id),
            render_row!(self, group_valid),
            render_row!(self, periodic_export_enabled)
        ])
    }
}

impl Renderable for BSC_EX_CollectorConfig_info {
    fn render(&self) -> String {
        render_to_record("NODE_collector_profile", "collector_profile", self.idx, "title", &vec![
            render_row!(self, time_interval_enable),
            render_row!(self, ra_enable),
            render_row!(self, pkt_build_enable),
            render_row!(self, max_records_per_pkt),
            render_row!(self, max_ptr),
            render_row!(self, enable),
            render_row!(self, time_interval),
        ])
    }
}

impl Renderable for BSC_DG_GroupTable_info {
    fn render(&self) -> String {
        render_to_record("NODE_dg_group", "dg_group", self.idx, "title", &vec![
            render_row!(self, group_valid),
            render_row!(self, timestamp_profile_idx),
            render_row!(self, collector_id),
            ("alu_0_profile", format!("{}", self.alu_profile_idxs[0]).as_str(), "alu_0_profile"),
            ("alu_1_profile", format!("{}", self.alu_profile_idxs[1]).as_str(), "alu_1_profile"),
            ("alu_2_profile", format!("{}", self.alu_profile_idxs[2]).as_str(), "alu_2_profile"),
            ("alu_3_profile", format!("{}", self.alu_profile_idxs[3]).as_str(), "alu_3_profile"),
            ("alu_4_profile", format!("{}", self.alu_profile_idxs[4]).as_str(), "alu_4_profile"),
            ("alu_5_profile", format!("{}", self.alu_profile_idxs[5]).as_str(), "alu_5_profile"),
            ("alu_6_profile", format!("{}", self.alu_profile_idxs[6]).as_str(), "alu_6_profile"),
            ("alu_7_profile", format!("{}", self.alu_profile_idxs[7]).as_str(), "alu_7_profile"),
            ("alu_8_profile", format!("{}", self.alu_profile_idxs[8]).as_str(), "alu_8_profile"),
            ("alu_9_profile", format!("{}", self.alu_profile_idxs[9]).as_str(), "alu_9_profile"),
            ("alu_10_profile", format!("{}", self.alu_profile_idxs[10]).as_str(), "alu_10_profile"),
            ("alu_11_profile", format!("{}", self.alu_profile_idxs[11]).as_str(), "alu_11_profile"),
        ])
    }
}

impl Renderable for BSC_DG_GroupTimestampProfileTable_info {
    fn render(&self) -> String {
        fn ts_select_to_str(ts: u32) -> String {
            String::from(match ts {
                0 => "LEGACY_TOD (0)",
                1 => "NTP_TOD (1)",
                2 => "PTP_TOD (2)",
                3 => "CMIC_TS (3)",
                4 => "PKT_TS0 (4)",
                5 => "PKT_TS1 (5)",
                _ => "UNKNOWN TIMESTAMP SELECT"
            })
        }
        render_to_record("NODE_timestamp_profile", "timestamp_profile", self.idx, "title", &vec![
            ("ts_select_0", ts_select_to_str(self.ts_select_0).as_str(), "ts_select_0"),
            render_row!(self, data_0),
            ("ts_select_1", ts_select_to_str(self.ts_select_1).as_str(), "ts_select_1"),
            render_row!(self, data_1),
            ("ts_select_2", ts_select_to_str(self.ts_select_2).as_str(), "ts_select_2"),
            render_row!(self, data_2),
            ("ts_select_3", ts_select_to_str(self.ts_select_3).as_str(), "ts_select_3"),
            render_row!(self, data_3),
        ])
    }
}

impl Renderable for BSK_FTFP_Policy_info {
    fn render(&self) -> String {
        render_to_record("NODE_ftfp_policy", "ftfp_policy", self.idx, "title", &vec![
            render_row!(self, group_id),
            render_row!(self, bidir_flow),
            render_row!(self, session_key_lts_profile),
            render_row!(self, session_data_lts_profile),
            render_row!(self, alu_data_lts_profile),
            render_row!(self, session_key_type),
            render_row!(self, do_not_ft),
            render_row!(self, learn_diable),
            render_row!(self, uflow_fields_valid),
        ])
    }
}

impl Renderable for BSK_FTFP_TCAM_info {
    fn render(&self) -> String {
        let mask_rec = format!("{:02X?}", self.mask);
        let key_rec = format!("{:02X?}", self.key);
        render_to_record("NODE_ftfp_tcam", "ftfp_tcam", self.idx, "title", &vec![
            ("key", key_rec.as_str(), "key"),
            ("mask", mask_rec.as_str(), "mask"),
            render_row!(self, key_type),
            render_row!(self, mask_type),
            render_row!(self, valid)
        ])
    }
}

impl Renderable for BSK_SessionKeyLTSMuxCtrl0_info {
    fn render(&self) -> String {
        let mut extract_map = HashMap::<(u32, u32, u32), &str>::new();
        extract_map.insert((0x85, 0x1, 0x0), "switch_port (8b)");
        extract_map.insert((0x0E, 0x0, 0x0), "src_ipv4_lower (16b)");
        extract_map.insert((0x0F, 0x0, 0x0), "src_ipv4_upper (16b)");
        extract_map.insert((0x16, 0x0, 0x0), "dst_ipv4_lower (16b)");
        extract_map.insert((0x17, 0x0, 0x0), "dst_ipv4_upper (16b)");
        extract_map.insert((0x11, 0x1, 0x0), "ip_proto (8b)");
        extract_map.insert((0x23, 0x0, 0x0), "l4_src_port (16b)");
        extract_map.insert((0x22, 0x0, 0x0), "l4_dst_port (16b)");
        extract_map.insert((0x25, 0x1, 0x0), "tcp_flags (8b)");
        extract_map.insert((0x14, 0x0, 0x0), "ipv4_len (16b)");
        extract_map.insert((0x0, 0x0, 0x0), "none (16b)");

        render_to_record("NODE_ses_key_mux", "ses_key_mux", self.idx, "title", &vec![
            ("extract_a_0", extract_map.get(&self.extracts[0]).unwrap_or(&format!("unknown ({:?})", &self.extracts[0]).as_str()), "extract_a_0"),
            ("extract_a_1", extract_map.get(&self.extracts[1]).unwrap_or(&format!("unknown ({:?})", &self.extracts[1]).as_str()), "extract_a_1"),
            ("extract_a_2", extract_map.get(&self.extracts[2]).unwrap_or(&format!("unknown ({:?})", &self.extracts[2]).as_str()), "extract_a_2"),
            ("extract_a_3", extract_map.get(&self.extracts[3]).unwrap_or(&format!("unknown ({:?})", &self.extracts[3]).as_str()), "extract_a_3"),
            ("extract_a_4", extract_map.get(&self.extracts[4]).unwrap_or(&format!("unknown ({:?})", &self.extracts[4]).as_str()), "extract_a_4"),
            ("extract_a_5", extract_map.get(&self.extracts[5]).unwrap_or(&format!("unknown ({:?})", &self.extracts[5]).as_str()), "extract_a_5"),
            ("extract_a_6", extract_map.get(&self.extracts[6]).unwrap_or(&format!("unknown ({:?})", &self.extracts[6]).as_str()), "extract_a_6"),
            ("extract_a_7", extract_map.get(&self.extracts[7]).unwrap_or(&format!("unknown ({:?})", &self.extracts[7]).as_str()), "extract_a_7"),
            ("extract_a_8", extract_map.get(&self.extracts[8]).unwrap_or(&format!("unknown ({:?})", &self.extracts[8]).as_str()), "extract_a_8"),
            ("extract_a_9", extract_map.get(&self.extracts[9]).unwrap_or(&format!("unknown ({:?})", &self.extracts[9]).as_str()), "extract_a_9"),
        ])
    }
}

impl Renderable for BSK_SessionKeyLTSMask0_info {
    fn render(&self) -> String {
        let mask_rec = format!("{:02X?}", self.mask);
        render_to_record("NODE_ses_key_mask", "ses_key_mask", self.idx, "title", &vec![
            ("mask", mask_rec.as_str(), "mask"),
        ])
    }
}

impl Renderable for BSK_SessionDataLTSMuxCtrl0_info {
    fn render(&self) -> String {
        let mut extract_map = HashMap::<(u32, u32, u32), &str>::new();
        extract_map.insert((0x85, 0x1, 0x0), "switch_port (8b)");
        extract_map.insert((0x0E, 0x0, 0x0), "src_ipv4_lower (16b)");
        extract_map.insert((0x0F, 0x0, 0x0), "src_ipv4_upper (16b)");
        extract_map.insert((0x16, 0x0, 0x0), "dst_ipv4_lower (16b)");
        extract_map.insert((0x17, 0x0, 0x0), "dst_ipv4_upper (16b)");
        extract_map.insert((0x11, 0x1, 0x0), "ip_proto (8b)");
        extract_map.insert((0x23, 0x0, 0x0), "l4_src_port (16b)");
        extract_map.insert((0x22, 0x0, 0x0), "l4_dst_port (16b)");
        extract_map.insert((0x25, 0x1, 0x0), "tcp_flags (8b)");
        extract_map.insert((0x14, 0x0, 0x0), "ipv4_len (16b)");
        extract_map.insert((0x0, 0x0, 0x0), "none (16b)");

        render_to_record("NODE_ses_data_mux", "ses_data_mux", self.idx, "title", &vec![
            ("extract_a_0", extract_map.get(&self.extracts[0]).unwrap_or(&format!("unknown ({:?})", &self.extracts[0]).as_str()), "extract_a_0"),
            ("extract_a_1", extract_map.get(&self.extracts[1]).unwrap_or(&format!("unknown ({:?})", &self.extracts[1]).as_str()), "extract_a_1"),
            ("extract_a_2", extract_map.get(&self.extracts[2]).unwrap_or(&format!("unknown ({:?})", &self.extracts[2]).as_str()), "extract_a_2"),
            ("extract_a_3", extract_map.get(&self.extracts[3]).unwrap_or(&format!("unknown ({:?})", &self.extracts[3]).as_str()), "extract_a_3"),
            ("extract_a_4", extract_map.get(&self.extracts[4]).unwrap_or(&format!("unknown ({:?})", &self.extracts[4]).as_str()), "extract_a_4"),
            ("extract_a_5", extract_map.get(&self.extracts[5]).unwrap_or(&format!("unknown ({:?})", &self.extracts[5]).as_str()), "extract_a_5"),
            ("extract_a_6", extract_map.get(&self.extracts[6]).unwrap_or(&format!("unknown ({:?})", &self.extracts[6]).as_str()), "extract_a_6"),
            ("extract_a_7", extract_map.get(&self.extracts[7]).unwrap_or(&format!("unknown ({:?})", &self.extracts[7]).as_str()), "extract_a_7"),
            ("extract_a_8", extract_map.get(&self.extracts[8]).unwrap_or(&format!("unknown ({:?})", &self.extracts[8]).as_str()), "extract_a_8"),
            ("extract_a_9", extract_map.get(&self.extracts[9]).unwrap_or(&format!("unknown ({:?})", &self.extracts[9]).as_str()), "extract_a_9"),
        ])
    }
}

impl Renderable for BSK_SessionDataLTSMask0_info {
    fn render(&self) -> String {
        let mask_rec = format!("{:02X?}", self.mask);
        render_to_record("NODE_ses_data_mask", "ses_data_mask", self.idx, "title", &vec![
            ("mask", mask_rec.as_str(), "mask"),
        ])
    }
}

impl Renderable for BSK_ALUDataLTSMuxCtrlPlusMask_info {
    fn render(&self) -> String {
        let mask_rec = format!("{:02X?}", self.mask);
        let extract_cs = format!("{:?}", self.extract_cs);
        render_to_record("NODE_alu_data_mux_mask", "alu_data_mux_mask", self.idx, "title", &vec![
            ("mask", mask_rec.as_str(), "mask"),
            ("extracts", extract_cs.as_str(), "extracts")
        ])
    }
}

impl Renderable for BSK_FTFP_LTS_LogicalTblSelSRAM_info {
    fn render(&self) -> String {
        render_to_record("NODE_log_tbl_sel_sram", "log_tbl_sel_sram", self.idx, "title", &vec![
            render_row!(self, group_key_type),
            render_row!(self, delay_mode),
            render_row!(self, group_lookup_enable)
        ])
    }
}

impl Renderable for BSK_FTFP_LTS_LogicalTblSelTCAM_info {
    fn render(&self) -> String {
        render_to_record("NODE_log_tbl_sel_tcam", "log_tbl_sel_tcam", self.idx, "title", &vec![
            render_row!(self, valid),
            render_row!(self, parser1_l4_valid),
            render_row!(self, parser1_l4_valid_mask),
            render_row!(self, l3_type_mask),
            render_row!(self, hve_results_1_z1_uc),
            render_row!(self, hve_results_1_z1_uc_mask),
            render_row!(self, hve_results_1_z1_bc_mask),
        ])
    }
}

impl Renderable for BSK_FTFP_LTS_MuxCtrl0_info {
    fn render(&self) -> String {
        let mut extract_map = HashMap::<(u32, u32, u32), &str>::new();
        extract_map.insert((0x85, 0x1, 0x0), "switch_port (8b)");
        extract_map.insert((0x0E, 0x0, 0x0), "src_ipv4_lower (16b)");
        extract_map.insert((0x0F, 0x0, 0x0), "src_ipv4_upper (16b)");
        extract_map.insert((0x16, 0x0, 0x0), "dst_ipv4_lower (16b)");
        extract_map.insert((0x17, 0x0, 0x0), "dst_ipv4_upper (16b)");
        extract_map.insert((0x11, 0x1, 0x0), "ip_proto (8b)");
        extract_map.insert((0x23, 0x0, 0x0), "l4_src_port (16b)");
        extract_map.insert((0x22, 0x0, 0x0), "l4_dst_port (16b)");
        extract_map.insert((0x25, 0x1, 0x0), "tcp_flags (8b)");
        extract_map.insert((0x14, 0x0, 0x0), "ipv4_len (16b)");
        extract_map.insert((0x0, 0x0, 0x0), "none (16b)");

        render_to_record("NODE_log_tbl_sel_mux_ctrl", "log_tbl_sel_mux_ctrl", self.idx, "title", &vec![
            ("extract_a_0", extract_map.get(&self.extracts[0]).unwrap_or(&format!("unknown ({:?})", &self.extracts[0]).as_str()), "extract_a_0"),
            ("extract_a_1", extract_map.get(&self.extracts[1]).unwrap_or(&format!("unknown ({:?})", &self.extracts[1]).as_str()), "extract_a_1"),
            ("extract_a_2", extract_map.get(&self.extracts[2]).unwrap_or(&format!("unknown ({:?})", &self.extracts[2]).as_str()), "extract_a_2"),
            ("extract_a_3", extract_map.get(&self.extracts[3]).unwrap_or(&format!("unknown ({:?})", &self.extracts[3]).as_str()), "extract_a_3"),
            ("extract_a_4", extract_map.get(&self.extracts[4]).unwrap_or(&format!("unknown ({:?})", &self.extracts[4]).as_str()), "extract_a_4"),
            ("extract_a_5", extract_map.get(&self.extracts[5]).unwrap_or(&format!("unknown ({:?})", &self.extracts[5]).as_str()), "extract_a_5"),
            ("extract_a_6", extract_map.get(&self.extracts[6]).unwrap_or(&format!("unknown ({:?})", &self.extracts[6]).as_str()), "extract_a_6"),
            ("extract_a_7", extract_map.get(&self.extracts[7]).unwrap_or(&format!("unknown ({:?})", &self.extracts[7]).as_str()), "extract_a_7"),
            ("extract_a_8", extract_map.get(&self.extracts[8]).unwrap_or(&format!("unknown ({:?})", &self.extracts[8]).as_str()), "extract_a_8"),
            ("extract_a_9", extract_map.get(&self.extracts[9]).unwrap_or(&format!("unknown ({:?})", &self.extracts[9]).as_str()), "extract_a_9"),
        ])
    }
}

impl Renderable for BSK_FTFP_LTS_Mask0_info {
    fn render(&self) -> String {
        let mask_rec = format!("{:02X?}", self.mask);
        render_to_record("NODE_log_tbl_sel_mask", "log_tbl_sel_mask", self.idx, "title", &vec![
            ("mask", mask_rec.as_str(), "mask")
        ])
    }
}

impl Renderable for BSC_KG_AgeOutProfile_info {
    fn render(&self) -> String {
        fn interval_to_real_val(interval: u32) -> String {
            String::from(match interval {
                0 => "100ms",
                1 => "1s",
                2 => "10s",
                3 => "1min",
                4 => "10min",
                5 => "30min",
                6 => "1hour",
                7 => "10hour",
                8 => "1day",
                _ => "invalid"
            })
        }
        render_to_record("NODE_age_out_profile", "age_out_profile", self.idx, "title", &vec![
            render_row!(self, enable),
            render_row!(self, export_age_out),
            ("interval", interval_to_real_val(self.interval).as_str(), "interval")
        ])
    }
}

impl Renderable for BSC_PolicyActionProfile_info {
    fn render(&self) -> String {
        let ft = match self.ft {0 => "IFT", 1 => "MFT", 2 => "EFT", _ => "UNKNWON_FLOWTRACKER"};
        render_to_record(format!("NODE_{}_pdd_profile", ft).as_str(), format!("{}_pdd_profile", ft).as_str(), self.idx, "title", &vec![
            ("alu32_0", format!("{}", self.alus[0]).as_str(), "alu32_0"),
            ("alu32_1", format!("{}", self.alus[1]).as_str(), "alu32_1"),
            ("alu32_2", format!("{}", self.alus[2]).as_str(), "alu32_2"),
            ("alu32_3", format!("{}", self.alus[3]).as_str(), "alu32_3"),
            ("alu32_4", format!("{}", self.alus[4]).as_str(), "alu32_4"),
            ("alu32_5", format!("{}", self.alus[5]).as_str(), "alu32_5"),
            ("alu32_6", format!("{}", self.alus[6]).as_str(), "alu32_6"),
            ("alu32_7", format!("{}", self.alus[7]).as_str(), "alu32_7"),
            ("alu32_8", format!("{}", self.alus[8]).as_str(), "alu32_8"),
            ("alu32_9", format!("{}", self.alus[9]).as_str(), "alu32_9"),
            ("alu32_10", format!("{}", self.alus[10]).as_str(), "alu32_10"),
            ("alu32_11", format!("{}", self.alus[11]).as_str(), "alu32_11"),
            ("ts_engine_0", format!("{}", self.ts_engines[0]).as_str(), "ts_engine_0"),
            ("ts_engine_1", format!("{}", self.ts_engines[1]).as_str(), "ts_engine_1"),
            ("ts_engine_2", format!("{}", self.ts_engines[2]).as_str(), "ts_engine_2"),
            ("ts_engine_3", format!("{}", self.ts_engines[3]).as_str(), "ts_engine_3"),
        ])
    }
}

impl Renderable for BSC_DT_PDEProfileTable_info {
    fn render(&self) -> String {
        enum EType { ALU, TS }
        let mut first_byte_map = HashMap::<u32, (&str, EType)>::new();
        first_byte_map.insert(106, ("alu_0 (4B)", EType::ALU));
        first_byte_map.insert(102, ("alu_1 (4B)", EType::ALU));
        first_byte_map.insert(98,  ("alu_2 (4B)", EType::ALU));
        first_byte_map.insert(94,  ("alu_3 (4B)", EType::ALU));
        first_byte_map.insert(90,  ("alu_4 (4B)", EType::ALU));
        first_byte_map.insert(86,  ("alu_5 (4B)", EType::ALU));
        first_byte_map.insert(82,  ("alu_6 (4B)", EType::ALU));
        first_byte_map.insert(78,  ("alu_7 (4B)", EType::ALU));
        first_byte_map.insert(74,  ("alu_8 (4B)", EType::ALU));
        first_byte_map.insert(70,  ("alu_9 (4B)", EType::ALU));
        first_byte_map.insert(66,  ("alu_10 (4B)", EType::ALU));
        first_byte_map.insert(62,  ("alu_11 (4B)", EType::ALU));
        first_byte_map.insert(136, ("ts_0 (6B)", EType::TS));
        first_byte_map.insert(130, ("ts_1 (6B)", EType::TS));
        first_byte_map.insert(124, ("ts_2 (6B)", EType::TS));
        first_byte_map.insert(118, ("ts_3 (6B)", EType::TS));

        let mut bytes_ordered = [0; 64];
        for (SHIFT_AMT, val) in self.shift_amounts.iter() {
            bytes_ordered[*SHIFT_AMT as usize] = *val;
        }

        let mut entries = Vec::<(String, String, String)>::new();
        let mut cur_byte_idx = 63;
        while cur_byte_idx > 0 {
            match first_byte_map.get(&bytes_ordered[cur_byte_idx]) {
                Some((text, typ)) => {
                    match typ {
                        &EType::ALU => { 
                            entries.push((format!("SHIFT_BYTE_[{},{},{},{}]", cur_byte_idx, cur_byte_idx - 1, cur_byte_idx - 2, cur_byte_idx - 3), String::from(*text), String::from(*text)));
                            cur_byte_idx -= 4; 
                        },
                        &EType::TS => { 
                            entries.push((format!("SHIFT_BYTE_[{},{},{},{},{},{}]", cur_byte_idx, cur_byte_idx - 1, cur_byte_idx - 2, cur_byte_idx - 3, cur_byte_idx - 4, cur_byte_idx - 5), String::from(*text), String::from(*text)));
                            cur_byte_idx -= 6; 
                        }
                    }
                },
                None => {
                    cur_byte_idx -= 1;
                }
            }
        }

        let ft = match self.ft {0 => "IFT", 1 => "MFT", 2 => "EFT", _ => "UNKNWON_FLOWTRACKER"};
        render_to_record(format!("NODE_{}_pde_profile", ft).as_str(), format!("{}_pde_profile", ft).as_str(), self.idx, "title", 
            &entries.iter().map(|(a, b, c)| (a.as_str(), b.as_str(), c.as_str())).collect()
        )
    }
}

impl Renderable for BSC_DT_ExportPDEProfileTable_info {
    fn render(&self) -> String {
        enum EType { ALU, TS }
        let mut first_byte_map = HashMap::<u32, (&str, EType)>::new();
        first_byte_map.insert(106, ("alu_0 (4B)", EType::ALU));
        first_byte_map.insert(102, ("alu_1 (4B)", EType::ALU));
        first_byte_map.insert(98,  ("alu_2 (4B)", EType::ALU));
        first_byte_map.insert(94,  ("alu_3 (4B)", EType::ALU));
        first_byte_map.insert(90,  ("alu_4 (4B)", EType::ALU));
        first_byte_map.insert(86,  ("alu_5 (4B)", EType::ALU));
        first_byte_map.insert(82,  ("alu_6 (4B)", EType::ALU));
        first_byte_map.insert(78,  ("alu_7 (4B)", EType::ALU));
        first_byte_map.insert(74,  ("alu_8 (4B)", EType::ALU));
        first_byte_map.insert(70,  ("alu_9 (4B)", EType::ALU));
        first_byte_map.insert(66,  ("alu_10 (4B)", EType::ALU));
        first_byte_map.insert(62,  ("alu_11 (4B)", EType::ALU));
        first_byte_map.insert(136, ("ts_0 (6B)", EType::TS));
        first_byte_map.insert(130, ("ts_1 (6B)", EType::TS));
        first_byte_map.insert(124, ("ts_2 (6B)", EType::TS));
        first_byte_map.insert(118, ("ts_3 (6B)", EType::TS));

        let mut bytes_ordered = [0; 64];
        for (SHIFT_AMT, val) in self.shift_amounts.iter() {
            bytes_ordered[*SHIFT_AMT as usize] = *val;
        }

        let mut entries = Vec::<(String, String, String)>::new();
        let mut cur_byte_idx = 63;
        while cur_byte_idx > 0 {
            match first_byte_map.get(&bytes_ordered[cur_byte_idx]) {
                Some((text, typ)) => {
                    match typ {
                        &EType::ALU => { 
                            entries.push((format!("SHIFT_BYTE_[{},{},{},{}]", cur_byte_idx, cur_byte_idx - 1, cur_byte_idx - 2, cur_byte_idx - 3), String::from(*text), String::from(*text)));
                            cur_byte_idx -= 4; 
                        },
                        &EType::TS => { 
                            entries.push((format!("SHIFT_BYTE_[{},{},{},{},{},{}]", cur_byte_idx, cur_byte_idx - 1, cur_byte_idx - 2, cur_byte_idx - 3, cur_byte_idx - 4, cur_byte_idx - 5), String::from(*text), String::from(*text)));
                            cur_byte_idx -= 6; 
                        }
                    }
                },
                None => {
                    cur_byte_idx -= 1;
                }
            }
        }

        let ft = match self.ft {0 => "IFT", 1 => "MFT", 2 => "EFT", _ => "UNKNWON_FLOWTRACKER"};
        render_to_record(format!("NODE_{}_export_pde_profile", ft).as_str(), format!("{}_export_pde_profile", ft).as_str(), self.idx, "title", 
            &entries.iter().map(|(a, b, c)| (a.as_str(), b.as_str(), c.as_str())).collect()
        )
    }
}

impl Renderable for BSC_KG_FlowExceedProfile_info {
    fn render(&self) -> String {
        render_to_record("NODE_flow_exceed_profile", "flow_exceed_profile", self.idx, "title", &vec![
            render_row!(self, flow_exceed_profile)
        ])
    }
}


#include <soc/drv.h>
#include <soc/error.h>
#include <soc/mem.h>
#include <soc/mcm/memregs.h>
#include <soc/format.h>
#include <shared/bsl.h>
#include <appl/diag/system.h>
#include <appl/diag/parse.h>
#include <appl/diag/shell.h>
#include <bcm_int/esw/flowtracker/ft_group.h>

#include <bcm/flowtracker.h> //for debug info
#include <bcm_int/esw/flowtracker/ft_alu.h> //for debug info alu
#include <bcm/time.h> //clock enable lives here
